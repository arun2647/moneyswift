package com.example.swiftMoney.base

import android.annotation.SuppressLint
import android.app.Application
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.view.Window
import com.example.swiftMoney.R
import com.example.swiftMoney.network.Repository
import com.google.firebase.FirebaseApp
import java.util.*


class MyApplication : Application() {

    companion object {

        var mLoader: Dialog? = null
        var application = MyApplication()
        var repository = Repository()


        @SuppressLint("StaticFieldLeak")
        lateinit var mContext: Context

        fun getContext(): Context {
            return mContext
        }

        fun showLoader(context: Context) {
            try {
                if (mLoader != null) {
                    mLoader!!.dismiss()
                    mLoader!!.cancel()
                }
                mLoader = Dialog(context)
                mLoader!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
                mLoader!!.setContentView(R.layout.layout_progress_dialog)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    Objects.requireNonNull(mLoader!!.window)
                        ?.setBackgroundDrawable(
                            ColorDrawable(
                                Color.TRANSPARENT
                            )
                        )
                }
                mLoader!!.setCancelable(false)
                mLoader!!.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun hideLoader() {
            if (mLoader != null) {
                mLoader!!.dismiss()
                mLoader!!.cancel()
            }
        }


    }

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(applicationContext)
        mContext = applicationContext

    }
}