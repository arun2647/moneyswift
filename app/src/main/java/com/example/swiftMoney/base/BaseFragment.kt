package com.example.swiftMoney.base

import android.app.Application
import androidx.fragment.app.Fragment
import com.example.swiftMoney.network.Repository

open class BaseFragment:Fragment() {

    companion object{
        var repository=Repository()
        var application=Application()
    }
}