package com.example.swiftMoney.view

import android.app.Application
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import com.example.swiftMoney.R
import com.example.swiftMoney.adapter.FilterKeywordAdapter
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.LayoutFilterBinding
import com.example.swiftMoney.model.companyTransactionFilter.CompanyTransactionFilterParamModel
import com.example.swiftMoney.model.walletHistory.WalletHistoryParamModel
import com.example.swiftMoney.network.Repository
import com.example.swiftMoney.utils.DatePickerDialog
import com.example.swiftMoney.utils.SharedPreference
import com.example.swiftMoney.utils.Status
import com.example.swiftMoney.utils.showToast
import com.example.swiftMoney.viewModel.CompanyViewModel
import com.example.swiftMoney.viewModel.ViewModelFactory
import com.example.swiftMoney.viewModel.WalletViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.slider.RangeSlider
import java.text.SimpleDateFormat
import java.util.*

class FilterDialog() : BottomSheetDialogFragment(), View.OnClickListener {

    lateinit var binding: LayoutFilterBinding
    var keywordList: ArrayList<String> = ArrayList()
    private var transactionType = listOf<String>("Completed", "Pending", "Declined", "Cancelled", "Refunded", "Failed")
    var currencyList = listOf<String>("GBP", "USD", "EUR", "CAD")
    var keyword: String = ""
    var fromDate: String = ""
    var toDate: String = ""
    var type: String = ""
    var selectCurrency: String = ""
    var fromAmount: String = ""
    var toAmount: String = ""
    var staffIdList: ArrayList<Int> = ArrayList()
    var selectedStaff: String = ""
    var isFromHome: Boolean = false


    companion object {
        val application = Application()
        val repository = Repository()
    }

    private val viewModel by viewModels<CompanyViewModel> {
        ViewModelFactory(
            application,
            repository
        )
    }

    private val walletViewModel by viewModels<WalletViewModel> {
        ViewModelFactory(
            application,
            repository
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.MyBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LayoutFilterBinding.inflate(layoutInflater)
        binding.onClick = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        keywordAdapter()
        observer()
        isFromHome = arguments?.getBoolean("isFromHome") == true
        (dialog as? BottomSheetDialog)?.behavior?.apply {
            isFitToContents = true

            state = BottomSheetBehavior.STATE_COLLAPSED
        }
        isCancelable = false
        binding.spinnerTransactionType.item = transactionType
        binding.spinnerCurrency.item = currencyList
        binding.etKeyword.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(p0: TextView?, actionId: Int, p2: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    val kwyword = binding.etKeyword.text.toString()
                    if (!kwyword.isNullOrEmpty()) {
                        binding.etKeyword.setText("")
                        keywordList.add(kwyword)
                        binding.rvKeywords.adapter?.notifyDataSetChanged()
                    } else {
                        requireContext().showToast("Enter keyword")
                    }

                    return true
                }
                return false
            }

        })


        binding.rangeSlider.addOnSliderTouchListener(object : RangeSlider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: RangeSlider) {
                val values = slider.values
                binding.tvAmountValue.text = "${values[0].toInt()}k - " + values[1].toInt() + "k"
                fromAmount = values[0].toInt().toString() + "000"
                toAmount = values[1].toInt().toString() + "000"
            }

            override fun onStopTrackingTouch(slider: RangeSlider) {
                val values = slider.values
                binding.tvAmountValue.text = "${values[0].toInt()}k - " + values[1].toInt() + "k"
                fromAmount = values[0].toInt().toString() + "000"
                toAmount = values[1].toInt().toString() + "000"
            }
        })

        val companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
        viewModel.getCompanyStaffList(companyId.toString())

        binding.spinnerTransactionType.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    p0: AdapterView<*>?,
                    p1: View?,
                    position: Int,
                    p3: Long
                ) {
                    when (position) {
                        0 -> {
                            type = "1"
                        }
                        1 -> {
                            type = "2"
                        }
                        2 -> {
                            type = "3"
                        }
                        3 -> {
                            type = "4"
                        }
                        4 -> {
                            type = "5"
                        }
                        5 -> {
                            type = "6"
                        }
                    }
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

            }

        binding.spinnerCurrency.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    p0: AdapterView<*>?,
                    p1: View?,
                    position: Int,
                    p3: Long
                ) {
                    selectCurrency = currencyList[position]
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

            }

        binding.spinnerStaff.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                selectedStaff = staffIdList[position].toString()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

        }

    }

    private fun observer() {
        viewModel.resultGetCompanyStaffList.observe(this) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        val list =
                            it.data?.data?.distinctBy { it.first_name }?.map { it.first_name }
                        binding.spinnerStaff.item = list
                        var idList = it.data?.data?.distinctBy { it.user_id }?.map { it.user_id }
                        idList?.let { it1 -> staffIdList.addAll(it1) }
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }

        walletViewModel.resultCompanyTransactionFilter.observe(this) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        val intent = Intent(requireContext(), FilterTransactionActivity::class.java)
                        intent.putExtra("transactionList", it.data?.data)
                        intent.putExtra("isFromHome", isFromHome)
                        startActivity(intent)
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }

        walletViewModel.resultWalletHistory.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        val intent = Intent(requireContext(), FilterTransactionActivity::class.java)
                        intent.putExtra("transactionList", it.data?.data)
                        intent.putExtra("isFromHome", isFromHome)
                        startActivity(intent)
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }
    }

    private fun keywordAdapter() {
        val adapter = FilterKeywordAdapter(requireContext(), keywordList, callback)
        binding.rvKeywords.adapter = adapter
    }

    var callback = object : FilterKeywordAdapter.FilterKeywordCallback {
        override fun getKeywordName(nameList: ArrayList<String>) {
            keyword = TextUtils.join(",", nameList)
        }

        override fun onRemoveItem(position: Int) {
            keywordList.removeAt(position)
            keywordAdapter()
        }

    }

    override fun onClick(v: View?) {
        when (v) {
            binding.rlFromDate -> {
                DatePickerDialog(fromDateCallback).show(parentFragmentManager, "")
            }
            binding.rlToDate -> {
                DatePickerDialog(toDateCallback).show(parentFragmentManager, "")
            }
            binding.ivClose -> {
                dismiss()
            }
            binding.btnApplyFilter -> {
                if (isFromHome) {
                    hitFilterApi()
                } else {
                    hitWalletFilterApi()
                }

            }
        }
    }

    private fun hitWalletFilterApi() {
        var companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)

        walletViewModel.getWalletHistory(
            WalletHistoryParamModel(
                companyId.toString(),
                fromAmount,
                toAmount,
                type,
                selectCurrency
            )
        )

    }

    private fun hitFilterApi() {
        val companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
        val body = CompanyTransactionFilterParamModel(
            companyId.toString(),
            type,
            fromAmount,
            toAmount,
            selectedStaff,
            toDate,
            fromDate,
            keyword,
            selectCurrency
        )
        walletViewModel.getCompanyTransactionFilter(body)
    }

    private val fromDateCallback = object : DatePickerDialog.DatePickerCallback {
        @RequiresApi(Build.VERSION_CODES.O)
        override fun getDate(year: Int, month: Int, day: Int) {
            val instance = Calendar.getInstance()
            instance.set(year, month, day)
            val simpleDateFormat = SimpleDateFormat("MMM dd,yyyy", Locale.ENGLISH)
            val formatDate = SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH)
            fromDate = formatDate.format(instance.time)
            binding.tvFromDate.text = simpleDateFormat.format(instance.time)
        }
    }

    private val toDateCallback = object : DatePickerDialog.DatePickerCallback {
        @RequiresApi(Build.VERSION_CODES.O)
        override fun getDate(year: Int, month: Int, day: Int) {
            val instance = Calendar.getInstance()
            instance.set(year, month, day)
            val simpleDateFormat = SimpleDateFormat("MMM dd,yyyy", Locale.ENGLISH)
            val formatDate = SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH)
            toDate = formatDate.format(instance.time)
            binding.tvToDate.text = simpleDateFormat.format(instance.time)
        }
    }


}