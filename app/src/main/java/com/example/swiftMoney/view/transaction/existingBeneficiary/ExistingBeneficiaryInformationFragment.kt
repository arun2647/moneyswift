package com.example.swiftMoney.view.transaction.existingBeneficiary

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.LinearLayout
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.swiftMoney.R
import com.example.swiftMoney.adapter.CompanyBeneficiaryAdapter
import com.example.swiftMoney.base.BaseFragment
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.FragmentExistingBeneficiaryInformationBinding
import com.example.swiftMoney.model.companyBeneficiary.Data
import com.example.swiftMoney.utils.SharedPreference
import com.example.swiftMoney.utils.Status
import com.example.swiftMoney.utils.showToast
import com.example.swiftMoney.viewModel.AuthViewModel
import com.example.swiftMoney.viewModel.ViewModelFactory
import com.example.swiftMoney.viewModel.WalletViewModel
import java.util.regex.Pattern


class ExistingBeneficiaryInformationFragment : BaseFragment(), View.OnClickListener {

    private var reference: String=""
    private var state: String=""
    private var bic: String=""
    private var branchNumber: String=""
    private var swiftCode: String=""
    private var ibanNumber: String=""
    private var wireRoutingNumber: String=""
    private var routingNumber: String=""
    private var accountNumber: String=""
    private var businessType: String=""
    private var bankCountry: String=""
    private var bankZipcode: String=""
    private var bankState: String=""
    private var bankAddress: String=""
    private var bankCity: String=""
    private var bankName: String=""

    lateinit var binding: FragmentExistingBeneficiaryInformationBinding
    var selectCountryName: String = ""
    var selectStateName: String = ""
    var countryListId: ArrayList<Int> = ArrayList()

    private var countryList:List<String> = ArrayList()
    private var stateList:List<String> = ArrayList()
    private val viewModel by viewModels<AuthViewModel> { ViewModelFactory(application, repository) }
    private val walletViewModel by viewModels<WalletViewModel> {
        ViewModelFactory(
            application,
            repository
        )
    }
    var getWallet: String = ""
    var amount: String = ""
    var beneficiaryList: ArrayList<Data> = ArrayList()
    var beneficiaryId=""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentExistingBeneficiaryInformationBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()
        getWallet = arguments?.getString("wallet", "").toString()
        amount = arguments?.getString("amount", "").toString()
        selectedCountryAndStateSpinner()
        binding.onClick = this
        viewModel.getCountryList()
        var companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
        walletViewModel.getCompanyBeneficiary(companyId.toString())
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(text: Editable?) {
                var list: ArrayList<Data> = ArrayList()
                if (text?.length!! >= 2) {
                    binding.llBeneficiary.visibility = View.VISIBLE
                    binding.llBeneficiary.animate().alpha(1.0f);
                    binding.llBeneficiary.animate().translationY(0f);
                    for (i in beneficiaryList.indices) {
                        if (beneficiaryList[i].beneficiary_name.toLowerCase().contains(text)) {
                            if (!list.contains(beneficiaryList[i])) {
                                list.add(beneficiaryList[i])
                            }
                        }
                    }
                    if (list.size<=5){
                        binding.llBeneficiary.layoutParams.height=LinearLayout.LayoutParams.WRAP_CONTENT
                    }
                    setBeneficiaryListAdapter(list)
                } else {
                    binding.llBeneficiary.animate().alpha(0f);
                    binding.llBeneficiary.animate().translationY(0F);
                    binding.llBeneficiary.visibility = View.GONE
                    setBeneficiaryListAdapter(beneficiaryList)
                }
            }
        })
    }

    private fun selectedCountryAndStateSpinner() {
        binding.spinnerCountry.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    p0: AdapterView<*>?,
                    p1: View?,
                    position: Int,
                    p3: Long
                ) {
                    val selectedCountryId = countryListId[position]
                    selectCountryName = p0?.selectedItem.toString()
                    viewModel.getStateList(selectedCountryId.toString())
                    binding.tvCountryError.visibility = View.GONE
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

            }

        binding.spinnerState.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                selectStateName = p0?.selectedItem.toString()
                binding.tvStateError.visibility = View.GONE
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnNext -> {
                val amountTransfer = amount
                val beneficiaryName = binding.etName.text.toString()
                val beneficiaryEmailAddress = binding.eetEmail.text.toString()
                val beneficiaryPhoneNumber = binding.etPhoneNumber.text.toString()
                val beneficiaryAddress = binding.etAddress.text.toString()
                val beneficiaryCity = binding.etCity.text.toString()
                val beneficiaryZipCode = binding.etZipCode.text.toString()
                val bundle = Bundle()
                bundle.putString("amountTransfer", amountTransfer)
                bundle.putString("beneficiaryName", beneficiaryName)
                bundle.putString("beneficiaryEmailAddress", beneficiaryEmailAddress)
                bundle.putString("beneficiaryPhoneNumber", beneficiaryPhoneNumber)
                bundle.putString("beneficiaryAddress", beneficiaryAddress)
                bundle.putString("beneficiaryCity", beneficiaryCity)
                bundle.putString("beneficiaryCountry", selectCountryName)
                bundle.putString("beneficiaryState", selectStateName)
                bundle.putString("beneficiaryZipCode", beneficiaryZipCode)
                bundle.putString("wallet", getWallet)
                bundle.putString("bankName", bankName)
                bundle.putString("bankAddress", bankAddress)
                bundle.putString("bankCity", bankCity)
                bundle.putString("accountNumber", accountNumber)
                bundle.putString("bankCountry", bankCountry)
                bundle.putString("bankState", bankState)
                bundle.putString("bankZipcode", bankZipcode)
                bundle.putString("ibanNumber", ibanNumber)
                bundle.putString("routingNumber", routingNumber)
                bundle.putString("wireRoutingNumber", wireRoutingNumber)
                bundle.putString("branchNumber", branchNumber)
                bundle.putString("bic",bic)
                bundle.putString("swiftCode",swiftCode)
                bundle.putString("businessType",businessType)
                bundle.putString("beneficiaryId",beneficiaryId)
                bundle.putString("reference",reference)
                if (validate()) {
                    findNavController().navigate(
                        R.id.action_existingBeneficiaryInformationFragment_to_existingBeneficiaryBankDetailFragment,
                        bundle
                    )
                }

            }
        }
    }

    fun validate(): Boolean {
        var isValid: Boolean = true
        if (TextUtils.isEmpty(binding.etName.text.toString())) {
            isValid = false
            binding.etName.error = "Enter name"
        }
        if (TextUtils.isEmpty(binding.eetEmail.text.toString())) {
            isValid = false
            binding.eetEmail.error = "Enter email address"
        } else if (!Pattern.matches(Patterns.EMAIL_ADDRESS.toRegex().toString(),binding.eetEmail.text.toString())){
            isValid=false
            binding.eetEmail.error= "Enter valid email"
        }
        if (TextUtils.isEmpty(binding.etPhoneNumber.text.toString())) {
            isValid = false
            binding.etPhoneNumber.error = "Enter phone number"
        }else if (binding.etPhoneNumber.text.toString().length<9 || binding.etPhoneNumber.text.toString().length>12){
            isValid = false
            binding.etPhoneNumber.error = "Enter valid phone number"
        }
        if (TextUtils.isEmpty(binding.etAddress.text.toString())) {
            isValid = false
            binding.etAddress.error = "Enter address"
        }
        if (selectCountryName == "") {
            isValid = false
            requireContext().showToast("Select country")
        }
        if (selectStateName == "") {
            isValid = false
            requireContext().showToast("Select state")
        }
        if (TextUtils.isEmpty(binding.etCity.text.toString())) {
            isValid = false
            binding.etCity.error = "Enter city"
        }
        if (TextUtils.isEmpty(binding.etZipCode.text.toString())) {
            isValid = false
            binding.etZipCode.error = "Enter zip code"
        }
        return isValid
    }


    private fun observer() {
        viewModel.resultCountryList.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        for (i in it.data?.message!!) {
                            countryListId.add(i.id)
                        }
                       countryList=it.data?.message?.distinctBy { it.country_name }?.map { it.country_name }
                        binding.spinnerCountry.item = countryList
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }

        viewModel.resultStateList.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        stateList= it.data?.message?.distinctBy { it.name }?.map { it.name }!!
                        binding.spinnerState.item = stateList
                        for (i in stateList){
                            if (i == state){
                                val statePosition=stateList.indexOf(state)
                                binding.spinnerState.setSelection(statePosition)
                            }
                        }
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }

        walletViewModel.resultCompanyBeneficiary.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        beneficiaryList.clear()
                        it.data?.data?.let { it1 -> beneficiaryList.addAll(it1) }
                        it.data?.data?.let { it1 -> setBeneficiaryListAdapter(it1) }
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }


        walletViewModel.resultBeneficiaryId.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        val response=it.data?.data
                        val country=it.data?.data?.beneficiary_country
                         state=it.data?.data?.beneficiary_state.toString()

                        for (i in countryList){
                            if (i == country){
                                val countryPosition=countryList.indexOf(country)
                                binding.spinnerCountry.setSelection(countryPosition)
                            }
                        }
                        beneficiaryId= response?.id.toString()


                        binding.etName.setText(response?.beneficiary_name)
                        binding.eetEmail.setText(response?.beneficiary_email)
                        binding.etPhoneNumber.setText(response?.beneficiary_phone)
                        binding.etAddress.setText(response?.beneficiary_address)
                        binding.etCity.setText(response?.beneficiary_city)
                        binding.etZipCode.setText(response?.beneficiary_zip)
                        bankName= response?.bank_name.toString()
                        bankCity=response?.bank_city.toString()
                        bankAddress=response?.bank_address.toString()
                        bankState=response?.bank_state.toString()
                        bankZipcode=response?.bank_zipcode.toString()
                        bankCountry=response?.bank_country.toString()
                        businessType=response?.business_type.toString()
                        accountNumber=response?.account_number.toString()
                        routingNumber=response?.account_routing_number.toString()
                        wireRoutingNumber=response?.wire_routing_number.toString()
                        ibanNumber=response?.iban_number.toString()
                        swiftCode=response?.sort_swift_code.toString()
                        branchNumber=response?.branch_number.toString()
                        bic=response?.bic.toString()
                        reference=response?.type.toString()

                        //binding.spinnerCountry.item?.filter { it.equals() }
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }
    }

    private fun setBeneficiaryListAdapter(data: List<Data>) {
        val adapter = CompanyBeneficiaryAdapter(requireContext(), data, beneficiaryCallback)
        binding.rvBeneficiaryList.adapter = adapter
    }

    private val beneficiaryCallback = object : CompanyBeneficiaryAdapter.CompanyBeneficiaryCallback {
            override fun onClickItem(beneficiaryId: String,beneficiaryName:String) {
                binding.etSearch.setText(beneficiaryName)
                binding.llBeneficiary.visibility=View.GONE
                walletViewModel.getBeneficiaryId(beneficiaryId)
            }

        }
}