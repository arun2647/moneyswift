package com.example.swiftMoney.view

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.example.swiftMoney.base.BaseActivity
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.ActivityCompanyProfileBinding
import com.example.swiftMoney.utils.SharedPreference
import com.example.swiftMoney.utils.Status
import com.example.swiftMoney.utils.showToast
import com.example.swiftMoney.viewModel.CompanyViewModel
import com.example.swiftMoney.viewModel.ViewModelFactory

class CompanyProfileActivity : BaseActivity(), View.OnClickListener {

    lateinit var binding: ActivityCompanyProfileBinding

    private val viewModel by viewModels<CompanyViewModel> { ViewModelFactory(application, repository) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCompanyProfileBinding.inflate(layoutInflater)
        binding.onClick = this
        setContentView(binding.root)
        var companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
        viewModel.getCompanyDetail(companyId = companyId.toString())
        observer()
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.ivBack -> {
                onBackPressed()
            }
        }
    }

    private fun observer() {
        viewModel.resultGetCompanyDetail.observe(this) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        binding.tvCompanyName.text = it.data?.data?.company_name
                        binding.tvBusinessType.text = it.data?.data?.type_of_business
                        binding.tvWebsite.text = it.data?.data?.company_website
                        binding.tvAddress.text = it.data?.data?.company_address
                        binding.tvCountry.text = it.data?.data?.company_country
                        binding.tvState.text = it.data?.data?.company_state
                        binding.tvCity.text = it.data?.data?.company_city
                        binding.tvZipcode.text = it.data?.data?.company_zip
                        binding.tvPhoneNumber.text = it.data?.data?.company_phone
                        binding.tvEmail.text = it.data?.data?.email
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(this)
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        showToast(it?.message.toString())
                    }
                }
            }
        }
    }
}