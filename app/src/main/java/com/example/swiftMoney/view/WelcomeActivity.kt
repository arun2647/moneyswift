package com.example.swiftMoney.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.swiftMoney.databinding.ActivityWelcomeBinding

class WelcomeActivity : AppCompatActivity(),View.OnClickListener{
    lateinit var binding:ActivityWelcomeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityWelcomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.onClick=this

    }

    override fun onClick(v: View?) {
        when(v){
            binding.btnGetStarted -> {
                startActivity(Intent(this,LoginActivity::class.java))
            }
        }
    }
}