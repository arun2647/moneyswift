package com.example.swiftMoney.view

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.activity.viewModels
import com.example.swiftMoney.adapter.CompanyStaffListAdapter
import com.example.swiftMoney.base.BaseActivity
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.ActivityStaffListBinding
import com.example.swiftMoney.model.companyStaffList.Data
import com.example.swiftMoney.utils.SharedPreference
import com.example.swiftMoney.utils.Status
import com.example.swiftMoney.utils.showToast
import com.example.swiftMoney.viewModel.CompanyViewModel
import com.example.swiftMoney.viewModel.ViewModelFactory

class StaffListActivity : BaseActivity(), View.OnClickListener {

    lateinit var binding: ActivityStaffListBinding

    private val viewModel by viewModels<CompanyViewModel> {
        ViewModelFactory(
            application,
            repository
        )
    }
    var staffList:ArrayList<Data> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStaffListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.onClick = this
        observer()
        val companyId=GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID,0)
        viewModel.getCompanyStaffList(companyId.toString())

        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(text: Editable?) {
                var list: ArrayList<Data> = ArrayList()
                if (text?.length!! >= 2) {
                    binding.rvStaffList.visibility = View.VISIBLE
                    for (i in staffList.indices) {
                        if (staffList[i].first_name.toLowerCase().contains(text)) {
                            if (!list.contains(staffList[i])) {
                                list.add(staffList[i])
                            }
                        }
                    }
                    setStaffListAdapter(list)
                } else {
                    setStaffListAdapter(staffList)
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.ivBack -> {
                onBackPressed()
            }
        }
    }

    private fun observer() {
        viewModel.resultGetCompanyStaffList.observe(this) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        it.data?.data?.let { it1 -> setStaffListAdapter(it1) }
                        it.data?.data?.let { it1 -> staffList.addAll(it1) }
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(this)
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        showToast(it?.message.toString())
                    }
                }
            }
        }
    }

    private fun setStaffListAdapter(body: List<Data>) {
        val adapter = CompanyStaffListAdapter(this, body)
        binding.rvStaffList.adapter=adapter
    }

}