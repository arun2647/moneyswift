package com.example.swiftMoney.view.transaction.newBeneficiary

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ClipData
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.example.swiftMoney.base.BaseFragment
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.FragmentNewBeneficiaryBankDetailBinding
import com.example.swiftMoney.model.submitTransaction.SubmitTransactionParamModel
import com.example.swiftMoney.utils.*
import com.example.swiftMoney.view.HomeActivity
import com.example.swiftMoney.viewModel.AuthViewModel
import com.example.swiftMoney.viewModel.ViewModelFactory
import com.example.swiftMoney.viewModel.WalletViewModel
import id.zelory.compressor.Compressor
import kotlinx.coroutines.launch
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class NewBeneficiaryBankDetailFragment : BaseFragment(), View.OnClickListener {

    lateinit var binding: FragmentNewBeneficiaryBankDetailBinding

    var amountTransfer: String = ""
    var beneficiaryName: String = ""
    var beneficiaryEmailAddress: String = ""
    var beneficiaryPhoneNumber: String = ""
    var beneficiaryAddress: String = ""
    var beneficiaryCity: String = ""
    var beneficiaryZipCode: String = ""
    var beneficiaryState: String = ""
    var beneficiaryCountry: String = ""
    var wallet: String = ""

    var individualList = listOf("Individual", "Business")
    var referenceList = listOf("Reference", "Memo")
    var selectedIndividual = ""
    var selectedReference = ""
    val viewModel by viewModels<WalletViewModel> { ViewModelFactory(application, repository) }

    var imageFile: File? = null
    private lateinit var uri: Uri
    private var idCard: ArrayList<File>? = ArrayList()
    lateinit var imageFromGallery: ActivityResultLauncher<String>
    lateinit var cameraPermission: ActivityResultLauncher<String>
    lateinit var galleryPermission: ActivityResultLauncher<String>

    private val authViewModel by viewModels<AuthViewModel> {
        ViewModelFactory(
            application,
            repository
        )
    }

    var bankCountry = ""
    var bankState = ""
    var countryListId: ArrayList<Int> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNewBeneficiaryBankDetailBinding.inflate(layoutInflater)
        binding.onClick = this
        observer()
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        amountTransfer = arguments?.getString("amountTransfer", "").toString()
        beneficiaryName = arguments?.getString("beneficiaryName", "").toString()
        beneficiaryEmailAddress = arguments?.getString("beneficiaryEmailAddress", "").toString()
        beneficiaryPhoneNumber = arguments?.getString("beneficiaryPhoneNumber", "").toString()
        beneficiaryAddress = arguments?.getString("beneficiaryAddress", "").toString()
        beneficiaryCity = arguments?.getString("beneficiaryCity", "").toString()
        beneficiaryCountry = arguments?.getString("beneficiaryCountry", "").toString()
        beneficiaryState = arguments?.getString("beneficiaryState", "").toString()
        beneficiaryZipCode = arguments?.getString("beneficiaryZipCode", "").toString()
        wallet = arguments?.getString("wallet", "").toString()
        binding.spinnerIndividualCategory.item = individualList
        binding.spinnerIndividualCategory.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    selectedIndividual = individualList[p2]
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

            }
        binding.spinnerReference.item = referenceList
        binding.spinnerReference.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    selectedReference = referenceList[p2]
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

            }
        authViewModel.getCountryList()
        selectedCountryAndStateSpinner()

        imageFromGallery = imageFromGallery()
        cameraPermission = cameraPermission()
        galleryPermission = galleryPermission()

        when (wallet) {
            "GBP" -> {
                binding.tiIBanNumber.visibility = View.VISIBLE
                binding.tvIbanNumber.visibility = View.VISIBLE
                binding.tvSortNumber.visibility = View.VISIBLE
                binding.tiSwiftCode.visibility = View.VISIBLE
                binding.tvBic.visibility = View.VISIBLE
                binding.tiBicNumber.visibility = View.VISIBLE
            }
            "USD" -> {
                binding.tvACHRouting.visibility = View.VISIBLE
                binding.tiAchRoutingNumber.visibility = View.VISIBLE
                binding.tvWireRouting.visibility = View.VISIBLE
                binding.tiWireRouting.visibility = View.VISIBLE
            }
            "EUR" -> {
                binding.tiIBanNumber.visibility = View.VISIBLE
                binding.tvIbanNumber.visibility = View.VISIBLE
                binding.tvBic.visibility = View.VISIBLE
                binding.tiBicNumber.visibility = View.VISIBLE
            }
            "CAD" -> {
                binding.tvACHRouting.visibility = View.VISIBLE
                binding.tiAchRoutingNumber.visibility = View.VISIBLE
                binding.tvWireRouting.visibility = View.VISIBLE
                binding.tiWireRouting.visibility = View.VISIBLE
                binding.tvBranchNumber.visibility = View.VISIBLE
                binding.tiBranchNumber.visibility = View.VISIBLE

            }
        }
    }

    private fun selectedCountryAndStateSpinner() {
        binding.spinnerCountry.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    p0: AdapterView<*>?,
                    p1: View?,
                    position: Int,
                    p3: Long
                ) {
                    val selectedCountryId = countryListId[position]
                    bankCountry = p0?.selectedItem.toString()
                    authViewModel.getStateList(selectedCountryId.toString())
                    binding.tvCountryError.visibility = View.GONE
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

            }

        binding.spinnerState.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                bankState = p0?.selectedItem.toString()
                binding.tvStateError.visibility = View.GONE
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnSubmit -> {
                if (validate()) {
                    hitSubmitTransactionApi()
                }
            }
            binding.rlUploadImage, binding.ivIdCard, binding.tvPdfName -> {
                onImagePickDialog()
            }

        }
    }

    fun validate(): Boolean {
        var isValid: Boolean = true
        if (TextUtils.isEmpty(binding.etName.text.toString())) {
            isValid = false
            binding.etName.error = "Enter bank name"
        }
        if (TextUtils.isEmpty(binding.etBankAddress.text.toString())) {
            isValid = false
            binding.etBankAddress.error = "Enter bank address"
        }
        if (bankCountry == "") {
            isValid = false
            requireContext().showToast("Select country")
        }
        if (bankState == "") {
            isValid = false
            requireContext().showToast("Select state")
        }
        if (TextUtils.isEmpty(binding.etCity.text.toString())) {
            isValid = false
            binding.etCity.error = "Enter city"
        }
        if (TextUtils.isEmpty(binding.etZipCode.text.toString())) {
            isValid = false
            binding.etZipCode.error = "Enter zip code"
        }
        if (TextUtils.isEmpty(binding.etAccountNumber.text.toString())) {
            isValid = false
            binding.etAccountNumber.error = "Enter account number"
        }
        when (wallet) {
            "GBP" -> {
                if (TextUtils.isEmpty(binding.etIBANNumber.text.toString())) {
                    isValid = false
                    binding.etIBANNumber.error = "Enter IBAN Number"
                }
                if (TextUtils.isEmpty(binding.etSortCode.text.toString())) {
                    isValid = false
                    binding.etSortCode.error = "Enter sort/swift code"
                }

            }

            "USD" -> {
                if (TextUtils.isEmpty(binding.etAchRoutingNumber.text.toString())) {
                    isValid = false
                    binding.etAchRoutingNumber.error = "Enter ACH routing number"
                }
                if (TextUtils.isEmpty(binding.etWireRoutingNumber.text.toString())) {
                    isValid = false
                    binding.etWireRoutingNumber.error = "Enter wire routing number"
                }
            }

            "EUR" -> {
                if (TextUtils.isEmpty(binding.etIBANNumber.text.toString())) {
                    isValid = false
                    binding.etIBANNumber.error = "Enter IBAN Number"
                }
                if (TextUtils.isEmpty(binding.etBIC.text.toString())) {
                    isValid = false
                    binding.etBIC.error = "Enter BIC"
                }
            }

            "CAD" -> {
                if (TextUtils.isEmpty(binding.etAchRoutingNumber.text.toString())) {
                    isValid = false
                    binding.etAchRoutingNumber.error = "Enter ACH routing number"
                }
                if (TextUtils.isEmpty(binding.etWireRoutingNumber.text.toString())) {
                    isValid = false
                    binding.etWireRoutingNumber.error = "Enter wire routing number"
                }
                if (TextUtils.isEmpty(binding.etBranchNumber.text.toString())) {
                    isValid = false
                    binding.etBranchNumber.error = "Enter branch number"
                }
            }
        }
        if (idCard?.isEmpty() == true) {
            isValid = false
            requireContext().showToast("Please attach document")
        }
        return isValid
    }

    private fun hitSubmitTransactionApi() {
        val companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
        val userId = GetObjects.preference.getInteger(SharedPreference.Key.USERID, 0)
        val bankName = binding.etName.text.toString()
        val bankAddress = binding.etBankAddress.text.toString()
        val bankZipcode = binding.etZipCode.text.toString()
        val bankCity = binding.etCity.text.toString()
        val bankAccount = binding.etAccountNumber.text.toString()
        val bankIBANNumber = binding.etIBANNumber.text.toString()
        val bankSwiftCode = binding.etSortCode.text.toString()
        val bankIBC = binding.etBIC.text.toString()
        val bankReference = selectedReference
        val bankBranchNumber = binding.etBranchNumber.text.toString()
        val achRoutingNumber = binding.etAchRoutingNumber.text.toString()
        val wireRoutingNumber = binding.etWireRoutingNumber.text.toString()

        val body = SubmitTransactionParamModel(
            "null",
            "New",
            beneficiaryName,
            beneficiaryEmailAddress,
            beneficiaryPhoneNumber,
            beneficiaryAddress,
            beneficiaryCity,
            beneficiaryState,
            beneficiaryCountry,
            beneficiaryZipCode,
            companyId.toString(),
            bankName,
            bankAddress,
            bankZipcode,
            bankCity,
            bankState,
            bankCountry,
            bankAccount,
            userId.toString(),
            wallet,
            amountTransfer,
            bankReference,
            selectedIndividual,
            bankIBC,
            bankBranchNumber,
            bankSwiftCode,
            bankIBANNumber,
            wireRoutingNumber,
            achRoutingNumber,
            "A-1.0",
            idCard
        )

        viewModel.onSubmitTransaction(body)
    }

    private fun observer() {
        viewModel.resultSubmitTransaction.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.data?.message.toString())
                        startActivity(Intent(requireContext(), HomeActivity::class.java))
                        requireActivity().finishAffinity()
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }

        authViewModel.resultCountryList.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        for (i in it.data?.message!!) {
                            countryListId.add(i.id)
                        }
                        binding.spinnerCountry.item =
                            it.data?.message?.distinctBy { it.country_name }
                                ?.map { it.country_name }
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }

        authViewModel.resultStateList.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        binding.spinnerState.item =
                            it.data?.message?.distinctBy { it.name }?.map { it.name }
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }
    }


    private fun imageFromGallery(): ActivityResultLauncher<String> {
        return registerForActivityResult(
            ActivityResultContracts.GetContent()
        ) {
            if (it != null) {
                binding.ivIdCard.visibility = View.VISIBLE
                binding.tvPdfName.visibility = View.GONE
                val imageFile = FilePath.getPath(requireContext(), it)
                idCard?.clear()
                idCard?.add(File(imageFile))
                Glide.with(this).load(imageFile).into(binding.ivIdCard)
                binding.rlUploadImage.visibility = View.GONE
            }
        }
    }

    private fun cameraPermission(): ActivityResultLauncher<String> {
        return registerForActivityResult(ActivityResultContracts.RequestPermission()) { it ->
            if (it) {
                /* imageFile = File.createTempFile(
                     "IMG_",
                     ".jpg",
                     requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                 )*/
                imageFile = createImageFile()

                uri = FileProvider.getUriForFile(
                    requireContext(),
                    "${requireActivity().packageName}.provider",
                    imageFile!!
                )

                takePicture.launch(uri)
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        requireActivity(),
                        Manifest.permission.CAMERA
                    )
                ) {
                    ActivityCompat.requestPermissions(
                        requireActivity(),
                        arrayOf(
                            Manifest.permission.CAMERA
                        ),
                        120
                    )

                } else {
                    val alertDialogBuilder = AlertDialog.Builder(requireContext())
                    alertDialogBuilder.setTitle("Change Permissions in Settings")
                    alertDialogBuilder
                        .setMessage(
                            """Click SETTINGS to Manually Set Permissions""".trimIndent()
                        )
                        .setCancelable(true)
                        .setPositiveButton("SETTINGS") { dialog, id ->
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri = Uri.fromParts("package", requireActivity().packageName, null)
                            intent.data = uri
                            startActivityForResult(intent, 150) // Comment 3.
                        }

                    val alertDialog = alertDialogBuilder.create()
                    alertDialog.show()
                }
            }
        }
    }


    private fun createImageFile(): File? {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES
        )
        val image = File.createTempFile(
            imageFileName,  // prefix
            ".jpg",  // suffix
            storageDir // directory
        )
        return image
    }


    private fun galleryPermission(): ActivityResultLauncher<String> {
        return registerForActivityResult(ActivityResultContracts.RequestPermission()) { it ->
            if (it) {
                imageFromGallery.launch("image/*")
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        requireActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                ) {
                    ActivityCompat.requestPermissions(
                        requireActivity(),
                        arrayOf(
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        120
                    )
                } else {
                    val alertDialogBuilder = AlertDialog.Builder(requireContext())
                    alertDialogBuilder.setTitle("Change Permissions in Settings")
                    alertDialogBuilder
                        .setMessage(
                            """Click SETTINGS to Manually Set Permissions""".trimIndent()
                        )
                        .setCancelable(true)
                        .setPositiveButton("SETTINGS") { dialog, id ->
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri = Uri.fromParts("package", requireActivity().packageName, null)
                            intent.data = uri
                            startActivityForResult(intent, 150) // Comment 3.
                        }

                    val alertDialog = alertDialogBuilder.create()
                    alertDialog.show()
                }
            }
        }
    }


    private fun onImagePickDialog() {
        val itemList = arrayOf<CharSequence>("Camera", "Gallery", "Document")
        val mDialog = AlertDialog.Builder(requireContext())
        mDialog.setTitle("Upload document")
        mDialog.setItems(itemList) { dialogInterface, i ->
            when {
                itemList[i] == "Camera" -> {
                    cameraPermission.launch(
                        Manifest.permission.CAMERA
                    )
                }
                itemList[i] == "Gallery" -> {
                    galleryPermission.launch(
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                }
                itemList[i] == "Document" -> {
                    if (documentCheckPermission()) {
                        showFileChooser()
                    } else {
                        ActivityCompat.requestPermissions(
                            requireActivity(),
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            111
                        )
                    }

                }
            }
        }

        mDialog.show()
    }

    private fun selectFromGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1)
    }


    private val takePicture =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { isSaved ->
            if (isSaved) {
                binding.tvPdfName.visibility = View.GONE
                binding.ivIdCard.visibility = View.VISIBLE
                idCard?.clear()
                this.lifecycleScope.launch {
                    val compressedImageFile = Compressor.compress(requireContext(), imageFile!!)
                    compressedImageFile?.let { idCard?.add(it) }
                }
                Log.d("image", ":$imageFile ")
                Glide.with(this).load(imageFile).into(binding.ivIdCard)
                binding.rlUploadImage.visibility = View.GONE
            }
        }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 120 && resultCode == AppCompatActivity.RESULT_OK) {
            if (data?.clipData != null) {
                val count = data.clipData?.itemCount ?: 0
                idCard?.clear()
                for (i in 0 until count) {
                    binding.ivIdCard.visibility = View.GONE
                    binding.tvPdfName.visibility = View.VISIBLE
                    val mClipData: ClipData? = data?.clipData
                    val imageFile = FileUtil.from(requireContext(), mClipData?.getItemAt(i)?.uri)
                    idCard?.add(imageFile)
                    Log.d("image", "onActivityResult: ${idCard?.get(i)}")
                    binding.rlUploadImage.visibility = View.GONE
                }
                val stringBuilder = StringBuilder()
                for (i in idCard?.indices!!) {
                    val filename: String =
                        idCard!![i]?.path?.substring(idCard!![i].path!!.lastIndexOf("/") + 1)
                            .toString()
                    if (i == idCard!!.lastIndex) {
                        stringBuilder.append(filename)
                    } else {
                        stringBuilder.append(filename + " ,")
                    }
                }
                binding.tvPdfName.text = stringBuilder
            }
            //If single image selected
            else if (data?.data != null) {
                val document = data.data;
                Log.d("singleImage", "onActivityResult: ${idCard?.get(0)}")
                binding.ivIdCard.visibility = View.GONE
                binding.tvPdfName.visibility = View.VISIBLE
                idCard?.clear()
                idCard?.add(FileUtil.from(requireContext(), document))
                val filename: String =
                    document?.path?.substring(document.path!!.lastIndexOf("/") + 1).toString()
                binding.tvPdfName.text = filename
                binding.rlUploadImage.visibility = View.GONE
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 111) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showFileChooser()
            } else {
                Toast.makeText(
                    requireContext(),
                    "Oops you just denied the permission",
                    Toast.LENGTH_LONG
                ).show();
            }
        }
    }

    private fun showFileChooser() {
        val intent = Intent()
        intent.type = "*/*"
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        val mimetypes = arrayOf(
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/msword", "application/pdf"
        )
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select document"), 120)
    }

    @SuppressLint("NewApi")
    private fun documentCheckPermission(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }
}