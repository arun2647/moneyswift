package com.example.swiftMoney.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.example.swiftMoney.base.BaseActivity
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.ActivityStaffDetailBinding
import com.example.swiftMoney.utils.Status
import com.example.swiftMoney.utils.showToast
import com.example.swiftMoney.viewModel.CompanyViewModel
import com.example.swiftMoney.viewModel.ViewModelFactory

class StaffDetailActivity : BaseActivity(), View.OnClickListener {

    var staffId: String = ""
    lateinit var binding: ActivityStaffDetailBinding


    private val viewModel by viewModels<CompanyViewModel> {
        ViewModelFactory(
            application,
            repository
        )
    }

    var fileUrl:String=""
    var documentUrl:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStaffDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.onClick = this
        staffId = intent.getIntExtra("staffId", 0).toString()
        viewModel.getStaffDetail(staffId.toString())
        observer()
    }

    private fun observer() {
        viewModel.resultGetStaffDetail.observe(this) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        binding.etFirstName.setText(it.data?.message?.get(0)?.first_name)
                        binding.etLastName.setText(it.data?.message?.get(0)?.last_name)
                        binding.etEmail.setText(it.data?.message?.get(0)?.email)
                        binding.etPhoneNumber.setText(it.data?.message?.get(0)?.phone_number.toString())
                        //binding.etAddress.setText(it.data?.message?.get(0)?.a)
                        fileUrl= it.data?.fileBaseurl.toString()
                        documentUrl=it.data?.message?.get(0)?.id_card.toString()
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(this)
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        showToast(it?.message.toString())
                    }
                }
            }
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.ivBack -> {
                onBackPressed()
            }
            binding.tvViewDocument -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(fileUrl+documentUrl))
                startActivity(browserIntent)
            }
        }
    }
}