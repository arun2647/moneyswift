package com.example.swiftMoney.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.example.swiftMoney.R
import com.example.swiftMoney.databinding.FragmentSubmitTransactionBinding


class SubmitTransactionFragment : Fragment(),View.OnClickListener{

    lateinit var navController: NavController
    lateinit var binding: FragmentSubmitTransactionBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSubmitTransactionBinding.inflate(layoutInflater)
        binding.onClick=this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = requireActivity().findNavController(R.id.submitTransactionHost1)
    }
    override fun onClick(v: View?) {
        when(v){
            binding.llNew -> {
                binding.llNew.background= ContextCompat.getDrawable(requireContext(), R.drawable.bg_yellow_radius)
                binding.llExisting.background= ContextCompat.getDrawable(requireContext(), R.drawable.bg_white_radius)
                binding.tvExistingBeneficiary.setTextColor(ContextCompat.getColor(requireContext(), R.color.txt_color_grey))
                binding.tvNewBeneficiary.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
                navController.popBackStack(R.id.existingBeneficiaryPersonalInfoFragment,true)
                navController.navigate(R.id.newBeneficiaryPersonalInfoFragment)
            }
            binding.llExisting -> {
                binding.llNew.background= ContextCompat.getDrawable(requireContext(), R.drawable.bg_white_radius)
                binding.llExisting.background= ContextCompat.getDrawable(requireContext(), R.drawable.bg_yellow_radius)
                binding.tvNewBeneficiary.setTextColor(ContextCompat.getColor(requireContext(), R.color.txt_color_grey))
                binding.tvExistingBeneficiary.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
                navController.popBackStack(R.id.newBeneficiaryPersonalInfoFragment,true)
                navController.navigate(R.id.existingBeneficiaryPersonalInfoFragment)

            }
        }
    }


}