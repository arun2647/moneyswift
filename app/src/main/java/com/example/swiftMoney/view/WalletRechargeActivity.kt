package com.example.swiftMoney.view

import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import com.example.swiftMoney.base.BaseActivity
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.ActivityWalletRechargeBinding
import com.example.swiftMoney.model.walletRecharge.WalletRechargeParamModel
import com.example.swiftMoney.utils.DatePickerDialog
import com.example.swiftMoney.utils.SharedPreference
import com.example.swiftMoney.utils.Status
import com.example.swiftMoney.utils.showToast
import com.example.swiftMoney.viewModel.ViewModelFactory
import com.example.swiftMoney.viewModel.WalletViewModel
import com.google.android.material.slider.RangeSlider
import java.text.SimpleDateFormat
import java.util.*

class WalletRechargeActivity : BaseActivity(), View.OnClickListener {

    lateinit var binding: ActivityWalletRechargeBinding
    private val viewModel by viewModels<WalletViewModel> {
        ViewModelFactory(
            application,
            repository
        )
    }
    private var currencyList = arrayListOf<String>("USD", "GBP", "EUR", "CAD")
    var amount: String = ""
    var amount2: String = ""
    var selectedCurrency: String = ""
    var selectedTransactionDate: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWalletRechargeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.onClick = this
        observer()
        binding.spinnerCountry.item = currencyList.toMutableList() as List<Any>?
        binding.rangeSlider.addOnSliderTouchListener(object : RangeSlider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: RangeSlider) {
                val values = slider.values
                binding.tvAmountValue.text = "0k - " + values[0].toInt() + "k"
            }

            override fun onStopTrackingTouch(slider: RangeSlider) {
                val values = slider.values
                binding.tvAmountValue.text = "0k - " + values[0].toInt() + "k"
                amount = values[0].toInt().toString() + "000"
                amount2 = values[0].toString()
            }
        })
        binding.spinnerCountry.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    selectedCurrency = currencyList[p2]
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

            }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnRecharge -> {
                if (validate()) {
                    hitWalletRechargeApi()
                }
            }
            binding.rlTransactionDate -> {
                DatePickerDialog(dateCallback).show(supportFragmentManager, "")
            }
            binding.ivBack -> {
                onBackPressed()
            }
        }
    }

    private val dateCallback = object : DatePickerDialog.DatePickerCallback {
        @RequiresApi(Build.VERSION_CODES.O)
        override fun getDate(year: Int, month: Int, day: Int) {
            val instance = Calendar.getInstance()
            instance.set(year, month, day)
            val simpleDateFormat = SimpleDateFormat("MMM dd,yyyy", Locale.ENGLISH)
            binding.tvDate.text = simpleDateFormat.format(instance.time)
            selectedTransactionDate = binding.tvDate.text.toString()
        }
    }

    fun validate(): Boolean {
        var isValid: Boolean = true
        when {
            selectedCurrency == "" -> {
                isValid = false
                showToast("Select currency")
            }
            amount2 == "0.0" -> {
                isValid = false
                showToast("Select amount")
            }
            amount2 == "0.0" -> {
                isValid = false
                showToast("Select amount")
            }
            TextUtils.isEmpty(binding.etReferenceNumber.text.toString()) -> {
                isValid = false
                showToast("Enter reference number")
            }
            selectedTransactionDate == "" -> {
                isValid = false
                showToast("select Transaction Date")
            }
        }
        return isValid
    }

    private fun hitWalletRechargeApi() {
        val userId = GetObjects.preference.getInteger(SharedPreference.Key.USERID, 0)
        val companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
        val referenceNumber = binding.etReferenceNumber.text.toString()
        val referenceDate = binding.tvDate.text.toString()
        val comment = binding.etComments.text.toString()
        val body = WalletRechargeParamModel(
            userId.toString(),
            companyId.toString(),
            amount,
            selectedCurrency,
            referenceNumber,
            referenceDate,
            comment
        )
        viewModel.onWalletRecharge(body)
    }


    private fun observer() {
        viewModel.resultWalletRecharge.observe(this) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        showToast("Hooray, your wallet recharge request was successfully submitted for $selectedCurrency $amount. Please contact Money Swift for approval and to ensure it reflects in your balance!!!",Toast.LENGTH_LONG)
                        onBackPressed()
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(this)
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        showToast(it?.message.toString())
                    }
                }
            }
        }
    }


}