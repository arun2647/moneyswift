package com.example.swiftMoney.view.transaction.newBeneficiary

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.swiftMoney.R
import com.example.swiftMoney.base.BaseFragment
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.FragmentNewBeneficiaryPersonalInfoBinding
import com.example.swiftMoney.model.transactionFee.TransactionFeeParamModel
import com.example.swiftMoney.utils.SharedPreference
import com.example.swiftMoney.utils.Status
import com.example.swiftMoney.utils.showToast
import com.example.swiftMoney.viewModel.CompanyViewModel
import com.example.swiftMoney.viewModel.ViewModelFactory


class NewBeneficiaryPersonalInfoFragment : BaseFragment(), View.OnClickListener {

    lateinit var binding: FragmentNewBeneficiaryPersonalInfoBinding
    var walletList = listOf("GBP", "USD", "EUR", "CAD")
    var selectedWallet: String = ""
    val viewModel by viewModels<CompanyViewModel> { ViewModelFactory(application, repository) }
    var gbpBalance = ""
    var usdBalance = ""
    var eurBalance = ""
    var cadBalance = ""
    var fee: String = ""
    var feeType:String=""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNewBeneficiaryPersonalInfoBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.onClick = this
        observer()
        val companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
        viewModel.getBalance(companyId.toString())
        binding.spinnerWallet.item = walletList
        binding.spinnerWallet.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                when (position) {
                    0 -> {
                        if (gbpBalance == "") {
                            binding.tvBalance.text = "No balance"
                        } else {
                            binding.tvBalance.text = gbpBalance
                        }

                    }
                    1 -> {
                        if (usdBalance == "") {
                            binding.tvBalance.text = "No balance"
                        } else {
                            binding.tvBalance.text = usdBalance
                        }

                    }
                    2 -> {
                        if (eurBalance == "") {
                            binding.tvBalance.text = "No balance"
                        } else {
                            binding.tvBalance.text = eurBalance
                        }
                    }
                    3 -> {
                        if (cadBalance == "") {
                            binding.tvBalance.text = "No balance"
                        } else {
                            binding.tvBalance.text = cadBalance
                        }

                    }
                }
                selectedWallet = walletList[position]
                hitTransactionApi()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }

        binding.etAmountTransfer.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.length!! >= 1) {
                    if (selectedWallet != "") {
                        if (feeType=="percentage"){
                            try {
                                val transferAmount=binding.etAmountTransfer.text.toString()
                                val percentageAmount=transferAmount.toDouble().times(fee.toDouble()).div(100)
                                binding.tvTransferFee.text= "$fee%"
                                binding.tvChargeAmount.text= transferAmount.toDouble().plus(percentageAmount).toString()
                            }catch (e:java.lang.Exception){
                                e.printStackTrace()
                            }

                        }else{
                            val transferAmount=binding.etAmountTransfer.text.toString()
                            binding.tvTransferFee.text= "$fee"
                            binding.tvChargeAmount.text= transferAmount.toInt().plus(fee.toInt()).toString()
                        }
                    } else {
                        binding.etAmountTransfer.setText("")
                        requireContext().showToast("First you have to select wallet")
                    }
                } else {
                    binding.tvTransferFee.text = "-"
                    binding.tvChargeAmount.text = "-"
                }
            }

            override fun afterTextChanged(p0: Editable?) {

            }

        })
        binding.etAmountTransfer.setOnEditorActionListener(object :
            TextView.OnEditorActionListener {
            override fun onEditorAction(p0: TextView?, actionId: Int, p2: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                   if (feeType=="percentage"){
                       try {
                           val transferAmount=binding.etAmountTransfer.text.toString()
                           val percentageAmount=transferAmount.toDouble().times(fee.toDouble()).div(100)
                           binding.tvTransferFee.text= "$fee%"
                           binding.tvChargeAmount.text= transferAmount.toDouble().plus(percentageAmount).toString()
                       }catch (e:java.lang.Exception){
                           e.printStackTrace()
                       }

                   }else{
                       val transferAmount=binding.etAmountTransfer.text.toString()
                       binding.tvTransferFee.text= "$fee"
                       binding.tvChargeAmount.text= transferAmount.toInt().plus(fee.toInt()).toString()
                   }
                    return true
                }
                return false
            }

        })

    }


    private fun hitTransactionApi() {
        val companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
        val amount = binding.etAmountTransfer.text.toString()
        val body = TransactionFeeParamModel(companyId.toString(), selectedWallet, amount)
        viewModel.getTransactionFee(body)
    }

    fun validate(): Boolean {
        var isValid = true
        if (selectedWallet == "") {
            isValid = false
            requireContext().showToast("Select wallet")
        }
        if (TextUtils.isEmpty(binding.etAmountTransfer.text.toString())) {
            isValid = false
            binding.etAmountTransfer.error = "Enter amount to transfer"
        }
        if (binding.etAmountTransfer.text.toString()
                .isNotEmpty() && binding.tvBalance.text.toString().toDouble()
                .toInt() < binding.etAmountTransfer.text.toString()
                .toInt()
        ) {
            isValid = false
            requireContext().showToast("You don't have enough balance")
        }
        return isValid
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnNext -> {
                if (validate()) {
                    val bundle = Bundle()
                    val amountTransfer = binding.etAmountTransfer.text.toString()
                    bundle.putString("amountTransfer", amountTransfer)
                    bundle.putString("wallet", selectedWallet)
                    findNavController().navigate(
                        R.id.action_newBeneficiaryPersonalInfoFragment_to_newBeneficiaryInformationFragment,
                        bundle
                    )
                }
            }
        }
    }


    private fun observer() {
        viewModel.resultGetBalance.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        val gbpList = it.data?.message?.filter { it.currency == "GBP" }
                        val eurList = it.data?.message?.filter { it.currency == "EUR" }
                        val usdList = it.data?.message?.filter { it.currency == "USD" }
                        val cadList = it.data?.message?.filter { it.currency == "CAD" }
                        try {
                            gbpBalance = gbpList?.get(0)?.balance.toString()

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        try {
                            eurBalance = eurList?.get(0)?.balance.toString()
                        }catch (e:java.lang.Exception){
                            e.printStackTrace()
                        }

                        try {
                            usdBalance = usdList?.get(0)?.balance.toString()
                        }catch (e:java.lang.Exception){
                            e.printStackTrace()
                        }
                        try {
                            cadBalance = cadList?.get(0)?.balance.toString()
                        }catch (e:java.lang.Exception){
                            e.printStackTrace()
                        }



                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }


        viewModel.resultGetTransactionFee.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        fee = it.data?.data?.fees.toString()
                        feeType = it.data?.data?.fee_type.toString()
                    }
                    Status.LOADING -> {
                        // MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }
    }
}