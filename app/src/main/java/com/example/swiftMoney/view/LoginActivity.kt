package com.example.swiftMoney.view

import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import com.example.swiftMoney.BuildConfig
import com.example.swiftMoney.base.BaseActivity
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.ActivityLoginBinding
import com.example.swiftMoney.model.login.LoginParamModel
import com.example.swiftMoney.utils.*
import com.example.swiftMoney.viewModel.AuthViewModel
import com.example.swiftMoney.viewModel.ViewModelFactory
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import org.json.JSONException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class LoginActivity : BaseActivity(), View.OnClickListener, CallbackGoogleSignIn {

    lateinit var binding: ActivityLoginBinding
    private val viewModel by viewModels<AuthViewModel> { ViewModelFactory(application, repository) }

    var gso: GoogleSignInOptions? = null
    private var mGoogleSignInClient: GoogleSignInClient? = null
    var RC_SIGN_IN = 101
    private var callbackManager: CallbackManager? = null
    var mAuth: FirebaseAuth? = null
    var googleSignIn: GoogleSIgnIn? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        binding.onClick = this
        setContentView(binding.root)
        callbackManager = CallbackManager.Factory.create()
        FacebookSdk.sdkInitialize(applicationContext)
        googleSignIn = GoogleSIgnIn.getInstance(this, BuildConfig.FIREBASE_CLIENT_ID)
        initFacebook()
        observer()
        GoogleSIgnIn.callBack = this
        printHashKey(this)
    }

    private fun hitLoginApi() {
        val email = binding.etEmail.text.toString()
        val password = binding.etPassword.text.toString()
        viewModel.onLogin(LoginParamModel(email, password, "4", ""))
    }

    private fun validation(): Boolean {
        var isValid: Boolean = true
        if (TextUtils.isEmpty(binding.etEmail.text.toString())) {
            isValid = false
            binding.etEmail.error = "Enter your email"
        } else if (!Utility.emailValidation(binding.etEmail)) {
            isValid = false
            binding.etEmail.error = "Enter your valid email"
        }
        if (TextUtils.isEmpty(binding.etPassword.text.toString())) {
            isValid = false
            binding.etPassword.error = "Enter your password"
        }
        return isValid
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnSignUp -> {
                startActivity(Intent(this, RegisterActivity::class.java))
            }
            binding.tvForgotPassword -> {
                startActivity(Intent(this, ForgotPasswordActivity::class.java))
            }
            binding.btnLogin -> {
                if (validation()) {
                    hitLoginApi()
                }
            }
            binding.btnGoogle -> {
                val intent = GoogleSIgnIn.mGoogleSignInClient?.signInIntent
                startActivityForResult(intent, GoogleSIgnIn.RC_CODE)
                // FirebaseApp.initializeApp(this)
            }
            binding.btnFacebook -> {
                LoginManager.getInstance().logInWithReadPermissions(
                    this,
                    listOf("email", "public_profile", "user_friends")
                )
            }
        }
    }

    private fun observer() {
        viewModel.resultLogin.observe(this) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        GetObjects.preference.put(SharedPreference.Key.ISUSERLOGIN, true)
                        GetObjects.preference.put(
                            SharedPreference.Key.TOKEN,
                            it.data?.data?.access_token
                        )
                        GetObjects.preference.put(
                            SharedPreference.Key.USERID,
                            it.data?.data?.user_id!!
                        )
                        GetObjects.preference.put(
                            SharedPreference.Key.COMPANYID,
                            it.data?.data?.company_id
                        )
                        GetObjects.preference.put(SharedPreference.Key.EMAIL, it.data.data?.email)
                        val companyId = it.data.data.company_id
                        if (companyId == 0) {
                            val intent = Intent(this, RegisterActivity::class.java)
                            intent.putExtra("email", it.data.data.email)
                            intent.putExtra("userId", it.data.data.user_id)
                            intent.putExtra("isFromSocialLogin", true)
                            startActivity(intent)
                            finishAffinity()
                        } else {
                            startActivity(Intent(this, HomeActivity::class.java))
                            finishAffinity()
                        }
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(this)
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        showToast(it?.message.toString())
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager?.onActivityResult(requestCode, resultCode, data)
        googleSignIn?.onActivityResult(requestCode, resultCode, data)
    }


    private fun initFacebook() {

        LoginManager.getInstance().registerCallback(callbackManager, object :
            FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {

                val request = GraphRequest.newMeRequest(result?.accessToken) { obj, _ ->
                    try {
                        var email: String? = ""
                        var firstName: String? = ""
                        var lastName: String? = ""
                        var id: String? = ""
                        id = obj.getString("id")
                        if (obj.has("email")) {
                            email = obj.getString("email")
                        }
                        if (obj.has("first_name")) {
                            firstName = obj.getString("first_name")
                        }
                        if (obj.has("last_name")) {
                            lastName = obj.getString("last_name")
                        }
                        if (!email.isNullOrEmpty()) {
                            viewModel.onLogin(
                                LoginParamModel(
                                    email.toString(),
                                    "asdf",
                                    "2",
                                    id
                                )
                            )
                        } else {
                            Toast.makeText(
                                this@LoginActivity,
                                "Please setup your email in your Facebook Account",
                                Toast.LENGTH_LONG
                            ).show()
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
                val parameters = Bundle()
                parameters.putString("fields", "id,email,first_name,last_name")
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() {

            }

            override fun onError(error: FacebookException?) {

                Toast.makeText(
                    this@LoginActivity,
                    error?.message, Toast.LENGTH_SHORT
                ).show()
                Log.e("LoginActivity", error?.cause.toString())
            }
        })
    }

    override fun onSuccessGoogleSignIn(result: FirebaseUser?, name: String?) {
        viewModel.onLogin(
            LoginParamModel(
                result?.email.toString(),
                "asdf",
                "1",
                result?.providerId.toString()
            )
        )
    }

    override fun onFailureGoogleSignIn(exception: Exception?) {
        MyApplication.hideLoader()
        Toast.makeText(
            this,
            exception?.message + "Something went wrong",
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun onApiException(message: String?) {
        MyApplication.hideLoader()
        if (message != "12501: ") {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun printHashKey(pContext: Context) {
        try {
            val info: PackageInfo = pContext.getPackageManager()
                .getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey: String = String(Base64.encode(md.digest(), 0))
                Log.i("TAG", "printHashKey() Hash Key: $hashKey")
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e("TAG", "printHashKey()", e)
        } catch (e: Exception) {
            Log.e("TAG", "printHashKey()", e)
        }
    }
}