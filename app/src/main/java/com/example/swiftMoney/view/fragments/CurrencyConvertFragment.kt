package com.example.swiftMoney.view.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.swiftMoney.base.BaseFragment
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.FragmentCurrencyConvertBinding
import com.example.swiftMoney.model.convertCurrency.ConvertCurrencyParamModel
import com.example.swiftMoney.model.currencyConversionRate.CurrencyConversionRateParamModel
import com.example.swiftMoney.model.currencyConversionRate.CurrencyConversionRateResponse
import com.example.swiftMoney.network.Repository
import com.example.swiftMoney.utils.SharedPreference
import com.example.swiftMoney.utils.Status
import com.example.swiftMoney.utils.showToast
import com.example.swiftMoney.viewModel.CompanyViewModel
import com.example.swiftMoney.viewModel.ViewModelFactory
import com.example.swiftMoney.viewModel.WalletViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CurrencyConvertFragment : BaseFragment(), View.OnClickListener {

    private val currencyList = arrayOf("GBP", "USD", "EUR", "CAD")
    lateinit var binding: FragmentCurrencyConvertBinding
    val viewModel: WalletViewModel by viewModels { ViewModelFactory(application, repository) }
    val companyViewModel by viewModels<CompanyViewModel> {
        ViewModelFactory(
            application,
            repository
        )
    }

    var selectCurrencyFrom: String = ""
    var selectCurrencyTo: String = ""
    var isConvertedFromFirstEditText: Boolean = false
    var currencyFromChanged: Boolean = false
    var currencyToChanged: Boolean = false
    var gbpBalance = ""
    var usdBalance = ""
    var eurBalance = ""
    var cadBalance = ""
    var isClick = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCurrencyConvertBinding.inflate(layoutInflater)

        binding.onClick = this

        return binding.root
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()
        val companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
        companyViewModel.getBalance(companyId.toString())
        val spinnerCurrencyAdapter = ArrayAdapter(
            requireContext(),
            androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,
            currencyList
        )


        binding.spinnerCurrencyOne.adapter = spinnerCurrencyAdapter
        binding.spinnerCurrencyTwo.adapter = spinnerCurrencyAdapter


        binding.spinnerCurrencyOne.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    selectCurrencyFrom = currencyList[p2]
                    isClick = true
                    when (p2) {
                        0 -> {
                            if (gbpBalance == "") {
                                binding.tvBalance.text = "Your Balance: 0"
                            } else {
                                binding.tvBalance.text = "Your Balance: $gbpBalance"
                            }
                        }
                        1 -> {
                            if (usdBalance == "") {
                                binding.tvBalance.text = "Your Balance: 0"
                            } else {
                                binding.tvBalance.text = "Your Balance: $usdBalance"
                            }
                        }
                        2 -> {
                            if (eurBalance == "") {
                                binding.tvBalance.text = "Your Balance: 0"
                            } else {
                                binding.tvBalance.text = "Your Balance: $eurBalance"
                            }
                        }
                        3 -> {
                            if (cadBalance == "") {
                                binding.tvBalance.text = "Your Balance: 0"
                            } else {
                                binding.tvBalance.text = "Your Balance: $cadBalance"
                            }
                        }
                    }
                    currencyFromChanged = true
                    currencyToChanged = false
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

            }

        binding.spinnerCurrencyTwo.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    selectCurrencyTo = currencyList[p2]

                    getConversionRate(selectCurrencyFrom, selectCurrencyTo)
                    currencyFromChanged = false
                    currencyToChanged = true
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

            }


        binding.etCurrencyTo.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0!!.length > 0) {
                    isConvertedFromFirstEditText = false
                    isClick = true
                }
            }

        })

        binding.etCurrencyFrom.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0!!.length > 0) {
                    isConvertedFromFirstEditText = true
                    isClick = true
                }
            }

        })


        binding.etCurrencyFrom.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(p0: TextView?, actionId: Int, p2: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    isConvertedFromFirstEditText = true

                    if (binding.etCurrencyFrom.text.isNotEmpty()) {
                        getConversionRate(selectCurrencyFrom, selectCurrencyTo)
                    } else {
                        requireContext().showToast("Please enter amount")
                    }
                    return true
                }
                return false
            }

        })

        binding.etCurrencyTo.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(p0: TextView?, actionId: Int, p2: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    isConvertedFromFirstEditText = false
                    if (binding.etCurrencyTo.text.isNotEmpty()) {
                        getConversionRate(selectCurrencyFrom, selectCurrencyTo)
                    } else {
                        requireContext().showToast("Please enter amount")
                    }
                    //  getConversionRate(selectCurrencyTo, selectCurrencyFrom)
                    return true
                }
                return false
            }

        })


    }

    fun getConversionRate(currencyFrom: String, currencyTo: String) {
        val companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
        /* viewModel.getConversionRate(
             CurrencyConversionRateParamModel(
                 companyId.toString(),
                 currencyFrom,
                 currencyTo
             )
         )*/
        Repository().getConversionRate(
            CurrencyConversionRateParamModel(
                companyId.toString(),
                currencyFrom,
                currencyTo
            )
        ).enqueue(object : Callback<CurrencyConversionRateResponse> {
            override fun onResponse(
                call: Call<CurrencyConversionRateResponse>,
                response: Response<CurrencyConversionRateResponse>
            ) {
                val it = response.body()?.data
                var conversionRatUSD = it?.USD
                var conversionRateGBP = it?.GBP
                var conversionRateEUR = it?.EUR
                var conversionRateCAD = it?.CAD

                if (binding.etCurrencyFrom.text.toString()
                        .isNotEmpty() || binding.etCurrencyTo.text.toString().isNotEmpty()
                ) {

                    if (isConvertedFromFirstEditText) {
                        val firstCurrencyAmount = binding.etCurrencyFrom.text.toString()

                        var convertedAmountUSD = 0.0

                        if (it?.USD != 0.0) {
                            convertedAmountUSD =
                                firstCurrencyAmount.toDouble().toLong().times(conversionRatUSD!!)
                            convertedAmountUSD =
                                String.format("%.3f", convertedAmountUSD).toDouble()
                            binding.etCurrencyTo.setText(convertedAmountUSD.toDouble().toString())
                            binding.tvConvertAmount.text =
                                "$firstCurrencyAmount $selectCurrencyFrom = $convertedAmountUSD $selectCurrencyTo"
                            conversionRatUSD = String.format("%.3f", conversionRatUSD).toDouble()
                            binding.tvConversionRate.text = "Conversion Rate: $conversionRatUSD"
                            isClick = false
                        }

                        var convertedAmountGBP = 0.0
                        if (it?.GBP != 0.0) {
                            convertedAmountGBP =
                                firstCurrencyAmount.toDouble().toLong().times(conversionRateGBP!!)
                            convertedAmountGBP =
                                String.format("%.3f", convertedAmountGBP).toDouble()
                            binding.etCurrencyTo.setText(convertedAmountGBP.toString())
                            binding.tvConvertAmount.text =
                                "$firstCurrencyAmount $selectCurrencyFrom = $convertedAmountGBP $selectCurrencyTo"
                            conversionRateGBP = String.format("%.3f", conversionRateGBP).toDouble()
                            binding.tvConversionRate.text = "Conversion Rate: $conversionRateGBP"
                            isClick = false
                        }

                        var convertedAmountEUR = 0.0
                        if (it?.EUR != 0.0) {
                            convertedAmountEUR =
                                firstCurrencyAmount.toDouble().toLong().times(conversionRateEUR!!)
                            convertedAmountEUR =
                                String.format("%.3f", convertedAmountEUR).toDouble()
                            binding.etCurrencyTo.setText(convertedAmountEUR.toString())
                            binding.tvConvertAmount.text =
                                "$firstCurrencyAmount $selectCurrencyFrom = $convertedAmountEUR $selectCurrencyTo"
                            conversionRateEUR = String.format("%.3f", conversionRateEUR).toDouble()
                            binding.tvConversionRate.text = "Conversion Rate: $conversionRateEUR"
                            isClick = false

                        }

                        var convertedAmountCAD = 0.0
                        if (it?.CAD != 0.0) {
                            convertedAmountCAD =
                                firstCurrencyAmount.toDouble().toLong().times(conversionRateCAD!!)
                            convertedAmountCAD =
                                String.format("%.3f", convertedAmountCAD).toDouble()
                            binding.etCurrencyTo.setText(convertedAmountCAD.toString())
                            binding.tvConvertAmount.text =
                                "$firstCurrencyAmount $selectCurrencyFrom = $convertedAmountCAD $selectCurrencyTo"
                            conversionRateCAD = String.format("%.3f", conversionRateCAD).toDouble()
                            binding.tvConversionRate.text = "Conversion Rate: $conversionRateCAD"
                            isClick = false
                        }

                    } else {
                        val secondCurrencyAmount = binding.etCurrencyTo.text.toString()
                        var secondConvertedAmountUSD = 0.0
                        if (it?.USD != 0.0) {
                            secondConvertedAmountUSD =
                                secondCurrencyAmount.toDouble().toLong().times(conversionRatUSD!!)
                            secondConvertedAmountUSD =
                                String.format("%.3f", secondConvertedAmountUSD).toDouble()
                            binding.etCurrencyFrom.setText(secondConvertedAmountUSD.toString())
                            binding.tvConvertAmount.text =
                                "$secondCurrencyAmount $selectCurrencyTo = $secondConvertedAmountUSD $selectCurrencyFrom"
                            conversionRatUSD = String.format("%.3f", conversionRatUSD).toDouble()
                            binding.tvConversionRate.text = "Conversion Rate: $conversionRatUSD"
                            isClick = false
                        }

                        var secondConvertedAmountGBP = 0.0
                        if (it?.GBP != 0.0) {
                            secondConvertedAmountGBP =
                                secondCurrencyAmount.toDouble().toLong().times(conversionRateGBP!!)
                            secondConvertedAmountGBP =
                                String.format("%.3f", secondConvertedAmountGBP).toDouble()
                            binding.etCurrencyFrom.setText(secondConvertedAmountGBP.toString())
                            binding.tvConvertAmount.text =
                                "$secondCurrencyAmount $selectCurrencyTo = $secondConvertedAmountGBP $selectCurrencyFrom"
                            conversionRateGBP = String.format("%.3f", conversionRateGBP).toDouble()
                            binding.tvConversionRate.text = "Conversion Rate: $conversionRateGBP"
                            isClick = false
                        }

                        var secondConvertedAmountEUR = 0.0
                        if (it?.EUR != 0.0) {
                            secondConvertedAmountEUR =
                                secondCurrencyAmount.toDouble().toLong().times(conversionRateEUR!!)
                            secondConvertedAmountEUR =
                                String.format("%.3f", secondConvertedAmountEUR).toDouble()
                            binding.etCurrencyFrom.setText(secondConvertedAmountEUR.toString())
                            binding.tvConvertAmount.text =
                                "$secondCurrencyAmount $selectCurrencyTo = $secondConvertedAmountEUR $selectCurrencyFrom"
                            conversionRateEUR = String.format("%.3f", conversionRateEUR).toDouble()
                            binding.tvConversionRate.text = "Conversion Rate: $conversionRateEUR"
                            isClick = false

                        }

                        var secondConvertedAmountCAD = 0.0
                        if (it?.CAD != 0.0) {
                            secondConvertedAmountCAD =
                                secondCurrencyAmount.toDouble().toLong().times(conversionRateCAD!!)
                            secondConvertedAmountCAD =
                                String.format("%.3f", secondConvertedAmountCAD).toDouble()
                            binding.etCurrencyFrom.setText(secondConvertedAmountCAD.toString())
                            binding.tvConvertAmount.text =
                                "$secondCurrencyAmount $selectCurrencyTo = $secondConvertedAmountCAD $selectCurrencyFrom"
                            conversionRateCAD = String.format("%.3f", conversionRateCAD).toDouble()
                            binding.tvConversionRate.text = "Conversion Rate: $conversionRateCAD"
                            isClick = false
                        }
                    }

                } else {
                    if (conversionRatUSD != 0.0) {
                        try {
                            conversionRatUSD = String.format("%.3f", conversionRatUSD).toDouble()
                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }
                        binding.tvConversionRate.text = "Conversion Rate: $conversionRatUSD"
                    }
                    if (conversionRateCAD != 0.0) {
                        try {
                            conversionRateCAD = String.format("%.3f", conversionRateCAD).toDouble()
                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }
                        binding.tvConversionRate.text = "Conversion Rate: $conversionRateCAD"
                    }
                    if (conversionRateEUR != 0.0) {
                        try {
                            conversionRateEUR = String.format("%.3f", conversionRateEUR).toDouble()
                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }
                        binding.tvConversionRate.text = "Conversion Rate: $conversionRateEUR"
                    }
                    if (conversionRateGBP != 0.0) {
                        try {
                            conversionRateGBP = String.format("%.3f", conversionRateGBP).toDouble()
                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }
                        binding.tvConversionRate.text = "Conversion Rate: $conversionRateGBP"
                    }
                }
            }

            override fun onFailure(call: Call<CurrencyConversionRateResponse>, t: Throwable) {}
        })

    }


    override fun onClick(v: View?) {
        when (v) {
            binding.rlGetConversionAmount -> {
                if (!binding.etCurrencyFrom.text.isNullOrEmpty() || !binding.etCurrencyTo.text.isNullOrEmpty()) {
                    if (currencyFromChanged) {
                        if (isClick) {
                            isClick = false
                            getConversionRate(selectCurrencyFrom, selectCurrencyTo)
                        }

                    } else {
                        if (isClick) {
                            isClick = false
                            getConversionRate(selectCurrencyFrom, selectCurrencyTo)
                        }

                    }
                }
            }

            binding.btnConvertCurrency -> {
                if (binding.etCurrencyFrom.text.isNotEmpty() || binding.etCurrencyTo.text.toString()
                        .isNotEmpty()
                ) {
                    if (binding.tvBalance.text.toString()!="Your Balance: 0"){
                        hitConvertCurrencyApi()
                    }else{
                        Toast.makeText(requireContext(), "Insufficient balance...", Toast.LENGTH_SHORT)
                            .show()
                    }

                } else {
                    Toast.makeText(requireContext(), "Please enter amount", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    private fun hitConvertCurrencyApi() {
        val companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
        val userId = GetObjects.preference.getInteger(SharedPreference.Key.USERID, 0)
        val body = ConvertCurrencyParamModel(
            selectCurrencyFrom,
            selectCurrencyTo,
            binding.etCurrencyFrom.text.toString(),
            userId.toString(),
            companyId.toString()
        )
        viewModel.onConvertCurrency(body = body)
    }


    private fun observer() {
        viewModel.resultConvertCurrency.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        binding.etCurrencyTo.setText("")
                        binding.etCurrencyFrom.setText("")
                        binding.tvConvertAmount.text = ""
                        requireContext().showToast(it?.data?.message.toString())
                        val companyId =
                            GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
                        companyViewModel.getBalance(companyId.toString())
                        binding.tvConversionRate.text = "Conversion Rate:  "
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }


        companyViewModel.resultGetBalance.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        val gbpList = it.data?.message?.filter { it.currency == "GBP" }
                        val eurList = it.data?.message?.filter { it.currency == "EUR" }
                        val usdList = it.data?.message?.filter { it.currency == "USD" }
                        val cadList = it.data?.message?.filter { it.currency == "CAD" }
                        try {
                            gbpBalance = gbpList?.get(0)?.balance.toString()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                        try {
                            eurBalance = eurList?.get(0)?.balance.toString()
                        }catch (e: Exception) {
                            e.printStackTrace()
                        }

                        try {
                            usdBalance = usdList?.get(0)?.balance.toString()
                        }catch (e: Exception) {
                            e.printStackTrace()
                        }
                        try {
                            cadBalance = cadList?.get(0)?.balance.toString()
                        }catch (e: Exception) {
                            e.printStackTrace()
                        }
                        if (selectCurrencyFrom == "GBP") {
                            binding.tvBalance.text = "Your Balance: $gbpBalance"
                        } else if (selectCurrencyFrom == "USD") {
                            binding.tvBalance.text = "Your Balance: $usdBalance"
                        } else if (selectCurrencyFrom == "EUR") {
                            binding.tvBalance.text = "Your Balance: $eurBalance"
                        } else if (selectCurrencyFrom == "CAD") {
                            binding.tvBalance.text = "Your Balance: $cadBalance"
                        }

                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }
        viewModel.resultConversionRate.observe(requireActivity()) {
            when (it.status) {
                Status.SUCCESS -> {
                    MyApplication.hideLoader()
                    val conversionRatUSD = it?.data?.data?.USD
                    val conversionRateGBP = it?.data?.data?.GBP
                    val conversionRateEUR = it?.data?.data?.EUR
                    val conversionRateCAD = it?.data?.data?.CAD

                    if (isConvertedFromFirstEditText) {
                        val firstCurrencyAmount = binding.etCurrencyFrom.text.toString()


                        var convertedAmountUSD = 0.0
                        if (it.data?.data?.USD != 0.0) {
                            convertedAmountUSD =
                                firstCurrencyAmount.toLong().times(conversionRatUSD!!)
                            convertedAmountUSD =
                                String.format("%.5f", convertedAmountUSD).toDouble()
                            binding.etCurrencyTo.setText(convertedAmountUSD.toInt().toString())
                        }

                        var convertedAmountGBP = 0.0
                        if (it.data?.data?.GBP != 0.0) {
                            convertedAmountGBP =
                                firstCurrencyAmount.toLong().times(conversionRateGBP!!)
                            convertedAmountGBP =
                                String.format("%.2f", convertedAmountGBP).toDouble()
                            binding.etCurrencyTo.setText(convertedAmountGBP.toInt().toString())
                        }

                        var convertedAmountEUR = 0.0
                        if (it.data?.data?.EUR != 0.0) {
                            convertedAmountEUR =
                                firstCurrencyAmount.toLong().times(conversionRateEUR!!)
                            convertedAmountEUR =
                                String.format("%.2f", convertedAmountEUR).toDouble()
                            binding.etCurrencyTo.setText(convertedAmountEUR.toInt().toString())
                        }

                        var convertedAmountCAD = 0.0
                        if (it.data?.data?.CAD != 0.0) {
                            convertedAmountCAD =
                                firstCurrencyAmount.toLong().times(conversionRateCAD!!)
                            convertedAmountCAD =
                                String.format("%.2f", convertedAmountCAD).toDouble()
                            binding.etCurrencyTo.setText(convertedAmountCAD.toInt().toString())
                        }
                        val a = selectCurrencyTo
                        Log.d("TAG", "observer:$a")
                        when (a) {
                            "USD" -> {
                                binding.tvConvertAmount.text =
                                    "$firstCurrencyAmount $selectCurrencyFrom = $convertedAmountUSD $selectCurrencyTo"
                            }
                            "GBP" -> {
                                binding.tvConvertAmount.text =
                                    "$firstCurrencyAmount $selectCurrencyFrom = $convertedAmountGBP $selectCurrencyTo"
                            }
                            "EUR" -> {
                                binding.tvConvertAmount.text =
                                    "$firstCurrencyAmount $selectCurrencyFrom = $convertedAmountEUR $selectCurrencyTo"
                            }
                            "CAD" -> {
                                binding.tvConvertAmount.text =
                                    "$firstCurrencyAmount $selectCurrencyFrom = $convertedAmountCAD $selectCurrencyTo"
                            }
                        }
                    } else {
                        val secondCurrencyAmount = binding.etCurrencyTo.text.toString()


                        var secondConvertedAmountUSD = 0.0
                        if (it.data?.data?.USD != 0.0) {
                            secondConvertedAmountUSD =
                                secondCurrencyAmount.toLong().times(conversionRatUSD!!)
                            secondConvertedAmountUSD =
                                String.format("%.2f", secondConvertedAmountUSD).toDouble()
                            binding.etCurrencyFrom.setText(
                                secondConvertedAmountUSD.toInt().toString()
                            )
                        }

                        var secondConvertedAmountGBP = 0.0
                        if (it.data?.data?.GBP != 0.0) {
                            secondConvertedAmountGBP =
                                secondCurrencyAmount.toLong().times(conversionRateGBP!!)
                            secondConvertedAmountGBP =
                                String.format("%.2f", secondConvertedAmountGBP).toDouble()
                            binding.etCurrencyFrom.setText(
                                secondConvertedAmountGBP.toInt().toString()
                            )
                        }

                        var secondConvertedAmountEUR = 0.0
                        if (it.data?.data?.EUR != 0.0) {
                            secondConvertedAmountEUR =
                                secondCurrencyAmount.toLong().times(conversionRateEUR!!)
                            secondConvertedAmountEUR =
                                String.format("%.2f", secondConvertedAmountEUR).toDouble()
                            binding.etCurrencyFrom.setText(
                                secondConvertedAmountEUR.toInt().toString()
                            )
                        }

                        var secondConvertedAmountCAD = 0.0
                        if (it.data?.data?.CAD != 0.0) {
                            secondConvertedAmountCAD =
                                secondCurrencyAmount.toLong().times(conversionRateCAD!!)
                            secondConvertedAmountCAD =
                                String.format("%.2f", secondConvertedAmountCAD).toDouble()
                            binding.etCurrencyFrom.setText(
                                secondConvertedAmountCAD.toInt().toString()
                            )
                        }
                        when (selectCurrencyFrom) {
                            "USD" -> {
                                binding.tvConvertAmount.text =
                                    "$secondCurrencyAmount $selectCurrencyTo = $secondConvertedAmountUSD $selectCurrencyFrom"
                            }
                            "GBP" -> {
                                binding.tvConvertAmount.text =
                                    "$secondCurrencyAmount $selectCurrencyTo = $secondConvertedAmountGBP $selectCurrencyFrom"
                            }
                            "EUR" -> {
                                binding.tvConvertAmount.text =
                                    "$secondCurrencyAmount $selectCurrencyTo = $secondConvertedAmountEUR $selectCurrencyFrom"
                            }
                            "CAD" -> {
                                binding.tvConvertAmount.text =
                                    "$secondCurrencyAmount $selectCurrencyTo = $secondConvertedAmountCAD $selectCurrencyFrom"
                            }
                        }
                    }

                }
                Status.LOADING -> {
                    MyApplication.showLoader(requireContext())
                }
                Status.ERROR -> {
                    MyApplication.hideLoader()
                    requireContext().showToast(it?.message.toString())
                }
            }

        }
    }


}