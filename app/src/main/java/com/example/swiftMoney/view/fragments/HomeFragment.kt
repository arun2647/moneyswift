package com.example.swiftMoney.view.fragments

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingData
import androidx.paging.filter
import com.example.swiftMoney.R
import com.example.swiftMoney.adapter.CompanyDashboardAdapter
import com.example.swiftMoney.base.BaseFragment
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.FragmentHomeBinding
import com.example.swiftMoney.model.companyDashboard.Data
import com.example.swiftMoney.utils.ReceiptDialog
import com.example.swiftMoney.utils.SharedPreference
import com.example.swiftMoney.utils.Status
import com.example.swiftMoney.utils.showToast
import com.example.swiftMoney.view.FilterDialog
import com.example.swiftMoney.view.WalletRechargeActivity
import com.example.swiftMoney.viewModel.CompanyViewModel
import com.example.swiftMoney.viewModel.ViewModelFactory
import com.example.swiftMoney.viewModel.WalletViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import java.io.File
import java.io.FileOutputStream
import java.util.*


class HomeFragment : BaseFragment(), View.OnClickListener {

    private val viewModel by viewModels<WalletViewModel> {
        ViewModelFactory(
            application,
            repository
        )
    }
    private val companyViewModel by viewModels<CompanyViewModel> {
        ViewModelFactory(
            application,
            repository
        )
    }
    lateinit var binding: FragmentHomeBinding
    var dashboardTransactionList: ArrayList<Data> = ArrayList()
    var jsonString: String = ""
    var data: PagingData<Data> = PagingData.empty()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(layoutInflater)
        observer()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.onClick = this
        var companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
        // companyViewModel.getCompanyDashboard(companyId.toString())
        val adapter = CompanyDashboardAdapter(requireContext(), callback)
        binding.rvWalletHistory.adapter = adapter
        companyViewModel.getPagingDashboard(companyId.toString()).observe(viewLifecycleOwner) {
            it?.let {
                adapter.submitData(lifecycle, it)
                Handler(requireContext().mainLooper).postDelayed(Runnable {
                   dashboardTransactionList.addAll(adapter.snapshot().items)
                    Log.d("data", "observer: ${Gson().toJson(dashboardTransactionList)}")
                }, 5000)

                data = it
            }


        }
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(text: Editable?) {
                var list: ArrayList<Data> = ArrayList()
                if (text?.length!! >= 2) {
                    /* binding.rvWalletHistory.visibility = View.VISIBLE
                     for (i in dashboardTransactionList.indices) {
                         if (dashboardTransactionList[i].initiated_by.toLowerCase().contains(text)) {
                             if (!list.contains(dashboardTransactionList[i])) {
                                 list.add(dashboardTransactionList[i])
                             }
                         }
                     }*/
                    companyViewModel.getPagingDashboard(companyId.toString())
                        .observe(viewLifecycleOwner) {
                            it?.let {
                                this@HomeFragment.lifecycleScope.launch(Dispatchers.Main) {
                                    val dd = it.filter { it.initiated_by.toLowerCase().contains(text) }
                                    adapter.submitData(lifecycle, dd)

                                }
                            }
                        }

                } else {
                    //     setCompanyDashboardAdapter(dashboardTransactionList)
                }
            }
        })
    }


    private fun observer() {
        companyViewModel.resultGetCompanyDashboard.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        jsonString = Gson().toJson(it.data?.data)
                        it.data?.data?.let { it1 -> setCompanyDashboardAdapter(it1) }
                        if (it.data?.data.isNullOrEmpty()) {
                            binding.tvNoTransactionFound.visibility = View.VISIBLE
                            binding.rvWalletHistory.visibility = View.GONE
                        } else {
                            binding.tvNoTransactionFound.visibility = View.GONE
                            binding.rvWalletHistory.visibility = View.VISIBLE
                        }
                        Handler(requireContext().mainLooper).postDelayed(Runnable {
                            it.data?.data?.let { it1 ->
                                dashboardTransactionList.addAll(
                                    it1
                                )
                            }
                        }, 2000)


                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }
    }


    private fun setCompanyDashboardAdapter(it1: List<com.example.swiftMoney.model.companyDashboard.Data>) {
        val adapter = CompanyDashboardAdapter(requireContext(), callback)
        binding.rvWalletHistory.adapter = adapter
    }

    var callback = object : CompanyDashboardAdapter.HomeTransactionCallback {
        override fun onClickOrderId(data: Data) {
            val fragment = ReceiptDialog()
            val bundle = Bundle()
            bundle.putSerializable("data", data)
            fragment.arguments = bundle
            fragment.show(parentFragmentManager, "")
        }
    }


    override fun onClick(v: View?) {
        when (v) {
            binding.btnMakeARecharge -> {
                startActivity(Intent(requireContext(), WalletRechargeActivity::class.java))
            }
            binding.rlDownload -> {
                if (checkReadWritePermission()) {
                    MyApplication.showLoader(requireContext())
                    Handler(requireContext().mainLooper).postDelayed(Runnable {
                        createExcelSheet(dashboardTransactionList)
                    }, 2500)

                }
            }
            binding.btnFilter -> {
                val dialog = FilterDialog()
                val bundle = Bundle()
                bundle.putBoolean("isFromHome", true)
                dialog.arguments = bundle
                dialog.show(parentFragmentManager, "")
            }
        }
    }

    private fun createExcelSheet(data: List<Data>) {

        val hssfWorkBook = HSSFWorkbook()
        val hssSheet = hssfWorkBook.createSheet()
        val hssRow = hssSheet.createRow(0)
        val hssCell = hssRow.createCell(0)
        hssCell.setCellValue("ID")

        val dateCell = hssRow.createCell(1)
        dateCell.setCellValue("Date")

        val staffNameCell = hssRow.createCell(2)
        staffNameCell.setCellValue("Staff Name")

        val transactionReferenceNumberCell = hssRow.createCell(3)
        transactionReferenceNumberCell.setCellValue("Transaction Reference Number")

        val transactionCell = hssRow.createCell(4)
        transactionCell.setCellValue("Transaction")

        val fee = hssRow.createCell(5)
        fee.setCellValue("Fee")

        val beneficiaryname = hssRow.createCell(6)
        beneficiaryname.setCellValue("Beneficiary Name")

        val accountNumber = hssRow.createCell(7)
        accountNumber.setCellValue("Account Number")

        val accountRoutingNumber = hssRow.createCell(8)
        accountRoutingNumber.setCellValue("Account Routing Number")

        val wireRoutingNumber = hssRow.createCell(9)
        wireRoutingNumber.setCellValue("Wire Routing Number")


        val ibanNumber = hssRow.createCell(10)
        ibanNumber.setCellValue("IBAN Number")


        val sortSwiftCode = hssRow.createCell(11)
        sortSwiftCode.setCellValue("SORT Swift Code")

        val branchNumber = hssRow.createCell(12)
        branchNumber.setCellValue("Branch Number")

        val bic = hssRow.createCell(13)
        bic.setCellValue("BIC")

        val beneficiaryPhone = hssRow.createCell(14)
        beneficiaryPhone.setCellValue("Beneficiary Phone")


        val beneficiaryEmail = hssRow.createCell(15)
        beneficiaryEmail.setCellValue("Beneficiary Email")

        val bankAddress = hssRow.createCell(16)
        bankAddress.setCellValue("Bank Address")

        val bankCountry = hssRow.createCell(17)
        bankCountry.setCellValue("Bank Country")

        val businessType = hssRow.createCell(18)
        businessType.setCellValue("Business Type")


        val type = hssRow.createCell(19)
        type.setCellValue("Type")

        val orderDate = hssRow.createCell(20)
        orderDate.setCellValue("Order Date")

        val completeDate = hssRow.createCell(21)
        completeDate.setCellValue("Completed Date")


        val transactionMode = hssRow.createCell(22)
        transactionMode.setCellValue("Transaction Mode")

        val status = hssRow.createCell(23)
        status.setCellValue("Status")


        val document = hssRow.createCell(24)
        document.setCellValue("Documents")

        val pdf = hssRow.createCell(25)
        pdf.setCellValue("PDF")


        for (i in data.indices) {
            val row = hssSheet.createRow(i + 1)
            for (j in 0..25) {
                if (j == 0) {
                    for (k in data.indices) {
                        val cell = row.createCell(0)
                        cell.setCellValue(data[i].transaction_id.toString())
                    }
                }

                if (j == 1) {
                    for (k in data.indices) {
                        val cell = row.createCell(1)
                        cell.setCellValue(data[i].date_time.toString())
                    }
                }
                if (j == 2) {
                    for (k in data.indices) {
                        val cell = row.createCell(2)
                        cell.setCellValue(" ")
                    }
                }

                if (j == 3) {
                    for (k in data.indices) {
                        val cell = row.createCell(3)
                        cell.setCellValue(data[i].trans_ref.toString())
                    }
                }
                if (j == 4) {
                    for (k in data.indices) {
                        val cell = row.createCell(4)
                        cell.setCellValue(data[i].currency + " " + data[i].amount)
                    }
                }

                if (j == 5) {
                    for (k in data.indices) {
                        val cell = row.createCell(5)
                        cell.setCellValue(data[i].fee + " " + data[i].amount)
                    }
                }
                if (j == 6) {
                    for (k in data.indices) {
                        val cell = row.createCell(6)
                        cell.setCellValue(data[i].beneficiary_name)
                    }
                }
                if (j == 7) {
                    for (k in data.indices) {
                        val cell = row.createCell(7)
                        cell.setCellValue(data[i].account_number)
                    }
                }
                if (j == 8) {
                    for (k in data.indices) {
                        val cell = row.createCell(8)
                        cell.setCellValue(data[i].account_routing_number)
                    }
                }
                if (j == 9) {
                    for (k in data.indices) {
                        val cell = row.createCell(9)
                        cell.setCellValue(data[i].wire_routing_number)
                    }
                }
                if (j == 10) {
                    for (k in data.indices) {
                        val cell = row.createCell(10)
                        cell.setCellValue(data[i].iban_number)
                    }
                }
                if (j == 11) {
                    for (k in data.indices) {
                        val cell = row.createCell(11)
                        cell.setCellValue(data[i].sort_swift_code)
                    }
                }
                if (j == 12) {
                    for (k in data.indices) {
                        val cell = row.createCell(12)
                        cell.setCellValue(data[i].branch_number)
                    }
                }
                if (j == 13) {
                    for (k in data.indices) {
                        val cell = row.createCell(13)
                        cell.setCellValue(data[i].bic)
                    }
                }
                if (j == 14) {
                    for (k in data.indices) {
                        val cell = row.createCell(14)
                        cell.setCellValue(data[i].beneficiary_phone)
                    }
                }
                if (j == 15) {
                    for (k in data.indices) {
                        val cell = row.createCell(15)
                        cell.setCellValue(data[i].beneficiary_email)
                    }
                }
                if (j == 16) {
                    for (k in data.indices) {
                        val cell = row.createCell(16)
                        cell.setCellValue(data[i].bank_address)
                    }
                }
                if (j == 17) {
                    for (k in data.indices) {
                        val cell = row.createCell(17)
                        cell.setCellValue(data[i].bank_country)
                    }
                }
                if (j == 18) {
                    for (k in data.indices) {
                        val cell = row.createCell(18)
                        cell.setCellValue(data[i].business_type)
                    }
                }
                if (j == 19) {
                    for (k in data.indices) {
                        val cell = row.createCell(19)
                        cell.setCellValue(data[i].type)
                    }
                }
                if (j == 20) {
                    for (k in data.indices) {
                        val cell = row.createCell(20)
                        cell.setCellValue(data[i].order_date)
                    }
                }
                if (j == 21) {
                    for (k in data.indices) {
                        val cell = row.createCell(21)
                        cell.setCellValue(data[i].completed_date)
                    }
                }
                if (j == 22) {
                    for (k in data.indices) {
                        val cell = row.createCell(22)
                        cell.setCellValue(data[i].transaction_mode)
                    }
                }

                if (j == 23) {
                    for (k in data.indices) {
                        val cell = row.createCell(23)
                        cell.setCellValue(data[i].status)
                    }
                }
                if (j == 24) {
                    for (k in data.indices) {
                        val cell = row.createCell(24)
                        cell.setCellValue(data[i].documents.joinToString(","))
                    }
                }
                if (j == 25) {
                    for (k in data.indices) {
                        val cell = row.createCell(25)
                        cell.setCellValue(data[i].pdf)
                    }
                }

            }


        }
        try {
            val fileOutputStream = FileOutputStream(createFileForExcel())
            hssfWorkBook.write(fileOutputStream)
            if (fileOutputStream != null) {
                fileOutputStream.flush()
                fileOutputStream.close()
            }
            MyApplication.hideLoader()
            val snackbar: Snackbar =
                Snackbar.make(requireView(), "Successfully downloaded", Snackbar.LENGTH_LONG)
            val snackBarView = snackbar.view
            snackBarView.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.purple_700
                )
            )
            val textView =
                snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
            textView.setTextColor(Color.BLACK)
            snackbar.show()
        } catch (e: Exception) {
            MyApplication.hideLoader()
            e.printStackTrace()
        }

    }


    private fun createFileForExcel(): File {
        val localStorage: File? =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        val storagePath = localStorage?.absolutePath
        val rootPath = "$storagePath"
        val uniqueString: String = UUID.randomUUID().toString()
        val fileName = "/MoneySwift$uniqueString.xls"
        val root = File(rootPath)
        if (!root.mkdirs()) {
            Log.i("Test", "This path is already exist: " + root.absolutePath)
        }
        val file = File(rootPath + fileName)
        return file
    }

    fun checkReadWritePermission(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 101
            )
            return false
        }
        return true
    }
}