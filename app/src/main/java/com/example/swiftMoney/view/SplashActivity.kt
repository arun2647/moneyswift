package com.example.swiftMoney.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.swiftMoney.R
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.utils.SharedPreference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        var isUserLogin = GetObjects.preference.getBoolean(SharedPreference.Key.ISUSERLOGIN, false)
        this.lifecycleScope.launch(Dispatchers.Main) {
            delay(2000)
            if (isUserLogin) {
                startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                finishAffinity()
            } else {
                startActivity(Intent(this@SplashActivity, WelcomeActivity::class.java))
                finishAffinity()
            }

        }
    }
}