package com.example.swiftMoney.view.transaction

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.example.swiftMoney.R
import com.example.swiftMoney.databinding.ActivitySubmitTransactionBinding

class SubmitTransactionActivity : AppCompatActivity(),View.OnClickListener {

    lateinit var binding:ActivitySubmitTransactionBinding
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivitySubmitTransactionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        navController = this.findNavController(R.id.submitTransactionHost)
        binding.onClick=this

    }

    override fun onClick(v: View?) {
        when(v){
            binding.llNew -> {
                binding.llNew.background=ContextCompat.getDrawable(this,R.drawable.bg_yellow_radius)
                binding.llExisting.background=ContextCompat.getDrawable(this,R.drawable.bg_white_radius)
                binding.tvExistingBeneficiary.setTextColor(ContextCompat.getColor(this,R.color.txt_color_grey))
                binding.tvNewBeneficiary.setTextColor(ContextCompat.getColor(this,R.color.black))
             //   navController.popBackStack(R.id.existingBeneficiaryPersonalInfoFragment,true)
                navController.clearBackStack(R.id.existingBeneficiaryPersonalInfoFragment)
                navController.navigate(R.id.newBeneficiaryPersonalInfoFragment)
            }
            binding.llExisting -> {
                binding.llNew.background=ContextCompat.getDrawable(this,R.drawable.bg_white_radius)
                binding.llExisting.background=ContextCompat.getDrawable(this,R.drawable.bg_yellow_radius)
                binding.tvNewBeneficiary.setTextColor(ContextCompat.getColor(this,R.color.txt_color_grey))
                binding.tvExistingBeneficiary.setTextColor(ContextCompat.getColor(this,R.color.black))
            //    navController.popBackStack(R.id.newBeneficiaryPersonalInfoFragment,true)
                navController.clearBackStack(R.id.newBeneficiaryPersonalInfoFragment)
                navController.navigate(R.id.existingBeneficiaryPersonalInfoFragment)

            }
        }
    }
}