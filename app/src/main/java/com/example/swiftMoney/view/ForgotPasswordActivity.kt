package com.example.swiftMoney.view

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.example.swiftMoney.base.BaseActivity
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.ActivityForgotPasswordBinding
import com.example.swiftMoney.utils.Status
import com.example.swiftMoney.utils.showToast
import com.example.swiftMoney.viewModel.AuthViewModel
import com.example.swiftMoney.viewModel.ViewModelFactory

class ForgotPasswordActivity : BaseActivity(), View.OnClickListener {

    lateinit var binding: ActivityForgotPasswordBinding
    private val viewModel by viewModels<AuthViewModel> { ViewModelFactory(application, repository) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotPasswordBinding.inflate(layoutInflater)
        binding.onclick = this
        setContentView(binding.root)
        observer()
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnBackToLogin -> {
                onBackPressed()
            }
            binding.ivBack -> {
                onBackPressed()
            }
            binding.btnSend -> {
                if (!binding.etEmail.text.toString().isNullOrEmpty()) {
                    hitForgotPasswordApi()
                } else {
                    showToast("Enter your email")
                }
            }
        }
    }

    private fun hitForgotPasswordApi() {
        val email = binding.etEmail.text.toString()
        viewModel.onForgotPassword(email)
    }

    private fun observer() {
        viewModel.resultForgotPassword.observe(this) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        showToast(it?.data?.message.toString())
                        onBackPressed()
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(this)
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        showToast(it?.message.toString())
                    }
                }
            }
        }
    }
}