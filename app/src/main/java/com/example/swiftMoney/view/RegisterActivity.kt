package com.example.swiftMoney.view

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.viewModels
import com.example.swiftMoney.R
import com.example.swiftMoney.base.BaseActivity
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.ActivityRegisterBinding
import com.example.swiftMoney.model.register.RegisterParamModel
import com.example.swiftMoney.utils.Status
import com.example.swiftMoney.utils.Utility
import com.example.swiftMoney.utils.showToast
import com.example.swiftMoney.viewModel.AuthViewModel
import com.example.swiftMoney.viewModel.ViewModelFactory

class RegisterActivity : BaseActivity(), View.OnClickListener {

    private lateinit var binding: ActivityRegisterBinding
    private val viewModel by viewModels<AuthViewModel> { ViewModelFactory(application, repository) }
    var countryListId: ArrayList<Int> = ArrayList()
    var selectCountryName: String = ""
    var selectStateName: String = ""
    var email: String = ""
    var userId: String = ""
    var isFromSocialLogin: Boolean = false
    var selectedCompanyBusinessType=""
    var companyBusinessTypeList = arrayOf(
        "Money Transmitting",
        "Trading Company",
        "Currency Exchange Company",
        "Foreign Exchange Dealer",
        "Cryptocurrency Company",
        "Other"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.onClick = this
        viewModel.getCountryList()
        observer()
        selectedCountryAndStateSpinner()
        setCompanyBusinessTypeSpinner()

        isFromSocialLogin = intent.getBooleanExtra("isFromSocialLogin", false) == true
        if (isFromSocialLogin) {
            email = intent.getStringExtra("email").toString()
            userId = intent.getIntExtra("userId", 0).toString()
            binding.etEmailAddress.setText(email)
            binding.textView2.text = "Update Details"
            binding.btnSignUp.text = "Update"
            binding.llAccount.visibility = View.GONE
        }
    }

    private fun setCompanyBusinessTypeSpinner() {

        binding.spinnerCompanyBusinessType.item=companyBusinessTypeList.toList()
    }

    private fun selectedCountryAndStateSpinner() {
        binding.spinnerCountry.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    p0: AdapterView<*>?,
                    p1: View?,
                    position: Int,
                    p3: Long
                ) {
                    val selectedCountryId = countryListId[position]
                    selectCountryName = p0?.selectedItem.toString()
                    viewModel.getStateList(selectedCountryId.toString())
                    binding.tvCountryError.visibility = View.GONE
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

            }
        binding.spinnerCompanyBusinessType.onItemSelectedListener=object :AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val selectedCountryId = companyBusinessTypeList[p2]
                selectedCompanyBusinessType=selectedCountryId
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

        }

        binding.spinnerState.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                selectStateName = p0?.selectedItem.toString()
                binding.tvStateError.visibility = View.GONE
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnSignUp -> {
                if (validation()) {
                    hitRegisterApi()
                }
            }
            binding.btnSignIn -> {
                onBackPressed()
            }
        }
    }

    private fun hitRegisterApi() {
        val firstName = binding.etFirstName.text.toString()
        val lastName = binding.etLastName.text.toString()
        val emailAddress = binding.etEmailAddress.text.toString()
        val companyName = binding.etCompanyName.text.toString()
        val businessType = selectedCompanyBusinessType
        val website = binding.etWebsite.text.toString()
        val address = binding.etAddress.text.toString()
        val zipCode = binding.etZipCode.text.toString()
        val city = binding.etCity.text.toString()
        val phoneNumber = binding.etPhoneNumber.text.toString()
        val companyEmail = binding.etCompanyEmailAddress.text.toString()
        val password = binding.etPassword.text.toString()
        val body = RegisterParamModel(
            firstName,
            lastName,
            emailAddress,
            companyName,
            businessType,
            website,
            address,
            city,
            selectStateName,
            zipCode,
            selectCountryName,
            phoneNumber,
            companyEmail,
            password,
            userId
        )
        viewModel.onEmployeeRegister(body)
    }

    private fun validation(): Boolean {
        var isValid: Boolean = true
        if (TextUtils.isEmpty(binding.etFirstName.text.toString())) {
            isValid = false
            binding.etFirstName.error = "Enter your first name"
        }
        if (TextUtils.isEmpty(binding.etLastName.text.toString())) {
            isValid = false
            binding.etLastName.error = "Enter your last name"
        }
        if (TextUtils.isEmpty(binding.etEmailAddress.text.toString())) {
            isValid = false
            binding.etEmailAddress.error = "Enter your email address"
        } else if (!Utility.emailValidation(binding.etEmailAddress)) {
            isValid = false
            binding.etEmailAddress.error = "Enter your valid email address"
        }
        if (TextUtils.isEmpty(binding.etCompanyName.text.toString())) {
            isValid = false
            binding.etCompanyName.error = "Enter company name"
        }
        if (selectedCompanyBusinessType=="") {
            isValid = false
           showToast("Select company business type")
        }
        if (TextUtils.isEmpty(binding.etWebsite.text.toString())) {
            isValid = false
            binding.etWebsite.error = "Enter company website"
        }
        if (TextUtils.isEmpty(binding.etAddress.text.toString())) {
            isValid = false
            binding.etAddress.error = "Enter company address"
        }
        if (selectCountryName == "") {
            isValid = false
            binding.tvCountryError.visibility = View.VISIBLE
        }
        if (selectStateName == "") {
            isValid = false
            binding.tvStateError.visibility = View.VISIBLE
        }
        if (TextUtils.isEmpty(binding.etCity.text.toString())) {
            isValid = false
            binding.etCity.error = "Enter company city"
        }
        if (TextUtils.isEmpty(binding.etZipCode.text.toString())) {
            isValid = false
            binding.etZipCode.error = "Enter company zip code"
        }
        if (TextUtils.isEmpty(binding.etPhoneNumber.text.toString())) {
            isValid = false
            binding.etPhoneNumber.error = "Enter company phone number"
        }else if (binding.etPhoneNumber.text.toString().length<9 || binding.etPhoneNumber.text.toString().length>12){
            isValid = false
            binding.etPhoneNumber.error = "Enter valid phone number"
        }
        if (TextUtils.isEmpty(binding.etCompanyEmailAddress.text.toString())) {
            isValid = false
            binding.etCompanyEmailAddress.error = "Enter company email address"
        } else if (!Utility.emailValidation(binding.etCompanyEmailAddress)) {
            isValid = false
            binding.etCompanyEmailAddress.error = "Enter valid company email address"
        }
        if (TextUtils.isEmpty(binding.etPassword.text.toString())) {
            isValid = false
            binding.etPassword.error = "Enter your password"
        }
        if (TextUtils.isEmpty(binding.etConfirmPassword.text.toString())) {
            isValid = false
            binding.etConfirmPassword.error = "Enter your confirm password"
        }
        if (binding.etPassword.text.toString() != binding.etConfirmPassword.text.toString()){
            isValid = false
            binding.etConfirmPassword.error = "The password and confirmation password do not match."
        }
        return isValid
    }


    private fun observer() {
        viewModel.resultStaffRegister.observe(this) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        showToast(it.data?.message.toString(), Toast.LENGTH_LONG)
                        startActivity(Intent(this, LoginActivity::class.java))
                        finishAffinity()
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(this)
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        showToast(it?.message.toString())
                    }
                }
            }
        }

        viewModel.resultCountryList.observe(this) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        for (i in it.data?.message!!) {
                            countryListId.add(i.id)
                        }
                        binding.spinnerCountry.item =
                            it.data?.message?.distinctBy { it.country_name }
                                ?.map { it.country_name }
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(this)
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        showToast(it?.message.toString())
                    }
                }
            }
        }

        viewModel.resultStateList.observe(this) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        binding.spinnerState.item =
                            it.data?.message?.distinctBy { it.name }?.map { it.name }
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(this)
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        showToast(it?.message.toString())
                    }
                }
            }
        }
    }
}