package com.example.swiftMoney.view.transaction.newBeneficiary

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.swiftMoney.R
import com.example.swiftMoney.base.BaseFragment
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.FragmentNewBeneficiaryInformationBinding
import com.example.swiftMoney.utils.Status
import com.example.swiftMoney.utils.Utility
import com.example.swiftMoney.utils.showToast
import com.example.swiftMoney.viewModel.AuthViewModel
import com.example.swiftMoney.viewModel.ViewModelFactory


class NewBeneficiaryInformationFragment : BaseFragment(), View.OnClickListener {

    lateinit var binding: FragmentNewBeneficiaryInformationBinding
    var getWallet: String = ""
    var amountTransfer: String = ""
    private val viewModel by viewModels<AuthViewModel> { ViewModelFactory(application, repository) }
    var countryListId: ArrayList<Int> = ArrayList()
    var selectCountryName: String = ""
    var selectStateName: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNewBeneficiaryInformationBinding.inflate(layoutInflater)
        observer()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.onClick = this
        getWallet = arguments?.getString("wallet", "").toString()
        amountTransfer = arguments?.getString("amountTransfer", "").toString()
        selectedCountryAndStateSpinner()
        viewModel.getCountryList()
    }

    private fun selectedCountryAndStateSpinner() {
        binding.spinnerCountry.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    p0: AdapterView<*>?,
                    p1: View?,
                    position: Int,
                    p3: Long
                ) {
                    val selectedCountryId = countryListId[position]
                    selectCountryName = p0?.selectedItem.toString()
                    viewModel.getStateList(selectedCountryId.toString())
                    binding.tvCountryError.visibility = View.GONE
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

            }

        binding.spinnerState.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                selectStateName = p0?.selectedItem.toString()
                binding.tvStateError.visibility = View.GONE
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnNext -> {

                val beneficiaryName = binding.etName.text.toString()
                val beneficiaryEmailAddress = binding.eetEmail.text.toString()
                val beneficiaryPhoneNumber = binding.etPhoneNumber.text.toString()
                val beneficiaryAddress = binding.etAddress.text.toString()
                val beneficiaryCity = binding.etCity.text.toString()
                val beneficiaryZipCode = binding.etZipCode.text.toString()
                val bundle = Bundle()
                bundle.putString("amountTransfer", amountTransfer)
                bundle.putString("beneficiaryName", beneficiaryName)
                bundle.putString("beneficiaryEmailAddress", beneficiaryEmailAddress)
                bundle.putString("beneficiaryPhoneNumber", beneficiaryPhoneNumber)
                bundle.putString("beneficiaryAddress", beneficiaryAddress)
                bundle.putString("beneficiaryCity", beneficiaryCity)
                bundle.putString("beneficiaryCountry", selectCountryName)
                bundle.putString("beneficiaryState", selectStateName)
                bundle.putString("beneficiaryZipCode", beneficiaryZipCode)
                bundle.putString("wallet", getWallet)
                if (validate()) {
                    findNavController().navigate(
                        R.id.action_newBeneficiaryInformationFragment_to_newBeneficiaryBankDetailFragment,
                        bundle
                    )
                }
            }
        }
    }

    fun validate(): Boolean {
        var isValid: Boolean = true
        if (TextUtils.isEmpty(binding.etName.text.toString())) {
            isValid = false
            binding.etName.error = "Enter name"
        }
        if (TextUtils.isEmpty(binding.eetEmail.text.toString())) {
            isValid = false
            binding.eetEmail.error = "Enter email address"
        } else if (!Utility.emailValidation(binding.eetEmail)) {
            isValid = false
            binding.eetEmail.error = "Enter valid email"
        }
        if (TextUtils.isEmpty(binding.etPhoneNumber.text.toString())) {
            isValid = false
            binding.etPhoneNumber.error = "Enter phone number"
        }else if (binding.etPhoneNumber.text.toString().length<9 || binding.etPhoneNumber.text.toString().length>12){
            isValid = false
            binding.etPhoneNumber.error = "Enter valid phone number"
        }
        if (TextUtils.isEmpty(binding.etAddress.text.toString())) {
            isValid = false
            binding.etAddress.error = "Enter address"
        }
        if (selectCountryName == "") {
            isValid = false
            requireContext().showToast("Select country")
        }
        if (selectStateName == "") {
            isValid = false
            requireContext().showToast("Select state")
        }
        if (TextUtils.isEmpty(binding.etCity.text.toString())) {
            isValid = false
            binding.etCity.error = "Enter city"
        }
        if (TextUtils.isEmpty(binding.etZipCode.text.toString())) {
            isValid = false
            binding.etZipCode.error = "Enter zip code"
        }
        return isValid
    }


    private fun observer() {
        viewModel.resultCountryList.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        for (i in it.data?.message!!) {
                            countryListId.add(i.id)
                        }
                        binding.spinnerCountry.item = it.data?.message?.distinctBy { it.country_name }?.map { it.country_name }
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }

        viewModel.resultStateList.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        binding.spinnerState.item =
                            it.data?.message?.distinctBy { it.name }?.map { it.name }
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }
    }
}