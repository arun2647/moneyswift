package com.example.swiftMoney.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import com.example.swiftMoney.base.BaseActivity
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.ActivityEditProfileBinding
import com.example.swiftMoney.model.editProfile.EditProfileParamModel
import com.example.swiftMoney.utils.*
import com.example.swiftMoney.viewModel.CompanyViewModel
import com.example.swiftMoney.viewModel.ViewModelFactory
import java.io.File

class EditProfileActivity : BaseActivity(), View.OnClickListener {

    lateinit var binding: ActivityEditProfileBinding
    private val viewModel by viewModels<CompanyViewModel> {
        ViewModelFactory(
            application,
            repository
        )
    }

    companion object {
        var imageFile: File? = null
        private lateinit var uri: Uri
        private var idCard: File? = null
        lateinit var imageFromGallery: ActivityResultLauncher<String>
        lateinit var cameraPermission: ActivityResultLauncher<String>
        lateinit var galleryPermission: ActivityResultLauncher<String>
    }

    var fileUrl: String = ""
    var documentUrl: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditProfileBinding.inflate(layoutInflater)
        binding.onClick = this
        setContentView(binding.root)
        observer()
        val userId = GetObjects.preference.getInteger(SharedPreference.Key.USERID, 0)
        viewModel.getStaffDetail(userId.toString())
        imageFromGallery = imageFromGallery()
        cameraPermission = cameraPermission()
        galleryPermission = galleryPermission()
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.ivBack -> {
                onBackPressed()
            }
            binding.btnUpdate -> {
                hitUpdateProfile()
            }
            binding.rlUploadImage, binding.ivIdCard -> {
                onImagePickDialog()
            }
            binding.tvViewDocument -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(fileUrl + documentUrl))
                startActivity(browserIntent)
            }

        }
    }

    private fun hitUpdateProfile() {
        val firstName = binding.etFirstName.text.toString()
        val lastName = binding.etLastName.text.toString()
        val email = binding.etEmail.text.toString()
        val password = binding.etPassword.text.toString()
        val companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
        val staffId = GetObjects.preference.getInteger(SharedPreference.Key.USERID, 0)
        val body = EditProfileParamModel(
            companyId.toString(),
            staffId.toString(),
            firstName,
            lastName,
            password,
            email,
            idCard
        )
        viewModel.onEditProfile(body)
    }


    private fun observer() {
        viewModel.resultOnEditProfile.observe(this) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        showToast(it?.data?.message.toString())
                        onBackPressed()
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(this)
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        showToast(it?.message.toString())
                    }
                }
            }
        }
        viewModel.resultGetStaffDetail.observe(this) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        try {
                            binding.etFirstName.setText(it.data?.message?.get(0)?.first_name)
                            binding.etLastName.setText(it.data?.message?.get(0)?.last_name)
                            binding.etEmail.setText(it.data?.message?.get(0)?.email)
                            //binding.etAddress.setText(it.data?.message?.get(0)?.a)
                            fileUrl = it.data?.fileBaseurl.toString()
                            documentUrl = it.data?.message?.get(0)?.id_card.toString()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(this)
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        showToast(it?.message.toString())
                    }
                }
            }
        }
    }

    private fun imageFromGallery(): ActivityResultLauncher<String> {
        return registerForActivityResult(
            ActivityResultContracts.GetContent()
        ) {
            if (it != null) {
                binding.ivIdCard.visibility = View.VISIBLE
                binding.tvPdfName.visibility = View.GONE
                val imageFile = FilePath.getPath(this, it)
                idCard = File(imageFile)
                Glide.with(this).load(imageFile).into(binding.ivIdCard)
                binding.rlUploadImage.visibility = View.GONE
            }
        }
    }

    private fun cameraPermission(): ActivityResultLauncher<String> {
        return registerForActivityResult(ActivityResultContracts.RequestPermission()) { it ->
            if (it) {
                imageFile = File.createTempFile(
                    "IMG_",
                    ".jpg",
                    this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                )


                uri = FileProvider.getUriForFile(
                    this,
                    "${this.packageName}.provider",
                    imageFile!!
                )

                takePicture.launch(uri)
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.CAMERA
                    )
                ) {
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(
                            Manifest.permission.CAMERA
                        ),
                        120
                    )

                } else {
                    val alertDialogBuilder = AlertDialog.Builder(this)
                    alertDialogBuilder.setTitle("Change Permissions in Settings")
                    alertDialogBuilder
                        .setMessage(
                            """Click SETTINGS to Manually Set Permissions""".trimIndent()
                        )
                        .setCancelable(true)
                        .setPositiveButton("SETTINGS") { dialog, id ->
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri = Uri.fromParts("package", packageName, null)
                            intent.data = uri
                            startActivityForResult(intent, 150) // Comment 3.
                        }

                    val alertDialog = alertDialogBuilder.create()
                    alertDialog.show()
                }
            }
        }
    }


    private fun galleryPermission(): ActivityResultLauncher<String> {
        return registerForActivityResult(ActivityResultContracts.RequestPermission()) { it ->
            if (it) {
                imageFromGallery.launch("image/*")
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                ) {
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        120
                    )
                } else {
                    val alertDialogBuilder = AlertDialog.Builder(this)
                    alertDialogBuilder.setTitle("Change Permissions in Settings")
                    alertDialogBuilder
                        .setMessage(
                            """Click SETTINGS to Manually Set Permissions""".trimIndent()
                        )
                        .setCancelable(true)
                        .setPositiveButton("SETTINGS") { dialog, id ->
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri = Uri.fromParts("package", packageName, null)
                            intent.data = uri
                            startActivityForResult(intent, 150) // Comment 3.
                        }

                    val alertDialog = alertDialogBuilder.create()
                    alertDialog.show()
                }
            }
        }
    }


    private fun onImagePickDialog() {
        val itemList = arrayOf<CharSequence>("Camera", "Gallery", "Document")
        val mDialog = AlertDialog.Builder(this)
        mDialog.setTitle("Take Photo")
        mDialog.setItems(itemList) { dialogInterface, i ->
            when {
                itemList[i] == "Camera" -> {
                    cameraPermission.launch(
                        Manifest.permission.CAMERA
                    )
                }
                itemList[i] == "Gallery" -> {
                    galleryPermission.launch(
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                }
                itemList[i] == "Document" -> {
                    if (documentCheckPermission()) {
                        showFileChooser()
                    } else {
                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            111
                        )
                    }

                }
            }
        }
        mDialog.show()
    }


    private val takePicture =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { isSaved ->
            if (isSaved) {
                binding.ivIdCard.visibility = View.VISIBLE

                idCard = imageFile
                Glide.with(this).load(imageFile).into(binding.ivIdCard)
                binding.rlUploadImage.visibility = View.GONE
                binding.tvPdfName.visibility = View.GONE
            }
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 120 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            val document = data.data;
            binding.ivIdCard.visibility = View.GONE
            binding.tvPdfName.visibility = View.VISIBLE
            idCard = FileUtil.from(this, document)
            val filename: String =
                document?.path?.substring(document?.path!!.lastIndexOf("/") + 1).toString()
            binding.tvPdfName.text = filename
            binding.rlUploadImage.visibility = View.GONE
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 111) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showFileChooser()
            } else {
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG)
                    .show();
            }
        }
    }

    private fun showFileChooser() {
        val intent = Intent()
        intent.type = "*/*"
        val mimetypes = arrayOf(
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/msword", "application/pdf"
        )
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Pdf"), 120)
    }

    @SuppressLint("NewApi")
    private fun documentCheckPermission(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }
}