package com.example.swiftMoney.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import com.example.swiftMoney.base.BaseActivity
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.ActivityAddStaffBinding
import com.example.swiftMoney.model.addCompanyStaff.AddCompanyStaffParamModel
import com.example.swiftMoney.utils.*
import com.example.swiftMoney.viewModel.CompanyViewModel
import com.example.swiftMoney.viewModel.ViewModelFactory
import java.io.File


class AddStaffActivity : BaseActivity(), View.OnClickListener {

    lateinit var binding: ActivityAddStaffBinding
    private val viewModel by viewModels<CompanyViewModel> {
        ViewModelFactory(
            application,
            repository
        )
    }

    companion object {
        var imageFile: File? = null
        private lateinit var uri: Uri
        private var idCard: File? = null
        lateinit var imageFromGallery: ActivityResultLauncher<String>
        lateinit var cameraPermission: ActivityResultLauncher<String>
        lateinit var galleryPermission: ActivityResultLauncher<String>
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddStaffBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.onClick = this
        observer()
        imageFromGallery = imageFromGallery()
        cameraPermission = cameraPermission()
        galleryPermission = galleryPermission()
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.ivBack -> {
                onBackPressed()
            }
            binding.btnAddStaff -> {
                if (validation()) {
                    hitAddStaffApi()
                }
            }
            binding.rlUploadImage,binding.ivIdCard,binding.tvPdfName-> {
                onImagePickDialog()
            }
        }
    }

    private fun hitAddStaffApi() {
        val firstName = binding.etFirstName.text.toString()
        val lastName = binding.etLastName.text.toString()
        val email = binding.etEmail.text.toString()
        val password = binding.etPassword.text.toString()
        var companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
        val body = AddCompanyStaffParamModel(
            companyId.toString(),
            email,
            firstName,
            lastName,
            password,
            idCard
        )

        viewModel.onAddCompanyStaff(body)
    }



    private fun validation(): Boolean {
        var isValid: Boolean = true
        if (TextUtils.isEmpty(binding.etFirstName.text.toString())) {
            isValid = false
            binding.etFirstName.error = "Enter first name"
        }
        if (TextUtils.isEmpty(binding.etLastName.text.toString())) {
            isValid = false
            binding.etLastName.error = "Enter last name"
        }
        if (TextUtils.isEmpty(binding.etEmail.text.toString())) {
            isValid = false
            binding.etEmail.error = "Enter email address"
        } else if (!Utility.emailValidation(binding.etEmail)) {
            isValid = false
            binding.etEmail.error = "Enter valid email address"
        }
        if (TextUtils.isEmpty(binding.etPassword.text.toString())) {
            isValid = false
            binding.etPassword.error = "Enter password"
        }
        if (TextUtils.isEmpty(binding.etConfirmPassword.text.toString())) {
            isValid = false
            binding.etConfirmPassword.error = "Enter confirm password"
        }
        if (binding.etPassword.text.toString() != binding.etConfirmPassword.text.toString()){
            isValid = false
            binding.etConfirmPassword.error = "The password and confirmation password do not match."
        }
        if (idCard==null) {
            isValid = false
            showToast("Upload Id card")
        }
        return isValid
    }

    private fun observer() {
        viewModel.resultAddCompanyStaffList.observe(this) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        showToast(it.data?.message.toString())
                        onBackPressed()
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(this)
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        showToast(it?.message.toString())
                    }
                }
            }
        }
    }

    private fun imageFromGallery(): ActivityResultLauncher<String> {
        return registerForActivityResult(
            ActivityResultContracts.GetContent()
        ) {
            if (it != null) {
                binding.ivIdCard.visibility = View.VISIBLE
                binding.tvPdfName.visibility = View.GONE
                val imageFile = FilePath.getPath(this, it)
                idCard = File(imageFile)
                Glide.with(this).load(imageFile).into(binding.ivIdCard)
                binding.rlUploadImage.visibility=View.GONE
            }
        }
    }

    private fun cameraPermission(): ActivityResultLauncher<String> {
        return registerForActivityResult(ActivityResultContracts.RequestPermission()) { it ->
            if (it) {
                imageFile = File.createTempFile(
                    "IMG_",
                    ".jpg",
                    this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                )


                uri = FileProvider.getUriForFile(
                    this,
                    "${this.packageName}.provider",
                    imageFile!!
                )

                takePicture.launch(uri)
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.CAMERA
                    )
                ) {
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(
                            Manifest.permission.CAMERA
                        ),
                        120
                    )

                } else {
                    val alertDialogBuilder = AlertDialog.Builder(this)
                    alertDialogBuilder.setTitle("Change Permissions in Settings")
                    alertDialogBuilder
                        .setMessage(
                            """Click SETTINGS to Manually Set Permissions""".trimIndent()
                        )
                        .setCancelable(true)
                        .setPositiveButton("SETTINGS") { dialog, id ->
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri = Uri.fromParts("package", packageName, null)
                            intent.data = uri
                            startActivityForResult(intent, 150) // Comment 3.
                        }

                    val alertDialog = alertDialogBuilder.create()
                    alertDialog.show()
                }
            }
        }
    }


    private fun galleryPermission(): ActivityResultLauncher<String> {
        return registerForActivityResult(ActivityResultContracts.RequestPermission()) { it ->
            if (it) {
                imageFromGallery.launch("image/*")
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                ) {
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        120
                    )
                } else {
                    val alertDialogBuilder = AlertDialog.Builder(this)
                    alertDialogBuilder.setTitle("Change Permissions in Settings")
                    alertDialogBuilder
                        .setMessage(
                            """Click SETTINGS to Manually Set Permissions""".trimIndent()
                        )
                        .setCancelable(true)
                        .setPositiveButton("SETTINGS") { dialog, id ->
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri = Uri.fromParts("package", packageName, null)
                            intent.data = uri
                            startActivityForResult(intent, 150) // Comment 3.
                        }

                    val alertDialog = alertDialogBuilder.create()
                    alertDialog.show()
                }
            }
        }
    }


    private fun onImagePickDialog() {
        val itemList = arrayOf<CharSequence>("Camera", "Gallery", "Document")
        val mDialog = AlertDialog.Builder(this)
        mDialog.setTitle("Upload ID Card")
        mDialog.setItems(itemList) { dialogInterface, i ->
            when {
                itemList[i] == "Camera" -> {
                    cameraPermission.launch(
                        Manifest.permission.CAMERA
                    )
                }
                itemList[i] == "Gallery" -> {
                    galleryPermission.launch(
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                }
                itemList[i] == "Document" -> {
                    if (documentCheckPermission()){
                        showFileChooser()
                    }else{
                       ActivityCompat.requestPermissions(this,arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),111)
                    }

                }
            }
        }

        mDialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 120 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            val document = data.data;
            binding.ivIdCard.visibility = View.GONE
            binding.tvPdfName.visibility = View.VISIBLE
            idCard = FileUtil.from(this,document)
            val filename: String = document?.path?.substring(document?.path!!.lastIndexOf("/") + 1).toString()
            binding.tvPdfName.text = filename
            binding.rlUploadImage.visibility=View.GONE
        }
    }



    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 111) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showFileChooser()
            } else {
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    private val takePicture =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { isSaved ->
            if (isSaved) {
                binding.ivIdCard.visibility = View.VISIBLE
                binding.tvPdfName.visibility = View.GONE
                idCard = imageFile
                Glide.with(this).load(imageFile).into(binding.ivIdCard)
                binding.rlUploadImage.visibility=View.GONE
            }
        }

    private fun showFileChooser() {
        val intent = Intent()
        intent.type = "*/*"
        val mimetypes = arrayOf(
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/msword","application/pdf"
        )
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Pdf"), 120)
    }

    @SuppressLint("NewApi")
    private fun documentCheckPermission(): Boolean {
        if(ActivityCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED){
            return true
        }
        return false
    }
}