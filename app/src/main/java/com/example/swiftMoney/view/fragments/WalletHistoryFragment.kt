package com.example.swiftMoney.view.fragments

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.os.Environment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.example.swiftMoney.R
import com.example.swiftMoney.adapter.WalletHistoryTabAdapter
import com.example.swiftMoney.base.BaseFragment
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.databinding.FragmentWalletHistoryBinding
import com.example.swiftMoney.model.walletHistory.Data
import com.example.swiftMoney.model.walletHistory.WalletHistoryParamModel
import com.example.swiftMoney.utils.SharedPreference
import com.example.swiftMoney.utils.Status
import com.example.swiftMoney.utils.showToast
import com.example.swiftMoney.view.FilterDialog
import com.example.swiftMoney.viewModel.ViewModelFactory
import com.example.swiftMoney.viewModel.WalletViewModel
import com.google.android.material.snackbar.Snackbar
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import java.io.File
import java.io.FileOutputStream
import java.util.*


class WalletHistoryFragment : BaseFragment(), View.OnClickListener {


    private val viewModel by viewModels<WalletViewModel> {
        ViewModelFactory(
            application,
            repository
        )
    }
    lateinit var binding: FragmentWalletHistoryBinding
    var dashboardTransactionList: ArrayList<Data> = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWalletHistoryBinding.inflate(layoutInflater)
        binding.onClick = this
        observer()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var companyId = GetObjects.preference.getInteger(SharedPreference.Key.COMPANYID, 0)
        viewModel.getWalletHistory(WalletHistoryParamModel(companyId.toString(), "", "", "", ""))
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(text: Editable?) {
                var list: ArrayList<Data> = ArrayList()
                if (text?.length!! >= 2) {
                    binding.rvWalletHistory.visibility = View.VISIBLE
                    for (i in dashboardTransactionList.indices) {
                        if (dashboardTransactionList[i].trans_ref.toLowerCase().contains(text)) {
                            if (!list.contains(dashboardTransactionList[i])) {
                                list.add(dashboardTransactionList[i])
                            }
                        }
                    }
                    setWalletHistoryAdapter(list)
                } else {
                    setWalletHistoryAdapter(dashboardTransactionList)
                }
            }
        })

    }

    private fun observer() {
        viewModel.resultWalletHistory.observe(viewLifecycleOwner) {
            it.let { data ->
                when (data.status) {
                    Status.SUCCESS -> {
                        MyApplication.hideLoader()
                        it.data?.data?.let { it1 -> setWalletHistoryAdapter(it1) }
                        it.data?.data?.let { it1 -> dashboardTransactionList.addAll(it1) }

                        if (it.data?.data.isNullOrEmpty()) {
                            binding.tvNoTransactionFound.visibility = View.VISIBLE
                            binding.rvWalletHistory.visibility = View.GONE
                        } else {
                            binding.tvNoTransactionFound.visibility = View.GONE
                            binding.rvWalletHistory.visibility = View.VISIBLE
                        }
                    }
                    Status.LOADING -> {
                        MyApplication.showLoader(requireContext())
                    }
                    Status.ERROR -> {
                        MyApplication.hideLoader()
                        requireContext().showToast(it?.message.toString())
                    }
                }
            }
        }
    }

    private fun setWalletHistoryAdapter(body: List<Data>) {
        val adapter = WalletHistoryTabAdapter(requireContext(), body)
        binding.rvWalletHistory.adapter = adapter
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.relativeLayout -> {
                FilterDialog().show(parentFragmentManager, "")
            }
            binding.rlDownload -> {
                if (checkReadWritePermission()) {
                    MyApplication.showLoader(requireContext())
                    createExcelSheet(dashboardTransactionList)
                }
            }
        }
    }

    fun createExcelSheet(data: ArrayList<Data>) {

        val hssfWorkBook = HSSFWorkbook()
        val hssSheet = hssfWorkBook.createSheet()
        val hssRow = hssSheet.createRow(0)
        val dateTimeCell = hssRow.createCell(0)
        dateTimeCell.setCellValue("Date Time")

        val transactionReference = hssRow.createCell(1)
        transactionReference.setCellValue("Transaction Ref")

        val description = hssRow.createCell(2)
        description.setCellValue("Description")

        val amount = hssRow.createCell(3)
        amount.setCellValue("Amount")

        val fee = hssRow.createCell(4)
        fee.setCellValue("Fee")


        val total = hssRow.createCell(5)
        total.setCellValue("Total")

        val balance = hssRow.createCell(6)
        balance.setCellValue("Balance")

        val currency = hssRow.createCell(7)
        currency.setCellValue("Currency")

        val type = hssRow.createCell(8)
        type.setCellValue("Type")

        val initiatedBy = hssRow.createCell(9)
        initiatedBy.setCellValue("Initiated By")

        for (i in data.indices) {
            val row = hssSheet.createRow(i + 1)
            for (j in 0..9) {
                if (j == 0) {
                    for (k in data.indices) {
                        val cell = row.createCell(0)
                        cell.setCellValue(data[i].date_time.toString())
                    }
                }
                if (j == 1) {
                    for (k in data.indices) {
                        val cell = row.createCell(1)
                        cell.setCellValue(data[i].trans_ref.toString())
                    }
                }
                if (j == 2) {
                    for (k in data.indices) {
                        val cell = row.createCell(2)
                        cell.setCellValue(data[i].description.toString())
                    }
                }
                if (j == 3) {
                    for (k in data.indices) {
                        val cell = row.createCell(3)
                        cell.setCellValue(data[i].amount.toString())
                    }
                }
                if (j == 4) {
                    for (k in data.indices) {
                        val cell = row.createCell(4)
                        cell.setCellValue(data[i].fee.toString())
                    }
                }
                if (j == 5) {
                    for (k in data.indices) {
                        val cell = row.createCell(5)
                        cell.setCellValue(data[i].total.toString())
                    }
                }
                if (j == 6) {
                    for (k in data.indices) {
                        val cell = row.createCell(6)
                        cell.setCellValue(data[i].balance.toString())
                    }
                }
                if (j == 7) {
                    for (k in data.indices) {
                        val cell = row.createCell(7)
                        cell.setCellValue(data[i].currency.toString())
                    }
                }
                if (j == 8) {
                    for (k in data.indices) {
                        val cell = row.createCell(8)
                        cell.setCellValue(data[i].type.toString())
                    }
                }
                if (j == 9) {
                    for (k in data.indices) {
                        val cell = row.createCell(9)
                        cell.setCellValue(data[i].initiated_by.toString())
                    }
                }
            }
        }
        try {
            val fileOutputStream = FileOutputStream(createFileForExcel())
            hssfWorkBook.write(fileOutputStream)
            if (fileOutputStream != null) {
                fileOutputStream.flush()
                fileOutputStream.close()
            }
            MyApplication.hideLoader()
            val snackbar: Snackbar =
                Snackbar.make(requireView(), "Successfully downloaded", Snackbar.LENGTH_LONG)
            val snackBarView = snackbar.view
            snackBarView.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.purple_700
                )
            )
            val textView =
                snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
            textView.setTextColor(Color.BLACK)
            snackbar.show()
        } catch (e: Exception) {
            MyApplication.hideLoader()
            e.printStackTrace()
        }
    }

    private fun createFileForExcel(): File {
        val localStorage: File? =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        val storagePath = localStorage?.absolutePath
        val rootPath = "$storagePath"
        val uniqueString: String = UUID.randomUUID().toString()
        val fileName = "/MoneySwiftWalletHistory$uniqueString.xls"
        val root = File(rootPath)
        if (!root.mkdirs()) {
            Log.i("Test", "This path is already exist: " + root.absolutePath)
        }
        val file = File(rootPath + fileName)
        return file
    }

    fun checkReadWritePermission(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 101
            )
            return false
        }
        return true
    }
}