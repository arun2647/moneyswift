package com.example.swiftMoney.view

import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.swiftMoney.adapter.CompanyDashboardAdapter
import com.example.swiftMoney.adapter.CompanyTransactionFilterAdapter
import com.example.swiftMoney.adapter.WalletHistoryFilterAdapter
import com.example.swiftMoney.base.BaseActivity
import com.example.swiftMoney.databinding.ActivityFilterTransactionBinding
import com.example.swiftMoney.model.companyTransactionFilter.Data
import com.example.swiftMoney.utils.ReceiptDialog

class FilterTransactionActivity : BaseActivity(), View.OnClickListener {

    lateinit var binding: ActivityFilterTransactionBinding
    var transactionList: ArrayList<com.example.swiftMoney.model.companyDashboard.Data> = ArrayList()
    var walletList: ArrayList<com.example.swiftMoney.model.walletHistory.Data> = ArrayList()
    var isFromHome: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFilterTransactionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.onClick = this
        isFromHome = intent.getBooleanExtra("isFromHome", false)
        try {
            if (isFromHome){
                transactionList = intent.getSerializableExtra("transactionList") as ArrayList<com.example.swiftMoney.model.companyDashboard.Data>
                setTransactionAdapter(transactionList)
            }else{
                walletList=intent.getSerializableExtra("transactionList") as ArrayList<com.example.swiftMoney.model.walletHistory.Data>
                setWalletHistory(walletList)
            }



        } catch (e: Exception) {
            binding?.tvNoTransactionFound.visibility=View.VISIBLE
            binding?.rvWalletHistory.visibility=View.GONE
            e.printStackTrace()
        }

    }

    private fun setTransactionAdapter(transactionList: ArrayList<com.example.swiftMoney.model.companyDashboard.Data>) {
        if (transactionList.isNotEmpty()) {
            Log.d("TAG", "setTransactionAdapter: $transactionList ")
            binding.tvNoTransactionFound.visibility = View.GONE
            binding.rvWalletHistory.visibility = View.VISIBLE
                val adapter = CompanyTransactionFilterAdapter(this, transactionList,callback)
                binding.rvWalletHistory.adapter = adapter
        } else {
            Log.d("TAG", "setTransactionAdapter: $transactionList ")
            binding.tvNoTransactionFound.visibility = View.VISIBLE
            binding.rvWalletHistory.visibility = View.GONE
        }

    }

    private fun setWalletHistory(transactionList: ArrayList<com.example.swiftMoney.model.walletHistory.Data>) {
        if (transactionList.isNotEmpty()) {
            Log.d("TAG", "setTransactionAdapter: $transactionList ")
            binding.tvNoTransactionFound.visibility = View.GONE
            binding.rvWalletHistory.visibility = View.VISIBLE

                val adapter = WalletHistoryFilterAdapter(this, transactionList)
                binding.rvWalletHistory.adapter = adapter

        } else {
            Log.d("TAG", "setTransactionAdapter: $transactionList ")
            binding.tvNoTransactionFound.visibility = View.VISIBLE
            binding.rvWalletHistory.visibility = View.GONE
        }

    }

    var callback = object : CompanyTransactionFilterAdapter.TransactionFilterCallback {
        override fun onClickOrderId(data: com.example.swiftMoney.model.companyDashboard.Data) {
            val fragment = ReceiptDialog()
            val bundle = Bundle()
            bundle.putSerializable("data", data)
            bundle.putBoolean("isFromFiler",false)
            fragment.arguments = bundle
            fragment.show(supportFragmentManager, "")
        }
    }


    override fun onClick(v: View?) {
        when (v) {
            binding.ivBack -> {
                onBackPressed()
            }
        }
    }
}