package com.example.swiftMoney.view

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.Gravity
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.example.swiftMoney.R
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.databinding.ActivityHomeBinding
import com.example.swiftMoney.utils.SharedPreference
import com.example.swiftMoney.utils.setTint
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class HomeActivity : AppCompatActivity(), View.OnClickListener {


    lateinit var binding: ActivityHomeBinding
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        binding.onClick = this
        binding.navigationLayout.onClick = this
        setContentView(binding.root)
        printHashKey(this)
        navController = this.findNavController(R.id.nav_host_fragment)
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.navHome -> {
                navController.popBackStack(R.id.nav_two, true)
                navController.popBackStack(R.id.nav_currency, true)
                navController.popBackStack(R.id.nav_wallet, true)
                navController.navigate(R.id.nav_home)
                binding.ivDrawer.visibility = View.VISIBLE
                binding.textView10.text = "Dashboard"
                binding.vv1.visibility = View.VISIBLE
                binding.vv2.visibility = View.GONE
                binding.vv3.visibility = View.GONE
                binding.vv4.visibility = View.GONE
                binding.navHome.setTint(R.color.bg_blue)
                binding.navTwo.setTint(R.color.txt_color_search_grey)
                binding.navCurrency.setTint(R.color.txt_color_search_grey)
                binding.navWallet.setTint(R.color.txt_color_search_grey)
            }
            binding.navTwo -> {
                binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                navController.popBackStack(R.id.nav_home, true)
                navController.popBackStack(R.id.nav_currency, true)
                navController.popBackStack(R.id.nav_wallet, true)
                navController.navigate(R.id.nav_two)
                binding.ivDrawer.visibility = View.GONE
                binding.textView10.text = "Submit transaction"
                binding.vv1.visibility = View.GONE
                binding.vv2.visibility = View.VISIBLE
                binding.vv3.visibility = View.GONE
                binding.vv4.visibility = View.GONE
                binding.navHome.setTint(R.color.txt_color_search_grey)
                binding.navTwo.setTint(R.color.bg_blue)
                binding.navCurrency.setTint(R.color.txt_color_search_grey)
                binding.navWallet.setTint(R.color.txt_color_search_grey)
            }

            binding.navCurrency -> {
                binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                navController.popBackStack(R.id.nav_two, true)
                navController.popBackStack(R.id.nav_home, true)
                navController.popBackStack(R.id.nav_wallet, true)
                navController.navigate(R.id.nav_currency)
                binding.ivDrawer.visibility = View.GONE
                binding.textView10.text = "Convert Currency"
                binding.vv1.visibility = View.GONE
                binding.vv2.visibility = View.GONE
                binding.vv3.visibility = View.VISIBLE
                binding.vv4.visibility = View.GONE
                binding.navHome.setTint(R.color.txt_color_search_grey)
                binding.navTwo.setTint(R.color.txt_color_search_grey)
                binding.navCurrency.setTint(R.color.bg_blue)
                binding.navWallet.setTint(R.color.txt_color_search_grey)
            }
            binding.navWallet -> {
                binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                navController.popBackStack(R.id.nav_two, true)
                navController.popBackStack(R.id.nav_currency, true)
                navController.popBackStack(R.id.nav_home, true)
                navController.navigate(R.id.nav_wallet)
                binding.ivDrawer.visibility = View.GONE
                binding.textView10.text = "Wallet history"
                binding.vv1.visibility = View.GONE
                binding.vv2.visibility = View.GONE
                binding.vv3.visibility = View.GONE
                binding.vv4.visibility = View.VISIBLE
                binding.navHome.setTint(R.color.txt_color_search_grey)
                binding.navTwo.setTint(R.color.txt_color_search_grey)
                binding.navCurrency.setTint(R.color.txt_color_search_grey)
                binding.navWallet.setTint(R.color.bg_blue)
            }
            binding.ivDrawer -> {
                binding.drawerLayout.openDrawer(Gravity.LEFT)
            }
            binding.navigationLayout.btnCompanyProfile -> {
                startActivity(Intent(this, CompanyProfileActivity::class.java))
                binding.drawerLayout.closeDrawer(Gravity.LEFT)
            }
            binding.navigationLayout.btnEditProfile -> {
                startActivity(Intent(this, EditProfileActivity::class.java))
                binding.drawerLayout.closeDrawer(Gravity.LEFT)
            }
            binding.navigationLayout.btnAddStaff -> {
                startActivity(Intent(this, AddStaffActivity::class.java))
                binding.drawerLayout.closeDrawer(Gravity.LEFT)
            }
            binding.navigationLayout.btnStaffList -> {
                startActivity(Intent(this, StaffListActivity::class.java))
                binding.drawerLayout.closeDrawer(Gravity.LEFT)
            }
            binding.navigationLayout.btnLogout -> {
                logout()
            }
        }
    }


    private fun logout() {
        val builder1: AlertDialog.Builder = AlertDialog.Builder(this)
        builder1.setMessage("Are you sure want to logout?")
        builder1.setCancelable(true)

        builder1.setPositiveButton(
            "Yes"
        ) { _, _ ->
            GetObjects.preference.put(SharedPreference.Key.ISUSERLOGIN, false)
            startActivity(Intent(this, LoginActivity::class.java))
            finishAffinity()
        }

        builder1.setNegativeButton(
            "No"
        ) { dialog, id -> dialog.dismiss() }

        val alert11: AlertDialog = builder1.create()
        alert11.show()
    }

    private fun printHashKey(pContext: Context) {
        try {
            val info: PackageInfo = pContext.getPackageManager()
                .getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey: String = String(Base64.encode(md.digest(), 0))
                Log.i("TAG", "printHashKey() Hash Key: $hashKey")
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e("TAG", "printHashKey()", e)
        } catch (e: Exception) {
            Log.e("TAG", "printHashKey()", e)
        }
    }
}