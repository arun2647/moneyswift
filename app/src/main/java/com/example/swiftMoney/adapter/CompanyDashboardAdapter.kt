package com.example.swiftMoney.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagingData
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.swiftMoney.R
import com.example.swiftMoney.databinding.ItemHomeBinding
import com.example.swiftMoney.model.companyDashboard.Data
import java.lang.Exception

class CompanyDashboardAdapter(val context: Context,val callback:HomeTransactionCallback) :
    PagingDataAdapter<Data, CompanyDashboardAdapter.ViewHolder>(DataDifferntiator) {


    inner class ViewHolder(val binding: ItemHomeBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindData(position: Int) {
            val body = getItem(position)
            try {
                binding.tvInitiatedBy.text = body?.initiated_by
                binding.tvDateTime.text = body?.date_time
                binding.tvAmount.text = "$${body?.amount}"
                binding.tvOrder.text = "#${body?.transaction_id}"
                binding.tvFee.text = "$${body?.fee}"
                binding.tvBeneficiaryName.text = "${body?.beneficiary_name}"
                try {
                    val amount = body?.amount?.toString()?.toDouble()
                    val fee = body?.fee?.toString()?.toDouble()
                    val totalPrice = fee?.toDouble()?.let { amount?.toDouble()?.plus(it) }
                    val a = String.format("%.2f", totalPrice).toDouble()
                    binding.tvTotal.text = "$${a}"
                }catch (e:Exception){
                    e.printStackTrace()
                }



                val background = binding.materialButton.background
                if (body?.status == "Pending") {
                    background.setTint(ContextCompat.getColor(context, R.color.btn_yellow))
                    binding.materialButton.setTextColor(
                        ContextCompat.getColor(
                            context,
                            R.color.btn__text_yellow
                        )
                    )
                } else if (body?.status == "Completed") {
                    background.setTint(ContextCompat.getColor(context, R.color.btn_green))
                    binding.materialButton.setTextColor(
                        ContextCompat.getColor(
                            context,
                            R.color.btn_text_green
                        )
                    )
                } else if (body?.status == "Refund") {
                    background.setTint(ContextCompat.getColor(context, R.color.btn_red))
                    binding.materialButton.setTextColor(
                        ContextCompat.getColor(
                            context,
                            R.color.btn_text_red
                        )
                    )
                }
                binding.materialButton.text = body?.status
                //  binding.tvTotal.text = "$${body[position].total}"
                //   binding.tvFee.text = "$${body[position].total} ${body[position].currency}"
            }catch (e:IndexOutOfBoundsException){
                e.printStackTrace()
            }
        }

    }

    object DataDifferntiator : DiffUtil.ItemCallback<Data>() {


        override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem == newItem
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemHomeBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isNullOrEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            val newItem = position
            holder.bindData(newItem)
        }
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(position)
        val body=getItem(position)
        holder.binding.tvOrder.setOnClickListener {
            body?.let { it1 -> callback.onClickOrderId(it1) }
        }
        holder.binding.tvOrderTitle.setOnClickListener {
            body?.let { it1 -> callback.onClickOrderId(it1) }
        }
    }


    interface HomeTransactionCallback{
        fun onClickOrderId(data: Data)
    }
}