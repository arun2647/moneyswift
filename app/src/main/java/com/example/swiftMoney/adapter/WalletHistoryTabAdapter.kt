package com.example.swiftMoney.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.swiftMoney.databinding.ItemWalletHistoryBinding
import com.example.swiftMoney.model.walletHistory.Data

class WalletHistoryTabAdapter(val context: Context, val body: List<Data>) :
    RecyclerView.Adapter<WalletHistoryTabAdapter.ViewHolder>() {


    inner class ViewHolder(val binding: ItemWalletHistoryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindData(position: Int) {
            binding.tvDateTime.text = body[position].date_time
            binding.tvAmount.text = "$${body[position].amount}"
            binding.tvName.text = body[position].trans_ref
            binding.tvWallet.text = body[position].type + "(${body[position].currency})"
            //binding.tvFee.text = "$${body[position].total} ${body[position].currency}"
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemWalletHistoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(position)
    }

    override fun getItemCount(): Int = body.size
}