package com.example.swiftMoney.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.swiftMoney.databinding.ItemDocumentBinding

class DocumentListAdapter(val context: Context, val body: List<String>,val callback:DocumentCallback) :
    RecyclerView.Adapter<DocumentListAdapter.ViewHolder>() {
    var count = 1

    inner class ViewHolder(val binding: ItemDocumentBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(position: Int) {
            if (position == body.lastIndex) {
                binding.tvDocument.text = "Doc $count"
            } else {
                binding.tvDocument.text = "Doc $count,"
            }
            count++
       itemView.setOnClickListener {
           callback.onClickDocument(body[position])
       }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemDocumentBinding.inflate(LayoutInflater.from(context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
      holder.bindData(position)
    }

    override fun getItemCount(): Int = body.size


    interface DocumentCallback{
        fun onClickDocument(url:String)
    }
}