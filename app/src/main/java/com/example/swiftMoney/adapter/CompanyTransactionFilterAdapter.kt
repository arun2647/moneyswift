package com.example.swiftMoney.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.swiftMoney.R
import com.example.swiftMoney.databinding.ItemHomeBinding
import com.example.swiftMoney.model.companyTransactionFilter.Data
import java.lang.Exception

class CompanyTransactionFilterAdapter(val context: Context,val body:ArrayList<com.example.swiftMoney.model.companyDashboard.Data>,val callback:TransactionFilterCallback):RecyclerView.Adapter<CompanyTransactionFilterAdapter.ViewHolder>() {

    inner class ViewHolder(val binding:ItemHomeBinding):RecyclerView.ViewHolder(binding.root){
        fun bindData(position: Int) {
            binding.tvInitiatedBy.text = body[position].initiated_by
            binding.tvDateTime.text = body[position].date_time
            binding.tvAmount.text = "$${body[position].amount}"
            binding.tvOrder.text="#${body[position].transaction_id}"
            binding.tvFee.text = "$${body[position].fee}"
            binding.tvBeneficiaryName.text = "$${body[position].beneficiary_name}"
            try {
                val amount = body[position].amount?.toString()?.toDouble()
                val fee = body[position].fee?.toString()?.toDouble()
                val totalPrice = fee?.toDouble()?.let { amount?.toDouble()?.plus(it) }
                val a = String.format("%.2f", totalPrice).toDouble()
                binding.tvTotal.text = "$${a}"
            }catch (e: Exception){
                e.printStackTrace()
            }


            val background=binding.materialButton.background
            if (body[position].status=="Pending"){
                background.setTint(ContextCompat.getColor(context, R.color.btn_yellow))
                binding.materialButton.setTextColor(ContextCompat.getColor(context, R.color.btn__text_yellow))
            }else if (body[position].status=="Completed"){
                background.setTint(ContextCompat.getColor(context, R.color.btn_green))
                binding.materialButton.setTextColor(ContextCompat.getColor(context, R.color.btn_text_green))
            }else if (body[position].status=="Refund"){
                background.setTint(ContextCompat.getColor(context, R.color.btn_red))
                binding.materialButton.setTextColor(ContextCompat.getColor(context, R.color.btn_text_red))
            }
            binding.materialButton.text=body[position].status
            //  binding.tvTotal.text = "$${body[position].total}"
            //   binding.tvFee.text = "$${body[position].total} ${body[position].currency}"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemHomeBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
      holder.bindData(position)
        holder.binding.tvOrder.setOnClickListener {
            body[position]?.let { it1 -> callback.onClickOrderId(it1) }
        }
        holder.binding.tvOrderTitle.setOnClickListener {
            body[position]?.let { it1 -> callback.onClickOrderId(it1) }
        }
    }

    override fun getItemCount(): Int =body.size

    interface TransactionFilterCallback{
        fun onClickOrderId(data:com.example.swiftMoney.model.companyDashboard.Data)
    }
}