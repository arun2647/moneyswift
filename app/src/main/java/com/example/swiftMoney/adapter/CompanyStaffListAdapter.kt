package com.example.swiftMoney.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.swiftMoney.databinding.ItemStaffListBinding
import com.example.swiftMoney.model.companyStaffList.Data
import com.example.swiftMoney.view.StaffDetailActivity

class CompanyStaffListAdapter(val context: Context, val body: List<Data>) :
    RecyclerView.Adapter<CompanyStaffListAdapter.ViewHolder>() {


    inner class ViewHolder(val binding: ItemStaffListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(position: Int) {
            binding.tvName.text = body[position].first_name + " " + body[position].last_name
            binding.tvEmail.text = body[position].email
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemStaffListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(position)
        holder.binding.btnViewProfile.setOnClickListener {
            val intent = Intent(context, StaffDetailActivity::class.java)
            intent.putExtra("staffId", body[position].user_id)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int = body.size


}