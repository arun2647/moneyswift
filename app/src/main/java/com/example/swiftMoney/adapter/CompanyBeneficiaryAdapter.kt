package com.example.swiftMoney.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.swiftMoney.databinding.ItemSpinnerBinding
import com.example.swiftMoney.model.companyBeneficiary.Data

class CompanyBeneficiaryAdapter(val context: Context,val data: List<Data>,val callback:CompanyBeneficiaryCallback):RecyclerView.Adapter<CompanyBeneficiaryAdapter.ViewHolder>(){




    inner class ViewHolder(val binding:ItemSpinnerBinding):RecyclerView.ViewHolder(binding.root){
        fun bindData(position:Int){
            binding.tvBeneficiaryName.text=data[position].beneficiary_name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemSpinnerBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(position)
        holder.itemView.setOnClickListener {
            callback.onClickItem(data[position].id.toString(),data[position].beneficiary_name)
        }
    }

    override fun getItemCount(): Int =data.size

    interface CompanyBeneficiaryCallback{
        fun onClickItem(beneficiaryId:String,beneficiaryName:String)
    }
}