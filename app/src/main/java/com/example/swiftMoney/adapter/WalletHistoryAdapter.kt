package com.example.swiftMoney.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.swiftMoney.databinding.ItemHomeBinding
import com.example.swiftMoney.model.walletHistory.Data

class WalletHistoryAdapter(val context: Context, val body: List<Data>) :
    RecyclerView.Adapter<WalletHistoryAdapter.ViewHolder>() {


    inner class ViewHolder(val binding: ItemHomeBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindData(position: Int) {
            binding.tvInitiatedBy.text = body[position].initiated_by
            binding.tvDateTime.text = body[position].date_time
            binding.tvAmount.text = "$${body[position].amount}"
            binding.tvTotal.text = "$${body[position].total}"
            binding.tvFee.text = "$${body[position].total} ${body[position].currency}"
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemHomeBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(position)
    }

    override fun getItemCount(): Int = body.size
}