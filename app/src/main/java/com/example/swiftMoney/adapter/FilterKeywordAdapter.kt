package com.example.swiftMoney.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.swiftMoney.databinding.ItemKeywordBinding

class FilterKeywordAdapter(
    val context: Context,
    val body: ArrayList<String>,
    val callback: FilterKeywordCallback
) :
    RecyclerView.Adapter<FilterKeywordAdapter.ViewHolder>() {

    val list: ArrayList<String> = ArrayList()

    inner class ViewHolder(val binding: ItemKeywordBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(position: Int) {
            binding.tvKeyword.text = body[position]
            list.add(body[position])
            callback.getKeywordName(list)
            binding.btnClose.setOnClickListener {
                list.remove(body[position])
               callback.onRemoveItem(position)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemKeywordBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(position)

    }

    override fun getItemCount(): Int = body.size

    interface FilterKeywordCallback {
        fun getKeywordName(nameList: ArrayList<String>)
        fun onRemoveItem(position: Int)
    }
}