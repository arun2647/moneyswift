package com.example.swiftMoney.utils

enum class Status {
    SUCCESS,
    LOADING,
    ERROR
}