package com.example.swiftMoney.utils

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.swiftMoney.adapter.DocumentListAdapter
import com.example.swiftMoney.base.GetObjects
import com.example.swiftMoney.databinding.ItemReceiptBinding
import com.example.swiftMoney.model.companyDashboard.Data


class ReceiptDialog : DialogFragment(), View.OnClickListener {
    var data: Data? = null
    var data2: com.example.swiftMoney.model.companyTransactionFilter.Data? = null
    var isFromFilter:Boolean=false
    lateinit var binding: ItemReceiptBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ItemReceiptBinding.inflate(layoutInflater)
        binding.onClick = this
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        val dialog: Dialog? = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window?.setLayout(width, height)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);
        isFromFilter= arguments?.getBoolean("isFromFiler") == true
        if (isFromFilter){
            data2 = arguments?.getSerializable("data") as? com.example.swiftMoney.model.companyTransactionFilter.Data
            binding.tvOrderDate.text = data2?.order_date
            binding.tvReferenceNumberTop.text = data2?.trans_ref
            binding.tvReferenceNumber.text = data2?.trans_ref
            binding.tvTransactionID.text=data?.transaction_id.toString()
            binding.tvCompletedDate.text = data2?.completed_date
            binding.tvTransactionStatus.text = data?.status
            binding.tvCurrency.text = data2?.currency
            when (data2?.currency) {
                "GBP" -> {
                    binding.tvIbanNumberTitle.visibility = View.VISIBLE
                    binding.tvIBANNumber.visibility = View.VISIBLE
                    binding.tvSortTitle.visibility = View.VISIBLE
                    binding.tvSortSwiftCode.visibility = View.VISIBLE
                    binding.tvBic.visibility = View.VISIBLE
                    binding.tvBicTitle.visibility = View.VISIBLE
                    binding.tvIBANNumber.text = data2?.iban_number
                    binding.tvSortSwiftCode.text = data2?.sort_swift_code
                    binding.tvBic.text = data2?.bic
                }
                "USD" -> {
                    binding.tvACHRoutingTitle.visibility = View.VISIBLE
                    binding.tvAchRouting.visibility = View.VISIBLE
                    binding.tvWireRouting.visibility = View.VISIBLE
                    binding.tvWireRoutingNumber.visibility = View.VISIBLE
                    binding.tvAchRouting.text = data2?.account_routing_number
                    binding.tvWireRouting.text = data2?.wire_routing_number
                }
                "EUR" -> {
                    binding.tvIBANNumber.visibility = View.VISIBLE
                    binding.tvIbanNumberTitle.visibility = View.VISIBLE
                    binding.tvBic.visibility = View.VISIBLE
                    binding.tvBicTitle.visibility = View.VISIBLE
                    binding.tvIBANNumber.text = data2?.iban_number
                    binding.tvBic.text = data2?.bic
                }
                "CAD" -> {
                    binding.tvAchRouting.visibility = View.VISIBLE
                    binding.tvACHRoutingTitle.visibility = View.VISIBLE
                    binding.tvWireRouting.visibility = View.VISIBLE
                    binding.tvWireRoutingNumber.visibility = View.VISIBLE
                    binding.tvBranchNumber.visibility = View.VISIBLE
                    binding.tvBranchTitle.visibility = View.VISIBLE
                    binding.tvAchRouting.text = data2?.account_routing_number
                    binding.tvWireRouting.text = data2?.wire_routing_number
                    binding.tvBranchNumber.text = data2?.branch_number
                }
            }
            binding.tvAmount.text = data2?.currency + " " + data2?.amount
            binding.tvTransactionFee.text = data2?.currency + " " + data2?.fee
            var subTotal: Double = 0.0
            try {
                subTotal = data2?.amount?.toDouble()?.plus(data2?.fee?.toDouble()!!)?.toDouble()!!
            } catch (e: Exception) {
                e.printStackTrace()
            }

            binding.tvSubTotal.text = data2?.currency + " " + subTotal.toString()
            binding.tvTransactionType.text = data2?.transaction_mode
            binding.tvName.text = data2?.beneficiary_name
           /* binding.tvAddress.text = data2?.beneficiary_address
            binding.tvCity.text = data2?.beneficiary_city
            binding.tvCountry.text = data2?.beneficiary_country
            binding.tvState.text = data2?.beneficiary_state
            binding.tvZip.text = data2?.beneficiary_zip*/
            binding.tvPhoneNumber.text = data2?.beneficiary_phone
            binding.tvEmail.text = data2?.beneficiary_email
            binding.tvBankAddress.text = data2?.bank_address
            binding.tvBankCountry.text = data2?.bank_country
        /*    binding.tvBankCity.text = data2?.bank_city
            binding.tvBankZip.text = data2?.bank_zipcode
            binding.tvBankName.text = data2?.bank_name
            binding.tvBankState.text = data2?.bank_state*/
            binding.tvAccountNumber.text = data2?.account_number
            binding.tvBusinessType.text = data2?.business_type.toString()
         /*   binding.tvReferenceMemo.text = data2?.reference_memo*/
          //  binding.tvSupportingDocument.text = data2?.documents
            binding.textView26.text = data2?.initiated_by
        }else{
            data = arguments?.getSerializable("data") as? Data
            binding.tvOrderDate.text = data?.order_date
            binding.tvReferenceNumberTop.text = data?.trans_ref
            binding.tvReferenceNumber.text = data?.trans_ref
            binding.tvTransactionID.text=data?.transaction_id.toString()
            binding.tvCompletedDate.text = data?.completed_date
            binding.tvTransactionStatus.text = data?.status
            binding.tvCurrency.text = data?.currency
            when (data?.currency) {
                "GBP" -> {
                    binding.tvIbanNumberTitle.visibility = View.VISIBLE
                    binding.tvIBANNumber.visibility = View.VISIBLE
                    binding.tvSortTitle.visibility = View.VISIBLE
                    binding.tvSortSwiftCode.visibility = View.VISIBLE
                    binding.tvBic.visibility = View.VISIBLE
                    binding.tvBicTitle.visibility = View.VISIBLE
                    binding.tvIBANNumber.text = data?.iban_number
                    binding.tvSortSwiftCode.text = data?.sort_swift_code
                    binding.tvBic.text = data?.bic
                }
                "USD" -> {
                    binding.tvACHRoutingTitle.visibility = View.VISIBLE
                    binding.tvAchRouting.visibility = View.VISIBLE
                    binding.tvWireRouting.visibility = View.VISIBLE
                    binding.tvWireRoutingNumber.visibility = View.VISIBLE
                    binding.tvAchRouting.text = data?.account_routing_number
                    binding.tvWireRouting.text = data?.wire_routing_number
                }
                "EUR" -> {
                    binding.tvIBANNumber.visibility = View.VISIBLE
                    binding.tvIbanNumberTitle.visibility = View.VISIBLE
                    binding.tvBic.visibility = View.VISIBLE
                    binding.tvBicTitle.visibility = View.VISIBLE
                    binding.tvIBANNumber.text = data?.iban_number
                    binding.tvBic.text = data?.bic
                }
                "CAD" -> {
                    binding.tvAchRouting.visibility = View.VISIBLE
                    binding.tvACHRoutingTitle.visibility = View.VISIBLE
                    binding.tvWireRouting.visibility = View.VISIBLE
                    binding.tvWireRoutingNumber.visibility = View.VISIBLE
                    binding.tvBranchNumber.visibility = View.VISIBLE
                    binding.tvBranchTitle.visibility = View.VISIBLE
                    binding.tvAchRouting.text = data?.account_routing_number
                    binding.tvWireRouting.text = data?.wire_routing_number
                    binding.tvBranchNumber.text = data?.branch_number
                }
            }
            binding.tvAmount.text = data?.currency + " " + data?.amount
            binding.tvTransactionFee.text = data?.currency + " " + data?.fee
            var subTotal: Double = 0.0
            try {
                subTotal = data?.amount?.toDouble()?.plus(data?.fee?.toDouble()!!)?.toDouble()!!
            } catch (e: Exception) {
                e.printStackTrace()
            }

            binding.tvSubTotal.text = data?.currency + " " + subTotal.toString()
            binding.tvTransactionType.text = data?.transaction_mode
            binding.tvName.text = data?.beneficiary_name
            binding.tvAddress.text = data?.beneficiary_address
            binding.tvCity.text = data?.beneficiary_city
            binding.tvCountry.text = data?.beneficiary_country
            binding.tvState.text = data?.beneficiary_state
            binding.tvZip.text = data?.beneficiary_zip
            binding.tvPhoneNumber.text = data?.beneficiary_phone
            binding.tvEmail.text = data?.beneficiary_email
            binding.tvBankAddress.text = data?.bank_address
            binding.tvBankCountry.text = data?.bank_country
            binding.tvBankCity.text = data?.bank_city
            binding.tvBankZip.text = data?.bank_zipcode
            binding.tvBankName.text = data?.bank_name
            binding.tvBankState.text = data?.bank_state
            binding.tvAccountNumber.text = data?.account_number
            binding.tvBusinessType.text = data?.business_type
            binding.tvReferenceMemo.text = data?.reference_memo
            binding.tvSupportingDocument.adapter=
                data?.documents?.let { DocumentListAdapter(requireContext(), it,documentCallback) }
          //  binding.tvSupportingDocument.text = data?.documents?.joinToString(",")
            binding.textView26.text = data?.initiated_by
        }


    }
    val documentCallback=object :DocumentListAdapter.DocumentCallback{
        override fun onClickDocument(url: String) {
            try {
                val browserIntent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(url)
                )
                startActivity(browserIntent)
            }catch (e:java.lang.Exception){
                e.printStackTrace()
            }
        }

    }

    override fun onClick(v: View?) {
        when (v) {
            binding.ivClose -> {
                dismiss()
            }


            binding.ivDownload -> {
                if (isFromFilter){
                    val browserIntent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://swiftmoney.techpriyanka.com/receipt/${data2?.transaction_id}")
                    )
                    startActivity(browserIntent)
                }else{
                    val browserIntent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://swiftmoney.techpriyanka.com/receipt/${data?.transaction_id}")
                    )
                    startActivity(browserIntent)
                }

            }
            binding.ivEmail -> {
                val intent = Intent(Intent.ACTION_SEND)
                //provide email address of the recipient as data
                intent.data = Uri.parse(
                    "mailto:${
                        GetObjects.preference.getString(
                            SharedPreference.Key.EMAIL,
                            ""
                        )
                    }"
                )
                //now we will add extras with the mail
                if (isFromFilter){
                    val TO =
                        arrayOf("${GetObjects.preference.getString(SharedPreference.Key.EMAIL, "")}")
                    intent.putExtra(Intent.EXTRA_EMAIL, TO);
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Transaction receipt")
                    intent.putExtra(
                        Intent.EXTRA_TEXT,
                        "https://swiftmoney.techpriyanka.com/receipt/${data2?.transaction_id}"
                    )
                    //set the type of mail
                    intent.type = "text/html"
                    startActivity(
                        Intent.createChooser(
                            intent,
                            "Send Email Using: "
                        )
                    );
                }else{
                    val TO =
                        arrayOf("${GetObjects.preference.getString(SharedPreference.Key.EMAIL, "")}")
                    intent.putExtra(Intent.EXTRA_EMAIL, TO);
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Transaction receipt")
                    intent.putExtra(
                        Intent.EXTRA_TEXT,
                        "https://swiftmoney.techpriyanka.com/receipt/${data?.transaction_id}"
                    )
                    //set the type of mail
                    intent.type = "text/html"
                    startActivity(
                        Intent.createChooser(
                            intent,
                            "Send Email Using: "
                        )
                    );
                }

            }
        }
    }
}