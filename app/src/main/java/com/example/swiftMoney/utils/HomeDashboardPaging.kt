package com.example.swiftMoney.utils

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.swiftMoney.model.companyDashboard.Data
import com.example.swiftMoney.network.GetApi

class HomeDashboardPaging(val id: String) : PagingSource<Int, Data>() {
    private val getApi = GetApi.api

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Data> {
        try {
            var currentLoadingPageKey = params.key ?: 1

            val response = getApi?.getCompanyDashboard(id, currentLoadingPageKey.toString())

            val prevKey = if (currentLoadingPageKey == 1) null else currentLoadingPageKey - 1


            var a = 0
            var type = 1
            if (response?.body()?.totalrecords!! % response.body()?.recordsperpage!! == 0) {
                val c = response.body()!!.totalrecords.toDouble()
                    .div(response.body()!!.recordsperpage.toDouble()).toInt()
                a=c
            } else {
                var b = response.body()!!.totalrecords.toDouble()
                    .div(response.body()!!.recordsperpage.toDouble()).toInt()
                b = b.plus(1)
                type = 2
                a=b
            }
            return LoadResult.Page(
                data = response?.body()?.data!!,
                prevKey = prevKey,
                nextKey = if (currentLoadingPageKey==a) null else currentLoadingPageKey.plus(1)

            )
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }

        /*  try {
              val currentLoadingPageKey = params.key ?: 1
              val jsonResponse = getApi?.getCompanyDashboard(id, currentLoadingPageKey.toString())
              val data = jsonResponse?.body()?.data ?: emptyList()
              val nextKey = if (jsonResponse?.body()?.data?.isEmpty() == true) {
                  null
              } else {
                  // initial load size = 3 * NETWORK_PAGE_SIZE
                  // ensure we're not requesting duplicating items, at the 2nd request
                  currentLoadingPageKey + (params.loadSize / 500)
              }
            return  LoadResult.Page(
                  data = listOf(data),
                  prevKey = null, // Only paging forward.
                  // assume that if a full page is not loaded, that means the end of the data
                  nextKey = nextKey
              )
          } catch (e: Exception) {
              LoadResult.Error(e)
          }*/


    }

    override fun getRefreshKey(state: PagingState<Int, Data>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
        //  return null
    }
}