package com.example.swiftMoney.utils

import android.app.AlertDialog
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.location.Address
import android.location.Geocoder
import android.net.ConnectivityManager
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.swiftMoney.R
import com.google.android.material.imageview.ShapeableImageView
import com.google.android.material.navigation.NavigationView
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.makeramen.roundedimageview.RoundedImageView
import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

fun getError(throwable: Throwable): String {
    if (throwable is UnknownHostException) {
        return "No Internet Connection"
    } else if (throwable is SocketTimeoutException) {
        return "Server is not responding. Please try again"
    } else if (throwable is ConnectException) {
        return "Failed to connect server"
    } else {
        return throwable.message.toString()
    }
}
fun ImageView.setTint(@ColorRes colorRes: Int) {
    ImageViewCompat.setImageTintList(this, ColorStateList.valueOf(ContextCompat.getColor(context, colorRes)))
}
fun Context.showToast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}
fun NavigationView.changeCornerRadius() {
    val navViewBackground : MaterialShapeDrawable = background as MaterialShapeDrawable
    val radius = resources.getDimension(R.dimen.roundCorner)
    navViewBackground.shapeAppearanceModel = navViewBackground.shapeAppearanceModel
        .toBuilder()
        .setTopLeftCorner(CornerFamily.ROUNDED, radius)
        .setBottomLeftCorner(CornerFamily.ROUNDED, radius)
        .build()
}

fun Context.loadImage(url: String, imageView: ImageView, progressBar: ProgressBar) {
    Glide.with(this).load(url).listener(object : RequestListener<Drawable> {
        override fun onLoadFailed(
            e: GlideException?,
            model: Any?,
            target: Target<Drawable>?,
            isFirstResource: Boolean
        ): Boolean {
            progressBar.visibility = View.GONE
            return false
        }

        override fun onResourceReady(
            resource: Drawable?,
            model: Any?,
            target: Target<Drawable>?,
            dataSource: DataSource?,
            isFirstResource: Boolean
        ): Boolean {
            progressBar.visibility = View.GONE
            return false
        }
    })?.placeholder(R.mipmap.place_holder).error(R.mipmap.place_holder).into(imageView)
}

fun Context.loadImageShapeable(
    url: String,
    imageView: ShapeableImageView,
    progressBar: ProgressBar
) {
    Glide.with(this).load(url).listener(object : RequestListener<Drawable> {
        override fun onLoadFailed(
            e: GlideException?,
            model: Any?,
            target: Target<Drawable>?,
            isFirstResource: Boolean
        ): Boolean {
            progressBar.visibility = View.GONE
            return false
        }

        override fun onResourceReady(
            resource: Drawable?,
            model: Any?,
            target: Target<Drawable>?,
            dataSource: DataSource?,
            isFirstResource: Boolean
        ): Boolean {
            progressBar.visibility = View.GONE
            return false
        }
    }).placeholder(R.mipmap.place_holder).error(R.mipmap.place_holder).into(imageView)
}



fun Context.loadImageRound(
    url: String,
    imageView: RoundedImageView,
    progressBar: ProgressBar
) {

    Log.d("TAG", "loadImageRound: $url")

    Glide.with(this).load(url).listener(object : RequestListener<Drawable> {
        override fun onLoadFailed(
            e: GlideException?,
            model: Any?,
            target: Target<Drawable>?,
            isFirstResource: Boolean
        ): Boolean {
            progressBar.visibility = View.GONE
            return false
        }

        override fun onResourceReady(
            resource: Drawable?,
            model: Any?,
            target: Target<Drawable>?,
            dataSource: DataSource?,
            isFirstResource: Boolean
        ): Boolean {

            progressBar.visibility = View.GONE
            return false
        }
    }).placeholder(R.mipmap.place_holder).error(R.mipmap.place_holder).into(imageView)
}


fun Context.alterDialog(message: String, alterDialogCallback: AlertDialogCallback) {
    val builder = AlertDialog.Builder(this)
    builder.setMessage(message)
    builder.setCancelable(false)
    builder.setPositiveButton("Yes") { _, _ ->
        alterDialogCallback.onYes()
    }
    builder.setNegativeButton("No") { dialog, _ ->
        dialog.dismiss()
    }
    builder.create()
    builder.show()
}

interface AlertDialogCallback {
    fun onYes()
}


class Utility {
    companion object {

        fun removeSecondsFromTIme(time:String): String {
            val simpleDateFormat=SimpleDateFormat("HH:mm:ss", Locale.ENGLISH)
            val apiTime=simpleDateFormat.parse(time)
            val formattedTime=SimpleDateFormat("HH:mm",Locale.ENGLISH)
            return formattedTime.format(apiTime)
        }

        fun getStateAndCountryName(context: Context?,latitude:Double,Longitude:Double): String {
            val geoCoder = Geocoder(context, Locale.getDefault())
            var address = ""
            try {
                val addresses: List<Address> = geoCoder.getFromLocation(latitude, Longitude, 1)

                if (addresses.size > 0) {
                    println(addresses.get(0).getLocality());
                    address="${addresses.get(0).getLocality()},${addresses.get(0).getCountryName()}"
                    println(addresses.get(0).getCountryName());
                }
            } catch (e1: IOException) {
                e1.printStackTrace()
            }
            return address
        }

        fun getStateAndCountryName2(context: Context?,latitude:Double,Longitude:Double): String {
            val geoCoder = Geocoder(context, Locale.getDefault())
            var address = ""
            try {
                val addresses: List<Address> = geoCoder.getFromLocation(latitude, Longitude, 1)

                if (addresses.size > 0) {
                    println(addresses.get(0).getLocality());
                    address= addresses[0].getAddressLine(0)
                    println(addresses.get(0).getCountryName());
                }
            } catch (e1: IOException) {
                e1.printStackTrace()
            }
            return address
        }
        fun emailValidation(editText: EditText): Boolean {
            return Pattern.matches(
                Patterns.EMAIL_ADDRESS.toRegex().toString(),
                editText.text.toString()
            )
        }

        fun passwordValidation(password: String?): Boolean {
            password?.let {
                val passwordPattern = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,})"
                val passwordMatcher = Regex(passwordPattern)
                return passwordMatcher.find(password) != null
            } ?: return false
        }

        fun getCurrentDate(): String? {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
            val currentTime = Date().time
            return dateFormat.format(currentTime)
        }

        fun getCurrentTime(): String? {
            val timeFormat = SimpleDateFormat("hh:mm:ss", Locale.ENGLISH)
            val currentTime = Date().time
            return timeFormat.format(currentTime)
        }

        fun isInternetConnected(context: Context?): Boolean {
            val manager: ConnectivityManager =
                context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val network = manager.activeNetworkInfo
            return network != null && network.isConnectedOrConnecting
        }
        fun formatServerDate(date:Date): String {
            val format=SimpleDateFormat("hh:mm a", Locale.ENGLISH)
            return format.format(date)
        }


    }
}