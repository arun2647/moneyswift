package com.example.swiftMoney.utils

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider

class GoogleSIgnIn{


    companion object {

        val RC_CODE = 101
        var mAuth: FirebaseAuth? = null
        var instance: GoogleSIgnIn? = null
        var callBack: CallbackGoogleSignIn?=null
        var mGoogleSignInOptions: GoogleSignInOptions? = null
        var mGoogleSignInClient: GoogleSignInClient? = null

        @JvmName("getInstance1")
        fun getInstance(context: Context,clientId:String): GoogleSIgnIn? {
            if (instance == null) {
                instance = GoogleSIgnIn()
                getGoogleSignInClient(context,clientId)
            }
            return instance
        }

        fun getGoogleSignInClient(context: Context,clientId:String){
            mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(clientId)
                .requestEmail()
                .build()
            mGoogleSignInClient = GoogleSignIn.getClient(context, mGoogleSignInOptions!!)
        }
        
        fun fireBaseAuthWithGoogle(account: GoogleSignInAccount) {
            mAuth = FirebaseAuth.getInstance()
            val credential = GoogleAuthProvider.getCredential(account.idToken, null)
            mAuth?.signInWithCredential(credential)?.addOnCompleteListener { task ->
                if (task.isSuccessful){
                    Log.d(ContentValues.TAG, "signInWithCredential:success");
                    callBack?.onSuccessGoogleSignIn(mAuth?.currentUser,account.displayName)
                }else{
                    Log.w(ContentValues.TAG, "signInWithCredential:failure", task.exception);
                    callBack?.onFailureGoogleSignIn(task.exception)
                }
            }
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_CODE) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)!!
                fireBaseAuthWithGoogle(account)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w(ContentValues.TAG, "Google sign in failed", e)
                callBack?.onApiException(e.message)
            }
        }
    }

    fun googleSignInIntent(activity: Activity) {
        val intent = mGoogleSignInClient?.signInIntent
        activity.startActivityForResult(intent, RC_CODE)
    }

}