package com.example.swiftMoney.utils

import com.google.firebase.auth.FirebaseUser
import java.lang.Exception

interface CallbackGoogleSignIn {
    fun onSuccessGoogleSignIn(result: FirebaseUser?,name:String?)
    fun onFailureGoogleSignIn(exception: Exception?)
    fun onApiException(message: String?)
}