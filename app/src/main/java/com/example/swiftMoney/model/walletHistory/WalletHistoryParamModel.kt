package com.example.swiftMoney.model.walletHistory

data class WalletHistoryParamModel(
    val comp_id:String,
    val range_to:String,
    val range_from:String,
    val type:String,
    val wallet:String
)
