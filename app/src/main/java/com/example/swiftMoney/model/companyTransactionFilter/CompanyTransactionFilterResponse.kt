package com.example.swiftMoney.model.companyTransactionFilter

import com.example.swiftMoney.model.companyDashboard.Data

data class CompanyTransactionFilterResponse(
    val data: ArrayList<Data>,
    val result: Boolean,
    val message:String
)