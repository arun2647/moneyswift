package com.example.swiftMoney.model.submitTransaction

data class SubmitTransactionResponse(
    val message: String,
    val result: Boolean
)