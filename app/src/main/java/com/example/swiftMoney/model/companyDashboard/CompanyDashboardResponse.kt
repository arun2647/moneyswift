package com.example.swiftMoney.model.companyDashboard

data class CompanyDashboardResponse(
    val data: List<Data>,
    val result: Boolean,
    var message:String,
    val totalrecords: Int,
    val recordsperpage: Int,
)