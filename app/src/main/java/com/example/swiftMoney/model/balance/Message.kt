package com.example.swiftMoney.model.balance

data class Message(
    val amount: String,
    val balance: String,
    val company_id: Int,
    val created_at: String,
    val created_by: Int,
    val currency: String,
    val id: Int,
    val updated_at: String,
    val updated_by: Int
)