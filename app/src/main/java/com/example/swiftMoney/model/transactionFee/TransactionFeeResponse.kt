package com.example.swiftMoney.model.transactionFee

data class TransactionFeeResponse(
    val `data`: Data,
    val result: Boolean,
    var message:String
)