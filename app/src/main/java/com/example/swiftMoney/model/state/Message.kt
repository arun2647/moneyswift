package com.example.swiftMoney.model.state

data class Message(
    val country_id: Int,
    val id: Int,
    val name: String
)