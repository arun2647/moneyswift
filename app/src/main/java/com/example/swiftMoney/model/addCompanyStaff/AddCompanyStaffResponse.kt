package com.example.swiftMoney.model.addCompanyStaff

data class AddCompanyStaffResponse(
    val message: String,
    val result: Boolean
)