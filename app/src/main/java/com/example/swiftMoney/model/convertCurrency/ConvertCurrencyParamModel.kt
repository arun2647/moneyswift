package com.example.swiftMoney.model.convertCurrency

data class ConvertCurrencyParamModel(
    val currencyfrom:String,
    val currencyto:String,
    val amount:String,
    val user_id:String,
    val company_id:String,
)
