package com.example.swiftMoney.model.editProfile

data class EditProfileResponse(
    val message: String,
    val result: Boolean
)