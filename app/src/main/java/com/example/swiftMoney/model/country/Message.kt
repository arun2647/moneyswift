package com.example.swiftMoney.model.country

data class Message(
    val country_code: String,
    val country_for: Int,
    val country_name: String,
    val id: Int,
    val phone_code: Int,
    val status: Int
)