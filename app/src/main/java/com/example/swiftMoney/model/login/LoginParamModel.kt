package com.example.swiftMoney.model.login

data class LoginParamModel(val email:String,val password:String,val login_type:String,val provider_id:String)
