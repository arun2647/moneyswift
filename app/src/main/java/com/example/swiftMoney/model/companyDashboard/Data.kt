package com.example.swiftMoney.model.companyDashboard

import android.os.Parcelable
import java.io.Serializable


data class Data(
    val account_number: String,
    val account_routing_number: String,
    val amount: String,
    val bank_address: String,
    val bank_country: String,
    val beneficiary_email: String,
    val beneficiary_name: String,
    val beneficiary_phone: String,
    val bic: String,
    val branch_number: String,
    val business_type: String,
    val completed_date: String,
    val date_time: String,
    val documents: ArrayList<String>,
    val currency:String,
    val fee: String,
    val iban_number: String,
    val initiated_by: String,
    val order_date: String,
    val pdf: String,
    val sort_swift_code: String,
    val status: String,
    val trans_ref: String,
    val transaction_id: Int,
    val transaction_mode: String,
    val type: String,
    val wire_routing_number: String,
    val beneficiary_zip: String,
    val beneficiary_address: String,
    val beneficiary_city: String,
    val beneficiary_country: String,
    val beneficiary_state: String,
    val bank_state: String,
    val bank_zipcode: String,
    val bank_name: String,
    val bank_city: String,
    val reference_memo: String,

):Serializable