package com.example.swiftMoney.model.walletHistory

data class WalletHistoryResponse(
    val data: ArrayList<Data>,
    val message:String,
    val result: Boolean
)