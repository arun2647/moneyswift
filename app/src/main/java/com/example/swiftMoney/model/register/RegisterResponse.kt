package com.example.swiftMoney.model.register

data class RegisterResponse(
    val message: String,
    val result: Boolean
)