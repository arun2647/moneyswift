package com.example.swiftMoney.model.beneficiaryId

data class BeneficiaryIdResponse(
    val `data`: Data,
    val result: Boolean,
    val message:String
)