package com.example.swiftMoney.model.forgotPassword

data class ForgotPasswordResponse(
    val message: String,
    val success: Boolean
)