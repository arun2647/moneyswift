package com.example.swiftMoney.model.login

data class Data(
    val access_token: String,
    val company_id: Int,
    val email: String,
    val first_name: String,
    val id_card: Any,
    val last_name: String,
    val phone_number: Any,
    val user_id: Int
)