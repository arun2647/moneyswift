package com.example.swiftMoney.model.companyDetail

data class Data(
    val comp_id: Int,
    val company_address: String,
    val company_country: String,
    val company_state: String,
    val company_city: String,
    val company_zip: String,
    val company_license: String,
    val company_name: String,
    val company_phone: String,
    val company_registration_no: String,
    val company_shortcode: Any,
    val company_website: String,
    val email: String,
    val type_of_business: String
)