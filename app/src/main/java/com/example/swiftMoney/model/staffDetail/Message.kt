package com.example.swiftMoney.model.staffDetail

data class Message(
    val api_token: String,
    val company_id: Int,
    val created_at: String,
    val created_by: String,
    val email: String,
    val email_verified: Int,
    val email_verified_at: Any,
    val first_name: String,
    val id: Int,
    val id_card: Any,
    val is_deleted: Int,
    val is_enabled: Int,
    val is_status: Int,
    val last_name: String,
    val otp: Int,
    val otp_generated: Any,
    val parent_id: String,
    val phone_number: Any,
    val role_id: String,
    val title: Any,
    val updated_at: String,
    val updated_by: String,
    val user_ip: String,
    val user_type: String
)