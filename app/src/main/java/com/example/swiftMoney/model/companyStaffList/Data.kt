package com.example.swiftMoney.model.companyStaffList

data class Data(
    val company_id: Int,
    val email: String,
    val first_name: String,
    val id_card: String,
    val last_name: String,
    val phone_number: Any,
    val user_id: Int
)