package com.example.swiftMoney.model.companyBeneficiary

data class CompanyBeneficiaryResponse(
    val `data`: List<Data>,
    val result: Boolean,
    val message:String,
)