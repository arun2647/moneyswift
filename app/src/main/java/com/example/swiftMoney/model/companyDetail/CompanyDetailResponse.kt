package com.example.swiftMoney.model.companyDetail

data class CompanyDetailResponse(
    val data: Data,
    val result: Boolean,
    val message:String
)