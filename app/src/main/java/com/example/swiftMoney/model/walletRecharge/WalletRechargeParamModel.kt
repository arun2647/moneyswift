package com.example.swiftMoney.model.walletRecharge

data class WalletRechargeParamModel(
    val user_id:String,
    val company_id:String,
    val amount:String,
    val currency:String,
    val reference_num:String,
    val reference_date:String,
    val comment:String,
)
