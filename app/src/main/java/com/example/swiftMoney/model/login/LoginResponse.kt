package com.example.swiftMoney.model.login

data class LoginResponse(
    val data: Data,
    val message:String,
    val result: Boolean
)