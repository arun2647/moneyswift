package com.example.swiftMoney.model.walletHistory

import java.io.Serializable

data class Data(
    val amount: String,
    val balance: String,
    val company_id: String,
    val currency: String,
    val date_time: String,
    val description: String,
    val fee: String,
    val initiated_by: String,
    val total: String,
    val trans_ref: String,
    val type: String
):Serializable