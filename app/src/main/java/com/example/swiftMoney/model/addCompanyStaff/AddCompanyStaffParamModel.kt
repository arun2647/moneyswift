package com.example.swiftMoney.model.addCompanyStaff

import java.io.File

data class AddCompanyStaffParamModel(
    val company_id: String,
    val email: String,
    val first_name: String,
    val last_name: String,
    val password: String,
    val id_card: File?
)
