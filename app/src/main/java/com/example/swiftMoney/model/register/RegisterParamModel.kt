package com.example.swiftMoney.model.register

data class RegisterParamModel(
    val first_name:String,
    val last_name:String,
    val email:String,
    val company_name:String,
    val type_of_business:String,
    val company_website:String,
    val company_address:String,
    val company_city:String,
    val company_state:String,
    val company_zip:String,
    val company_country:String,
    val key_contact_phone:String,
    val company_email:String,
    val password:String,
    val user_id:String
)


