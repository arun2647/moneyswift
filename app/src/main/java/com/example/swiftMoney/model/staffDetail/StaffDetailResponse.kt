package com.example.swiftMoney.model.staffDetail

data class StaffDetailResponse(
    val message: List<Message>,
    val result: Boolean,
    val fileBaseurl:String
)