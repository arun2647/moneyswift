package com.example.swiftMoney.model.editProfile

import java.io.File

data class EditProfileParamModel(

    val company_id:String,
    val staff_id:String,
    val first_name:String,
    val last_name:String,
    val password:String,
    val email:String,
    var idCard:File?
)
