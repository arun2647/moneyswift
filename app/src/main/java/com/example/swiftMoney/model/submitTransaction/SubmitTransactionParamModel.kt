package com.example.swiftMoney.model.submitTransaction

import java.io.File

data class SubmitTransactionParamModel(
    val beneficiary_id:String,
    val beneficiary_type:String,
    val beneficiary_name:String,
    val beneficiary_email:String,
    val beneficiary_phone:String,
    val beneficiary_address:String,
    val beneficiary_city:String,
    val beneficiary_state:String,
    val beneficiary_country:String,
    val beneficiary_zip:String,
    val company_id:String,
    val bank_name:String,
    val bank_address:String,
    val bank_zipcode:String,
    val bank_city:String,
    val bank_state:String,
    val bank_country:String,
    val account_number:String,
    val user_id:String,
    val wallet:String,
    val amount:String,
    val reference_memo:String,
    val individual_business:String,
    val bic:String,
    val branch_number:String,
    val sort_swift_code:String,
    val iban_number:String,
    val wire_routing_number:String,
    val account_routing_number:String,
    val device:String,
    val documents:ArrayList<File>?
)
