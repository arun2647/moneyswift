package com.example.swiftMoney.model.country

data class CountryResponse(
    val message: List<Message>,
    val result: Boolean
)