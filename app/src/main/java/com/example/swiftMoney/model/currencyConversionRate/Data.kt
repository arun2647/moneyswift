package com.example.swiftMoney.model.currencyConversionRate

data class Data(
    val USD: Double=0.0,
    val EUR: Double=0.0,
    val GBP: Double=0.0,
    val CAD: Double=0.0
)