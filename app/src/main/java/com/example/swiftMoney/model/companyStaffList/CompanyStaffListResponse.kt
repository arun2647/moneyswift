package com.example.swiftMoney.model.companyStaffList

data class CompanyStaffListResponse(
    val `data`: List<Data>,
    val message:String,
    val result: Boolean
)