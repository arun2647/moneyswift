package com.example.swiftMoney.model.state

data class StateResponse(
    val message: List<Message>,
    val result: Boolean
)