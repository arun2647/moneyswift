package com.example.swiftMoney.model.companyTransactionFilter

data class CompanyTransactionFilterParamModel(
    val comp_id:String,
    val type:String,
    val fromamount:String,
    val toamount:String,
    val staff:String,
    val range_to:String,
    val range_from:String,
    val keyword:String,
    val currency:String
)
