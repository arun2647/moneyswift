package com.example.swiftMoney.model.transactionFee

data class TransactionFeeParamModel(
    val company_id: String,
    val currency: String,
    val amount: String
)
