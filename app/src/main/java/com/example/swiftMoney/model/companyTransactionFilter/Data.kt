package com.example.swiftMoney.model.companyTransactionFilter

import java.io.Serializable

data class Data(
    val account_number: String,
    val account_routing_number: String,
    val amount: String,
    val bank_address: String,
    val bank_country: String,
    val beneficiary_email: String,
    val beneficiary_name: String,
    val beneficiary_phone: String,
    val bic: String,
    val branch_number: String,
    val business_type: Any,
    val completed_date: String,
    val currency: String,
    val date_time: String,
    val documents: String,
    val fee: String,
    val iban_number: String,
    val initiated_by: String,
    val order_date: String,
    val pdf: String,
    val sort_swift_code: String,
    val status: String,
    val trans_ref: String,
    val transaction_id: Int,
    val transaction_mode: String,
    val type: String,
    val wire_routing_number: String
):Serializable