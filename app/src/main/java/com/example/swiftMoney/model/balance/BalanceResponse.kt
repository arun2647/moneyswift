package com.example.swiftMoney.model.balance

data class BalanceResponse(
    val message: List<Message>,
    val result: Boolean
)