package com.example.swiftMoney.model.convertCurrency

data class ConvertCurrencyResponse(
    val message: String,
    val result: Boolean
)