package com.example.swiftMoney.model.currencyConversionRate

data class CurrencyConversionRateParamModel(
    val company_id:String,
    val currencyfrom:String,
    val currencyto:String,
)
