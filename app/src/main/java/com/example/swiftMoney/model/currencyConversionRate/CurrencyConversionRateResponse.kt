package com.example.swiftMoney.model.currencyConversionRate

data class CurrencyConversionRateResponse(
    val data: Data,
    val result: Boolean,
    val message:String
)