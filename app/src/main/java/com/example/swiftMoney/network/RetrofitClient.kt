package com.example.swiftMoney.network

import android.util.Log
import com.example.swiftMoney.BuildConfig
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.utils.SharedPreference

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitClient {

    init {
        retrofitClient = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    companion object {
        var retrofitClient: Retrofit? = null
        val okHttpClient = OkHttpClient.Builder()
            .callTimeout(80, TimeUnit.SECONDS)
            .connectTimeout(80, TimeUnit.SECONDS)
            .writeTimeout(80, TimeUnit.SECONDS)
            .readTimeout(80, TimeUnit.SECONDS)
            .addInterceptor(loggingHttpInjector())
            .addInterceptor(clientInterceptor())
            .build()

        private fun loggingHttpInjector(): HttpLoggingInterceptor {
            val logger = HttpLoggingInterceptor { message ->
                Log.d("injector", message)
            }
            logger.setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE)
            return logger
        }

        private fun clientInterceptor(): Interceptor {
            return Interceptor { chain ->
                var request: Request
                val token = SharedPreference.getInstance(MyApplication.getContext()).getString(SharedPreference.Key.TOKEN, "")
                println("token $token")
                request = if (!token.isNullOrEmpty()) {
                    chain.request().newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "text/json; charset=utf-8")
                        .addHeader("Authorization", "Bearer $token").build()
                } else {
                    chain.request().newBuilder()
                        .build()
                }
                chain.proceed(request)
            }
        }


    }

    fun getApi(): ApiInterface? {
        return retrofitClient!!.create(ApiInterface::class.java)
    }


}