package com.example.swiftMoney.network

import com.example.swiftMoney.model.addCompanyStaff.AddCompanyStaffResponse
import com.example.swiftMoney.model.balance.BalanceResponse
import com.example.swiftMoney.model.beneficiaryId.BeneficiaryIdResponse
import com.example.swiftMoney.model.companyBeneficiary.CompanyBeneficiaryResponse
import com.example.swiftMoney.model.companyDashboard.CompanyDashboardResponse
import com.example.swiftMoney.model.companyDetail.CompanyDetailResponse
import com.example.swiftMoney.model.companyStaffList.CompanyStaffListResponse
import com.example.swiftMoney.model.companyTransactionFilter.CompanyTransactionFilterResponse
import com.example.swiftMoney.model.convertCurrency.ConvertCurrencyResponse
import com.example.swiftMoney.model.country.CountryResponse
import com.example.swiftMoney.model.currencyConversionRate.CurrencyConversionRateResponse
import com.example.swiftMoney.model.editProfile.EditProfileResponse
import com.example.swiftMoney.model.forgotPassword.ForgotPasswordResponse
import com.example.swiftMoney.model.login.LoginResponse
import com.example.swiftMoney.model.register.RegisterResponse
import com.example.swiftMoney.model.staffDetail.StaffDetailResponse
import com.example.swiftMoney.model.state.StateResponse
import com.example.swiftMoney.model.submitTransaction.SubmitTransactionResponse
import com.example.swiftMoney.model.transactionFee.TransactionFeeResponse
import com.example.swiftMoney.model.walletHistory.WalletHistoryResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


interface ApiInterface {

    @GET("api/staffregister")
    suspend fun staffRegister(
        @Query("first_name") first_name: String,
        @Query("last_name") last_name: String,
        @Query("email") email: String,
        @Query("company_name") company_name: String,
        @Query("type_of_business") type_of_business: String,
        @Query("company_website") company_website: String,
        @Query("company_address") company_address: String,
        @Query("company_city") company_city: String,
        @Query("company_state") company_state: String,
        @Query("company_zip") company_zip: String,
        @Query("company_country") company_country: String,
        @Query("key_contact_phone") key_contact_phone: String,
        @Query("company_email") company_email: String,
        @Query("password") password: String,
        @Query("user_id") user_id: String,
    ): Response<RegisterResponse>

    @GET("api/countries")
    suspend fun getCountryList(): Response<CountryResponse>

    @GET("api/getstates")
    suspend fun getStates(@Query("countryId") countryId: String): Response<StateResponse>

    @GET("api/stafflogin")
    suspend fun onLogin(
        @Query("email") email: String,
        @Query("password") password: String,
        @Query("login_type") login_type: String,
        @Query("provider_id") provider_id: String,
    ): Response<LoginResponse>

    @GET("api/walletrecharge")
    suspend fun onWalletRecharge(
        @Query("user_id") user_id: String,
        @Query("comp_id") company_id: String,
        @Query("amount") amount: String,
        @Query("currency") currency: String,
        @Query("reference_num") reference_num: String,
        @Query("reference_date") reference_date: String,
        @Query("comment") comment: String
    ): Response<LoginResponse>


    @GET("api/companywallethistory")
    suspend fun getWalletHistory(
        @Query("comp_id") comp_id: String,
        @Query("range_to") range_to: String,
        @Query("range_from") range_from: String,
        @Query("type") type: String,
        @Query("wallet") wallet: String,
    ): Response<WalletHistoryResponse>

    @GET("api/companydetail")
    suspend fun getCompanyDetail(@Query("comp_id") comp_id: String): Response<CompanyDetailResponse>

    @GET("api/companystaffs")
    suspend fun getCompanyStaffList(@Query("comp_id") comp_id: String): Response<CompanyStaffListResponse>

    @Multipart
    @POST("api/createstaff")
    suspend fun onAddCompanyStaff(
       @Part("company_id") company_id:RequestBody,
       @Part("email") email:RequestBody,
       @Part("first_name") first_name:RequestBody,
       @Part("last_name") last_name:RequestBody,
       @Part("password") password:RequestBody,
       @Part id_card:MultipartBody.Part?,
    ): Response<AddCompanyStaffResponse>


    @Multipart
    @POST("api/updatestaff")
    suspend fun onEditProfile(
        @Part("company_id") company_id: RequestBody,
        @Part("staff_id") staff_id: RequestBody,
        @Part("first_name") first_name: RequestBody,
        @Part("last_name") last_name: RequestBody,
        @Part("password") password: RequestBody,
        @Part("email") email: RequestBody,
        @Part id_card:MultipartBody.Part?,
    ): Response<EditProfileResponse>

    @GET("api/staffdetails")
    suspend fun getStaffDetail(
        @Query("staff_id") staff_id: String
    ): Response<StaffDetailResponse>


    @GET("api/resetPassword")
    suspend fun onResetPassword(@Query("email") email: String): Response<ForgotPasswordResponse>


    @GET("api/conversionrate")
     fun getConversionRate(
        @Query("company_id") company_id: String,
        @Query("currencyfrom") currencyfrom: String,
        @Query("currencyto") currencyto: String,
    ):Call<CurrencyConversionRateResponse>

    @Multipart
    @POST("api/submittransaction")
    suspend fun onSubmitTransaction(
        @Part("beneficiary_id") beneficiary_id:RequestBody,
        @Part("beneficiary_type") beneficiary_type:RequestBody,
        @Part("beneficiary_name") beneficiary_name:RequestBody,
        @Part("beneficiary_email") beneficiary_email:RequestBody,
        @Part("beneficiary_phone") beneficiary_phone:RequestBody,
        @Part("beneficiary_address") beneficiary_address:RequestBody,
        @Part("beneficiary_city") beneficiary_city:RequestBody,
        @Part("beneficiary_state") beneficiary_state:RequestBody,
        @Part("beneficiary_country") beneficiary_country:RequestBody,
        @Part("beneficiary_zip") beneficiary_zip:RequestBody,
        @Part("company_id") company_id:RequestBody,
        @Part("bank_name") bank_name:RequestBody,
        @Part("bank_address") bank_address:RequestBody,
        @Part("bank_city") bank_city:RequestBody,
        @Part("bank_state") bank_state:RequestBody,
        @Part("bank_country") bank_country:RequestBody,
        @Part("bank_zipcode") bank_zipcode:RequestBody,
        @Part("account_number") account_number:RequestBody,
        @Part("account_routing_number") account_routing_number:RequestBody,
        @Part("wire_routing_number") wire_routing_number:RequestBody,
        @Part("iban_number") iban_number:RequestBody,
        @Part("sort_swift_code") sort_swift_code:RequestBody,
        @Part("branch_number") branch_number:RequestBody,
        @Part("bic") bic:RequestBody,
        @Part("individual_business") individual_business:RequestBody,
        @Part("reference_memo") reference_memo:RequestBody,
        @Part("user_id") user_id:RequestBody,
        @Part("wallet") wallet:RequestBody,
        @Part("amount") amount:RequestBody,
        @Part("device") device:RequestBody,
        @Part documents:ArrayList<MultipartBody.Part?>
    ):Response<SubmitTransactionResponse>


    @GET("api/getCompanyDashboard/{id}")
    suspend fun getCompanyDashboard(
        @Path("id") id:String,
        @Query("page")page:String
    ):Response<CompanyDashboardResponse>


    @GET("api/getBalance/{id}")
    suspend fun getBalance(
        @Path("id") id:String
    ):Response<BalanceResponse>

    @GET("api/transactionfee")
    suspend fun getTransactionFee(
        @Query("company_id") company_id:String,
        @Query("currency") currency:String,
        @Query("amount") amount:String,
    ):Response<TransactionFeeResponse>

    @FormUrlEncoded
    @POST("api/convertcurrency")
    suspend fun onConvertCurrency(
        @Field("currencyfrom") currencyfrom:String,
        @Field("currencyto") currencyto:String,
        @Field("amount") amount:String,
        @Field("user_id") user_id:String,
        @Field("company_id") company_id:String,
    ):Response<CurrencyConversionRateResponse>

    @FormUrlEncoded
    @POST("api/companytransactions")
    suspend fun getCompanyTransactionFilter(
        @Field("comp_id") comp_id:String,
        @Field("type") type:String,
        @Field("fromamount") fromamount:String,
        @Field("toamount") toamount:String,
        @Field("staff") staff:String,
        @Field("range_to") range_to:String,
        @Field("range_from") range_from:String,
        @Field("keyword") keyword:String,
        @Field("currency") currency:String,
    ):Response<CompanyTransactionFilterResponse>


    @GET("api/getBeneficiaryById/{id}")
    suspend fun getBeneficiaryId(@Path ("id") id:String):Response<BeneficiaryIdResponse>



    @FormUrlEncoded
    @POST("api/companybeneficiary")
    suspend fun getCompanyBeneficiary(@Field("company_id") company_id:String):Response<CompanyBeneficiaryResponse>
}