package com.example.swiftMoney.network

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.example.swiftMoney.model.addCompanyStaff.AddCompanyStaffParamModel
import com.example.swiftMoney.model.addCompanyStaff.AddCompanyStaffResponse
import com.example.swiftMoney.model.balance.BalanceResponse
import com.example.swiftMoney.model.beneficiaryId.BeneficiaryIdResponse
import com.example.swiftMoney.model.companyBeneficiary.CompanyBeneficiaryResponse
import com.example.swiftMoney.model.companyDashboard.CompanyDashboardResponse
import com.example.swiftMoney.model.companyDetail.CompanyDetailResponse
import com.example.swiftMoney.model.companyStaffList.CompanyStaffListResponse
import com.example.swiftMoney.model.companyTransactionFilter.CompanyTransactionFilterParamModel
import com.example.swiftMoney.model.companyTransactionFilter.CompanyTransactionFilterResponse
import com.example.swiftMoney.model.convertCurrency.ConvertCurrencyParamModel
import com.example.swiftMoney.model.country.CountryResponse
import com.example.swiftMoney.model.currencyConversionRate.CurrencyConversionRateParamModel
import com.example.swiftMoney.model.currencyConversionRate.CurrencyConversionRateResponse
import com.example.swiftMoney.model.editProfile.EditProfileParamModel
import com.example.swiftMoney.model.editProfile.EditProfileResponse
import com.example.swiftMoney.model.forgotPassword.ForgotPasswordResponse
import com.example.swiftMoney.model.login.LoginParamModel
import com.example.swiftMoney.model.login.LoginResponse
import com.example.swiftMoney.model.register.RegisterParamModel
import com.example.swiftMoney.model.register.RegisterResponse
import com.example.swiftMoney.model.staffDetail.StaffDetailResponse
import com.example.swiftMoney.model.state.StateResponse
import com.example.swiftMoney.model.submitTransaction.SubmitTransactionParamModel
import com.example.swiftMoney.model.submitTransaction.SubmitTransactionResponse
import com.example.swiftMoney.model.transactionFee.TransactionFeeParamModel
import com.example.swiftMoney.model.transactionFee.TransactionFeeResponse
import com.example.swiftMoney.model.walletHistory.WalletHistoryParamModel
import com.example.swiftMoney.model.walletHistory.WalletHistoryResponse
import com.example.swiftMoney.model.walletRecharge.WalletRechargeParamModel
import com.example.swiftMoney.utils.HomeDashboardPaging
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Response


class Repository {
    private val getApi = GetApi.api

    suspend fun onStaffRegister(body: RegisterParamModel): Response<RegisterResponse> {
        return getApi!!.staffRegister(
            body.first_name,
            body.last_name,
            body.email,
            body.company_name,
            body.type_of_business,
            body.company_website,
            body.company_address,
            body.company_city,
            body.company_state,
            body.company_zip, body.company_country, body.key_contact_phone, body.company_email,
            body.password,
            body.user_id
        )
    }

    fun getPagingDashboard(id: String) = Pager(
        config = PagingConfig(20,100,false,7),
        pagingSourceFactory = { HomeDashboardPaging(id) },
        initialKey = 1
    ).liveData


    suspend fun getCountryList(): Response<CountryResponse> {
        return getApi!!.getCountryList()
    }

    suspend fun getStateList(countryId: String): Response<StateResponse> {
        return getApi!!.getStates(countryId)
    }

    suspend fun onLogin(body: LoginParamModel): Response<LoginResponse> {
        return getApi!!.onLogin(body.email, body.password, body.login_type, body.provider_id)
    }

    suspend fun onWalletRecharge(body: WalletRechargeParamModel): Response<LoginResponse> {
        return getApi!!.onWalletRecharge(
            body.user_id,
            body.company_id,
            body.amount,
            body.currency,
            body.reference_num,
            body.reference_date,
            body.comment
        )
    }

    suspend fun getWalletHistory(body: WalletHistoryParamModel): Response<WalletHistoryResponse> {
        return getApi!!.getWalletHistory(
            body.comp_id,
            body.range_to,
            body.range_from,
            body.type,
            body.wallet
        )
    }


    suspend fun getCompanyDetail(companyId: String): Response<CompanyDetailResponse> {
        return getApi!!.getCompanyDetail(companyId)
    }

    suspend fun getCompanyStaffList(companyId: String): Response<CompanyStaffListResponse> {
        return getApi!!.getCompanyStaffList(companyId)
    }

    suspend fun onAddCompanyStaff(body: AddCompanyStaffParamModel): Response<AddCompanyStaffResponse> {

        val companyId = body.company_id.toRequestBody("text/plain".toMediaTypeOrNull())
        val email = body.email.toRequestBody("text/plain".toMediaTypeOrNull())
        val firstName = body.first_name.toRequestBody("text/plain".toMediaTypeOrNull())
        val lastName = body.last_name.toRequestBody("text/plain".toMediaTypeOrNull())
        val password = body.password.toRequestBody("text/plain".toMediaTypeOrNull())
        val imageRequestBody = body.id_card?.asRequestBody("*/*".toMediaTypeOrNull())
        val image = imageRequestBody?.let {
            MultipartBody.Part.createFormData(
                "id_card", body.id_card?.name,
                it
            )
        }
        return getApi!!.onAddCompanyStaff(
            companyId,
            email,
            firstName,
            lastName,
            password,
            image
        )
    }

    suspend fun onEditProfile(body: EditProfileParamModel): Response<EditProfileResponse> {
        val companyId = body.company_id.toRequestBody("text/plain".toMediaTypeOrNull())
        val staffId = body.staff_id.toRequestBody("text/plain".toMediaTypeOrNull())
        val email = body.email.toRequestBody("text/plain".toMediaTypeOrNull())
        val firstName = body.first_name.toRequestBody("text/plain".toMediaTypeOrNull())
        val lastName = body.last_name.toRequestBody("text/plain".toMediaTypeOrNull())
        val password = body.password.toRequestBody("text/plain".toMediaTypeOrNull())
        val imageRequestBody = body.idCard?.asRequestBody("*/*".toMediaTypeOrNull())
        val image = imageRequestBody?.let {
            MultipartBody.Part.createFormData(
                "id_card", body.idCard?.name,
                it
            )
        }
        return getApi!!.onEditProfile(
            companyId,
            staffId,
            firstName,
            lastName,
            password,
            email,
            image
        )
    }


    suspend fun getStaffDetail(staff_id: String): Response<StaffDetailResponse> {
        return getApi!!.getStaffDetail(staff_id)
    }

    suspend fun onForgotPassword(email: String): Response<ForgotPasswordResponse> {
        return getApi!!.onResetPassword(email)
    }

      fun getConversionRate(body: CurrencyConversionRateParamModel): Call<CurrencyConversionRateResponse> {
        return getApi!!.getConversionRate(body.company_id, body.currencyfrom, body.currencyto)
    }

    suspend fun onSubmitTransaction(body: SubmitTransactionParamModel): Response<SubmitTransactionResponse> {
        val beneficiary_id = body.beneficiary_id.toRequestBody("text/plain".toMediaTypeOrNull())
        val beneficiary_type = body.beneficiary_type.toRequestBody("text/plain".toMediaTypeOrNull())
        val beneficiary_name = body.beneficiary_name.toRequestBody("text/plain".toMediaTypeOrNull())
        val beneficiary_email = body.beneficiary_email.toRequestBody("text/plain".toMediaTypeOrNull())
        val beneficiary_phone = body.beneficiary_phone.toRequestBody("text/plain".toMediaTypeOrNull())
        val beneficiary_address = body.beneficiary_address.toRequestBody("text/plain".toMediaTypeOrNull())
        val beneficiary_city = body.beneficiary_city.toRequestBody("text/plain".toMediaTypeOrNull())
        val beneficiary_state = body.beneficiary_state.toRequestBody("text/plain".toMediaTypeOrNull())
        val beneficiary_country = body.beneficiary_country.toRequestBody("text/plain".toMediaTypeOrNull())
        val beneficiary_zip = body.beneficiary_zip.toRequestBody("text/plain".toMediaTypeOrNull())
        val company_id = body.company_id.toRequestBody("text/plain".toMediaTypeOrNull())
        val bank_name = body.bank_name.toRequestBody("text/plain".toMediaTypeOrNull())
        val bank_address = body.bank_address.toRequestBody("text/plain".toMediaTypeOrNull())
        val bank_city = body.bank_city.toRequestBody("text/plain".toMediaTypeOrNull())
        val bank_state = body.bank_state.toRequestBody("text/plain".toMediaTypeOrNull())
        val bank_country = body.bank_country.toRequestBody("text/plain".toMediaTypeOrNull())
        val bank_zipcode = body.bank_zipcode.toRequestBody("text/plain".toMediaTypeOrNull())
        val account_number = body.account_number.toRequestBody("text/plain".toMediaTypeOrNull())
        val individual_business = body.individual_business.toRequestBody("text/plain".toMediaTypeOrNull())
        val bic = body.bic.toRequestBody("text/plain".toMediaTypeOrNull())
        val branch_number = body.branch_number.toRequestBody("text/plain".toMediaTypeOrNull())
        val sort_swift_code = body.sort_swift_code.toRequestBody("text/plain".toMediaTypeOrNull())
        val iban_number = body.iban_number.toRequestBody("text/plain".toMediaTypeOrNull())
        val wire_routing_number = body.wire_routing_number.toRequestBody("text/plain".toMediaTypeOrNull())
        val account_routing_number = body.account_routing_number.toRequestBody("text/plain".toMediaTypeOrNull())
        val reference = body.reference_memo.toRequestBody("text/plain".toMediaTypeOrNull())
        val user_id = body.user_id.toRequestBody("text/plain".toMediaTypeOrNull())
        val wallet = body.wallet.toRequestBody("text/plain".toMediaTypeOrNull())
        val amount = body.amount.toRequestBody("text/plain".toMediaTypeOrNull())
        val device = body.device.toRequestBody("text/plain".toMediaTypeOrNull())
        val imageList:ArrayList<MultipartBody.Part?> = ArrayList()
        if (!body.documents.isNullOrEmpty()){
            for (i in body.documents){
                var imageRequestBody:RequestBody
                if (i.name.startsWith("NEWIMAGE_")){
                     imageRequestBody = i?.asRequestBody("image/*".toMediaTypeOrNull())

                }else{
                    imageRequestBody = i?.asRequestBody("*/*".toMediaTypeOrNull())
                }

                imageList.add(imageRequestBody?.let {
                    MultipartBody.Part.createFormData(
                        "documents[]",i.name,
                        it
                    )
                })
            }
        }else{
            val file = RequestBody.create(MultipartBody.FORM, "")
            imageList.add(MultipartBody.Part.createFormData("file", "", file))
        }

        return getApi!!.onSubmitTransaction(
            beneficiary_id,
            beneficiary_type,
            beneficiary_name,
            beneficiary_email,
            beneficiary_phone,
            beneficiary_address,
            beneficiary_city,
            beneficiary_state,
            beneficiary_country,
            beneficiary_zip,
            company_id,
            bank_name,
            bank_address,
            bank_city,
            bank_state,
            bank_country,
            bank_zipcode,
            account_number,
            account_routing_number,
            wire_routing_number,
            iban_number,
            sort_swift_code,
            branch_number,
            bic,
            individual_business,
            reference,
            user_id,
            wallet,
            amount,
            device,
            imageList
        )
    }

    suspend fun getCompanyDashboard(id: String): Response<CompanyDashboardResponse> {
        return getApi!!.getCompanyDashboard(id, "m")
    }

    suspend fun getBalance(id: String): Response<BalanceResponse> {
        return getApi!!.getBalance(id)
    }

    suspend fun getTransactionFee(body: TransactionFeeParamModel): Response<TransactionFeeResponse> {
        return getApi!!.getTransactionFee(body.company_id, body.currency, body.amount)
    }

    suspend fun onConvertCurrency(body: ConvertCurrencyParamModel): Response<CurrencyConversionRateResponse> {
        return getApi!!.onConvertCurrency(
            body.currencyfrom,
            body.currencyto,
            body.amount,
            body.user_id,
            body.company_id
        )
    }

    suspend fun getCompanyTransactionFilter(body: CompanyTransactionFilterParamModel): Response<CompanyTransactionFilterResponse> {
        return getApi!!.getCompanyTransactionFilter(
            body.comp_id,
            body.type,
            body.fromamount,
            body.toamount,
            body.staff,
            body.range_to,
            body.range_from,
            body.keyword,
            body.currency
        )
    }

    suspend fun getBeneficiaryId(id: String): Response<BeneficiaryIdResponse> {
        return getApi!!.getBeneficiaryId(id)
    }

    suspend fun getCompanyBeneficiary(companyId: String): Response<CompanyBeneficiaryResponse> {
        return getApi!!.getCompanyBeneficiary(companyId)
    }
}