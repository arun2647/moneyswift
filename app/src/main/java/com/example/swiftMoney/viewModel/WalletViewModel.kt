package com.example.swiftMoney.viewModel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.swiftMoney.model.beneficiaryId.BeneficiaryIdResponse
import com.example.swiftMoney.model.companyBeneficiary.CompanyBeneficiaryResponse
import com.example.swiftMoney.model.companyTransactionFilter.CompanyTransactionFilterParamModel
import com.example.swiftMoney.model.companyTransactionFilter.CompanyTransactionFilterResponse
import com.example.swiftMoney.model.convertCurrency.ConvertCurrencyParamModel
import com.example.swiftMoney.model.convertCurrency.ConvertCurrencyResponse
import com.example.swiftMoney.model.currencyConversionRate.CurrencyConversionRateParamModel
import com.example.swiftMoney.model.currencyConversionRate.CurrencyConversionRateResponse
import com.example.swiftMoney.model.login.LoginResponse
import com.example.swiftMoney.model.submitTransaction.SubmitTransactionParamModel
import com.example.swiftMoney.model.submitTransaction.SubmitTransactionResponse
import com.example.swiftMoney.model.walletHistory.WalletHistoryParamModel
import com.example.swiftMoney.model.walletHistory.WalletHistoryResponse
import com.example.swiftMoney.model.walletRecharge.WalletRechargeParamModel
import com.example.swiftMoney.network.Repository
import com.example.swiftMoney.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class WalletViewModel(val application: Application=Application(), val repository: Repository= Repository()) : ViewModel() {

    var resultWalletRecharge = MutableLiveData<Resource<LoginResponse>>()
    var resultWalletHistory = MutableLiveData<Resource<WalletHistoryResponse>>()
    var resultConversionRate = MutableLiveData<Resource<CurrencyConversionRateResponse>>()
    var resultSubmitTransaction = MutableLiveData<Resource<SubmitTransactionResponse>>()
    var resultConvertCurrency = MutableLiveData<Resource<CurrencyConversionRateResponse>>()
    var resultCompanyTransactionFilter = MutableLiveData<Resource<CompanyTransactionFilterResponse>>()
    var resultBeneficiaryId = MutableLiveData<Resource<BeneficiaryIdResponse>>()
    var resultCompanyBeneficiary = MutableLiveData<Resource<CompanyBeneficiaryResponse>>()


    fun onWalletRecharge(body: WalletRechargeParamModel) {
        resultWalletRecharge.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.onWalletRecharge(body)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultWalletRecharge.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultWalletRecharge.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultWalletRecharge.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultWalletRecharge.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }

    fun getWalletHistory(body: WalletHistoryParamModel) {
        resultWalletHistory.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.getWalletHistory(body)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultWalletHistory.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultWalletHistory.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultWalletHistory.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultWalletHistory.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }
   /* fun getConversionRate(body: CurrencyConversionRateParamModel) {
        resultConversionRate.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.getConversionRate(body)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultConversionRate.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultConversionRate.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultConversionRate.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultConversionRate.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }*/


    fun onSubmitTransaction(body: SubmitTransactionParamModel) {
        resultSubmitTransaction.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.onSubmitTransaction(body)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultSubmitTransaction.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultSubmitTransaction.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultSubmitTransaction.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultSubmitTransaction.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }
    fun onConvertCurrency(body: ConvertCurrencyParamModel) {
        resultConvertCurrency.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.onConvertCurrency(body)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultConvertCurrency.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultConvertCurrency.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultConvertCurrency.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultConvertCurrency.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }
    fun getCompanyTransactionFilter(body: CompanyTransactionFilterParamModel) {
        resultCompanyTransactionFilter.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.getCompanyTransactionFilter(body)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultCompanyTransactionFilter.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultCompanyTransactionFilter.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultCompanyTransactionFilter.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultCompanyTransactionFilter.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }


    fun getBeneficiaryId(id:String) {
        resultBeneficiaryId.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.getBeneficiaryId(id)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultBeneficiaryId.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultBeneficiaryId.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultBeneficiaryId.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultBeneficiaryId.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }

    fun getCompanyBeneficiary(companyId:String) {
        resultCompanyBeneficiary.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.getCompanyBeneficiary(companyId)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultCompanyBeneficiary.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultCompanyBeneficiary.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultCompanyBeneficiary.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultCompanyBeneficiary.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }
}