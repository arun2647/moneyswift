package com.example.swiftMoney.viewModel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.swiftMoney.base.MyApplication
import com.example.swiftMoney.model.addCompanyStaff.AddCompanyStaffParamModel
import com.example.swiftMoney.model.addCompanyStaff.AddCompanyStaffResponse
import com.example.swiftMoney.model.balance.BalanceResponse
import com.example.swiftMoney.model.companyDashboard.CompanyDashboardResponse
import com.example.swiftMoney.model.companyDashboard.Data
import com.example.swiftMoney.model.companyDetail.CompanyDetailResponse
import com.example.swiftMoney.model.companyStaffList.CompanyStaffListResponse
import com.example.swiftMoney.model.editProfile.EditProfileParamModel
import com.example.swiftMoney.model.editProfile.EditProfileResponse
import com.example.swiftMoney.model.staffDetail.StaffDetailResponse
import com.example.swiftMoney.model.transactionFee.TransactionFeeParamModel
import com.example.swiftMoney.model.transactionFee.TransactionFeeResponse
import com.example.swiftMoney.network.Repository
import com.example.swiftMoney.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CompanyViewModel(val application: Application, val repository: Repository) : ViewModel() {

    var resultGetCompanyDetail = MutableLiveData<Resource<CompanyDetailResponse>>()
    var resultGetCompanyStaffList = MutableLiveData<Resource<CompanyStaffListResponse>>()
    var resultAddCompanyStaffList = MutableLiveData<Resource<AddCompanyStaffResponse>>()
    var resultOnEditProfile = MutableLiveData<Resource<EditProfileResponse>>()
    var resultGetStaffDetail = MutableLiveData<Resource<StaffDetailResponse>>()
    var resultGetCompanyDashboard = MutableLiveData<Resource<CompanyDashboardResponse>>()
    var resultGetBalance = MutableLiveData<Resource<BalanceResponse>>()
    var resultGetTransactionFee = MutableLiveData<Resource<TransactionFeeResponse>>()
    var resultPagingData = MutableLiveData<PagingData<Data>>()

    fun getCompanyDetail(companyId: String) {
        resultGetCompanyDetail.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.getCompanyDetail(companyId)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultGetCompanyDetail.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultGetCompanyDetail.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultGetCompanyDetail.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultGetCompanyDetail.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }

    fun getCompanyStaffList(companyId: String) {
        resultGetCompanyStaffList.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.getCompanyStaffList(companyId)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultGetCompanyStaffList.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultGetCompanyStaffList.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultGetCompanyStaffList.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultGetCompanyStaffList.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }

    fun onAddCompanyStaff(body: AddCompanyStaffParamModel) {
        resultAddCompanyStaffList.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.onAddCompanyStaff(body)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultAddCompanyStaffList.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultAddCompanyStaffList.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultAddCompanyStaffList.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultAddCompanyStaffList.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }


    fun onEditProfile(body: EditProfileParamModel) {
        resultOnEditProfile.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.onEditProfile(body)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultOnEditProfile.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultOnEditProfile.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultOnEditProfile.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultOnEditProfile.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }

    fun getStaffDetail(staffId: String) {
        resultGetStaffDetail.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.getStaffDetail(staffId)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultGetStaffDetail.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultGetStaffDetail.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultGetStaffDetail.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultGetStaffDetail.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }


    fun getPagingDashboard(id: String): LiveData<PagingData<Data>> {
        //MyApplication.showLoader(MyApplication.getContext())
       return repository.getPagingDashboard(id)
    }


    fun getCompanyDashboard(id: String) {
        resultGetCompanyDashboard.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.getCompanyDashboard(id)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultGetCompanyDashboard.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultGetCompanyDashboard.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultGetCompanyDashboard.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultGetCompanyDashboard.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }


    fun getBalance(id: String) {
        resultGetBalance.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.getBalance(id)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultGetBalance.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultGetBalance.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultGetBalance.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultGetBalance.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }


    fun getTransactionFee(body: TransactionFeeParamModel) {
        resultGetTransactionFee.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.getTransactionFee(body)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.result == true) {
                            resultGetTransactionFee.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultGetTransactionFee.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultGetTransactionFee.value = Resource.error(
                            null,
                            response.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultGetTransactionFee.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }
}