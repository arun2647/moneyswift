package com.example.swiftMoney.viewModel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.swiftMoney.model.country.CountryResponse
import com.example.swiftMoney.model.forgotPassword.ForgotPasswordResponse
import com.example.swiftMoney.model.login.LoginParamModel
import com.example.swiftMoney.model.login.LoginResponse
import com.example.swiftMoney.model.register.RegisterParamModel
import com.example.swiftMoney.model.register.RegisterResponse
import com.example.swiftMoney.model.state.StateResponse
import com.example.swiftMoney.network.Repository
import com.example.swiftMoney.utils.Resource
import com.example.swiftMoney.utils.getError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AuthViewModel(val application: Application, val repository: Repository) : ViewModel() {

    var resultStaffRegister = MutableLiveData<Resource<RegisterResponse>>()
    var resultCountryList = MutableLiveData<Resource<CountryResponse>>()
    var resultStateList = MutableLiveData<Resource<StateResponse>>()
    var resultLogin = MutableLiveData<Resource<LoginResponse>>()
    var resultForgotPassword = MutableLiveData<Resource<ForgotPasswordResponse>>()


    fun onEmployeeRegister(body: RegisterParamModel) {
        resultStaffRegister.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.onStaffRegister(body)
                withContext(Dispatchers.Main) {
                    if (response?.code() == 200) {
                        if (response.body()?.result == true) {
                            resultStaffRegister.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultStaffRegister.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultStaffRegister.value = Resource.error(
                            null,
                            response?.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultStaffRegister.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }

    fun getCountryList() {
        resultCountryList.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.getCountryList()
                withContext(Dispatchers.Main) {
                    if (response?.code() == 200) {
                        if (response.body()?.result == true) {
                            resultCountryList.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultCountryList.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultCountryList.value = Resource.error(
                            null,
                            response?.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultCountryList.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }

    fun getStateList(countryId:String) {
        resultStateList.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.getStateList(countryId)
                withContext(Dispatchers.Main) {
                    if (response?.code() == 200) {
                        if (response.body()?.result == true) {
                            resultStateList.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultStateList.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultStateList.value = Resource.error(
                            null,
                            response?.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultStateList.value = Resource.error(null, t.message.toString())
                }
            }
        }
    }

    fun onLogin(body: LoginParamModel) {
        resultLogin.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.onLogin(body)
                withContext(Dispatchers.Main) {
                    if (response?.code() == 200) {
                        if (response.body()?.result == true) {
                            resultLogin.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultLogin.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultLogin.value = Resource.error(
                            null,
                            response?.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultLogin.value = Resource.error(null, getError(t))
                }
            }
        }
    }
    fun onForgotPassword(email:String) {
        resultForgotPassword.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.onForgotPassword(email)
                withContext(Dispatchers.Main) {
                    if (response.code() == 200) {
                        if (response.body()?.success == true) {
                            resultForgotPassword.value =
                                Resource.success(
                                    response.body()!!,
                                    response.body()?.message.toString()
                                )!!
                        } else {
                            resultForgotPassword.value =
                                Resource.error(null, response.body()?.message.toString())
                        }
                    } else {
                        resultForgotPassword.value = Resource.error(
                            null,
                            response?.message().toString()
                        )
                    }
                }
            } catch (t: Throwable) {
                withContext(Dispatchers.Main) {
                    resultForgotPassword.value = Resource.error(null, getError(t))
                }
            }
        }
    }
}