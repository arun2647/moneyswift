package com.example.swiftMoney.viewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.swiftMoney.network.Repository
import java.lang.IllegalArgumentException


class ViewModelFactory(
    private val application: Application,
    private val repository: Repository
) : ViewModelProvider.AndroidViewModelFactory(application) {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        when {
            modelClass.isAssignableFrom(AuthViewModel::class.java) -> {
                return AuthViewModel(application,repository) as T
            }

            modelClass.isAssignableFrom(WalletViewModel::class.java) -> {
                return WalletViewModel(application,repository) as T
            }
            modelClass.isAssignableFrom(CompanyViewModel::class.java) -> {
                return CompanyViewModel(application,repository) as T
            }
            else -> throw IllegalArgumentException("Unknown ViewModel class")
        }

    }
}
